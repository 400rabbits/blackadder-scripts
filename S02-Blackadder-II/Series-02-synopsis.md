# Blackadder II: Being a true and japesome historie of Elizabethan England

## Bells

Served by a dungball in a dress and accompanied by a bird-brained dimwit he can't shake off, Edmund, the bastard great-great-grandson of the repulsive original, is reasonably normal— until he meets Bob...

## Head

Edmund,newly appointed minister in charge of religious genocide and Lord High Executioner, finds himself in a spot of bother when he completely ruins Lord Farrow's weekend by cutting his head off.

## Potato

Blackadder sets off unwillingly to seek out new potatoes and to boldly go where Sir Rather-a-Wally Raleigh has already gone before...

## Money

Trouble is in store for Edmund when the baby-eating Bishop of Bath and Wells drops by unexpectedly and tries to shove a red-hot poker up places where a cotton bud would be kinder.

## Beer

An embarrassing incident with a turnip, an ostrich feather and a fantastically Puritan aunt leads to a right royal to-do in the Blackadder house-hold.

## Chains

Edmund is slightly inconvenienced when a fat-headed German chamber-pot boxes him up in a chestful of iron spikes and leaves him to play charades with a crazed Spanish interrogator...
