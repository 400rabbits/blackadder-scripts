# Blackadder Scripts

## Summary

*Blackadder* is a series of four BBC One pseudohistorical British sitcoms, plus several one-off instalments, which originally aired from 1983 to 1989. All television episodes starred Rowan Atkinson as the antihero Edmund Blackadder and Tony Robinson as Blackadder's dogsbody, Baldrick. Each series was set in a different historical period. [0]

I quote *Blackadder* a lot; it was a favourite TV show of mine back in the long-long ago. Though I can recall many hilariously *apropos* comments from the show's protagonists, I often have trouble with verbatim recall, and so search the intertubes for verification.

Now, in 2012, some kind fellow, who has maybe retired to their dream turnip in the country, started collecting scripts and posting them online at [BlackAdder Scripts: A blog which contains all four seasons of blackadders scripts.][https://allblackadderscripts.blogspot.com/](1) But there are a number of errors and inconsistencies.

Some of these have been corrected by the good people over at [Blackadder Quotes; Best Blackadder quotes online](https://blackadderquotes.com/). I find their format hard to digest, however, and so this time-wasting little project: a miserly attempt to crowd-collect additions/corrections/annotations etc.

**UPDATE**:
I've since found out that— and it should have been bloody obvious that that wwas such a thing— these transcripts were published in 1999.

```bibtex
@BOOK{Curtis1999,
  title     = "Blackadder",
  subtitle  = "The Whole Damn Dynasty",
  author    = "Curtis, Richard and Elton, Ben and Atkinson, Rowan and Lloyd, John"
  publisher = "Penguin Books",
  month     =  nov,
  year      =  1999,
  address   = "Harlow, England",
  language  = "en"
  isbn      = "0140280359"
}
```

For the most part, the transcripts are taken from the aired shows, and not from the published scripts. There is some variation between the two.

## References

- [0] <https://en.wikipedia.org/wiki/Blackadder>
- [1] <https://allblackadderscripts.blogspot.com/>
- [2] <https://blackadderquotes.com/>
- [3] <https://www.reddit.com/r/blackadder/>
- [4] <https://www.fanfiction.net/search/?keywords=blackadder&ready=1&type=story>
