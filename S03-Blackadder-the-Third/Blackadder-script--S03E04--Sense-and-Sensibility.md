Title:      Blackadder III: Episode 4: Sense and Sensibility
Credit:     written by
Author:     Richard Curtis AND Ben Elton
Credit:     directed by
Director:   Mandie Fletcher
Source:     NULL
Notes:      UNOFFICIAL TRANSCRIPTION FROM AIRED SHOW
Original air date: 8 october 1987
Copyright:  (c) 1987, British Broadcasting Corporation
Summary:    Following a failed assassination attempt, the Prince Regent hires two actors to tutor him in public speaking.

# Blackadder III: Episode 4: Sense and Sensibility

## DRAMATIS PERSONAE

E: Edmund Blackadder
B: Baldrick
G: Prince George
M: Mrs. Miggins
K: Keanrick, a thespian
MO: Mossop, another thespian
A: An Anarchist

## SCENE 1: THE KITCHEN

> Baldrick is polishing shoes. Blackadder enters and adjusts his best necktie. He is dressed more smartly than usual.

**Baldrick**
You look smart, Mr. Blackadder. Going somewhere nice?

**Blackadder**
No, I'm off to the theatre.

**Baldrick**
Don't you like it then?

**Blackadder**
No, I don't! A load of stupid actors strutting around, shouting, with their chests thrust out so far, you'd think their nipples were attached to a pair of charging elephants! And the *worst* thing about it is having to go with Prince *Mini-Brain*!

**Baldrick**
What, doesn't he like it, either?

**Blackadder**
No, no, he loves it. The problem is that he doesn't realize it's *made up*. Last year, when Brutus was about to kill Julius Caesar, the Prince yelled out, "Look behind you, Mr. Caesar!".

**Baldrick**
I can't see a point in the theatre. All that sex and violence. I get enough of that at home. Except for the sex, of course.

**Blackadder**
While we're out, Baldrick, I want you to give this palace a good *clean*. It's so dirty, it'd be unacceptable to a dung-beetle that had lost interest in its career and really let itself go.

[THE PRINCE CALLS OUT.]

**Prince**
Come along, Blackadder, or we'll miss the first act!

**Blackadder**
[IN A LOUD VOICE]
Coming, Sir. As fast as I can.

> He sits down with his legs up on the table

Stick the kettle on, Baldrick.

## SCENE 2: THE THEATRE

> Two actors are on stage in Egyptian garb, fighting with sword and knife. Quite a lot of overacting is involved.

**Keanrick**
Now Sir, give I this advice to thee: Never, never, never trust thine enemy.

> Keanrick draws his sword and stabs Mossop very unrealistically through the armpit. Prince George is horror-struck. Mossop sinks to the floor.

[THE PRINCE WATCHES RAPTLY; BLACKADDER COMPLETELY DISINTERESTEDLY.]

**Keanrick**
Thy life is forfeit... [KICKS MOSSOP, WHO IS STILL DYING NOISILY] Thy life is forfeit, Sir, and at an end, like our poor play. We hope it pleased you, friends.

[APPLAUSE, EXCEPT FROM THE PRINCE'S BOX.]

**Prince**
Certainly not, you murdering rotter! Guards-- arrest that man!

**Blackadder**
Your Highness-- it's only a play.

**Prince**
Oh, well, that's all very well, but that about the poor fellow who's *dead*? Saying it's only a play will not feed and clothe the little ones he leaves behind!

[SHOUTS] Call the militia!

**Blackadder**
But, Sir, he's not dead. See, he stands, awaiting your applause.

**Prince**
Oh, I say, that's very clever. He really isn't dead. (shouts and applauds) Oh Bravo! Bravo!

> The prince's applause sets off the whole audience clapping. On stage, Keanrick whispers to Mossop.

**Keanrick**
Blast, the Prince likes it!

**Mossop**
Oh shit, we'll close tonight.

> Suddenly, a wild-eyed anarchist jumps up on stage. He shouts at the royal box.

**Anarchist**
Work for the weavers! Smash the Spinning Jenny! Burn the rolling Rosalind! Destroy the going-up-and-down-a-bit-and-then-moving- along Gertrude! And death to the stupid Prince who grows fat on the profits!

> He hurls a fizzing bomb into the prince's box and jumps offstage. Prince George catches the bomb in very high spirits.

[THE AUDIENCE SCREAM AND RUN FOR COVER, EXCEPT FOR THE PRINCE.]

**Prince**
I say, how exciting! This play's getting better and better! Bravo! Bravo!

> Blackadder is sensibly hiding behind the curtains.

**Blackadder**
It's not a play anymore, Sir. Put the bomb down and make your way quietly to the exit.

**Prince**
Blackadder, you old thing, your problem is you can't tell when something's real and when it's not!

> There is a big explosion.

## SCENE 3: PRINCE GEORGE'S CHAMBERS

Prince George is on the chaise longue bandaged up and eating heartily. Blackadder is attending him.

**Prince**
I must say, Blackadder, that was a close shave! Why on earth would an anarchist possibly want to kill *you*?

**Blackadder**
I think it might've been *you* he was after, Sir.

**Prince**
Oh hogwash! What on earth makes you say that?

**Blackadder**
Well, my suspicions were first aroused by his use of the words, "Death to the stupid Prince!"

**Prince**
It was a bit rude, wasn't it?

**Blackadder**
These are volatile times, your Highness. The American Revolution lost your father the Colonies, the French Revolution murdered brave King Louis and there are tremendous rumblings in Prussia, although that might have something to do with the sausages. The whole world cries out, "Peace, Freedom, and a few less fat bastards eating all the pie."

**Prince**
Well, yes, quite, something must be done! Any ideas?

> Prince George muses thoughtfully on the pie in his hand.

**Blackadder**
Yes, Sir. Next week is your royal father's birthday celebrations. I suggest that I write a brilliant speech for you to recite to show the oppressed masses how unusually sensitive you are.

> Blackadder holds a vial of smelling-salts under the prince's nose to help him in his fragile state.

 G: PPHHHGGTTT! Well, tell me about these "oppressed masses", what are they so worked up about?

**Blackadder**
They're worked up, Sir, because they're so poor, they're forced to have children simply to provide a cheap alternative to turkey at Christmas. Disease and deprivation stalk our land like two giant stalking things. And the working man is poised to overthrow us.

> Baldrick enters, holding a bucket and broom. Prince George jumps up and tries to hide in front of the fireplace.

**Prince**
Oh, my God— and here he is!

**Blackadder**
Don't be silly, Sir. That's Baldrick, my dogsbody.

**Prince**
What's silly about that? He looks like an oppressed mass to me. Get him out of here at once!

**Blackadder**
Shoo, Baldrick! Carry on with your cleaning elsewhere. And by the end of tonight, I want that dining table so clean I can eat my dinner off it.

> Baldrick exits with resignation.

**Prince**
Crickey, Blackadder, I'm dicing with death here. The sooner I can show how unusually sensitive I am, the better.
[He BURPS]
Oh, I just had another brilliant thought.

**Blackadder**
[SKEPTICALLY] Another one, your Highness?

**Prince**
Yes, another one, actually! You remember that one I, I had about, uh, wearing underwear on the outside to save on laundry bills? Well, what I'm thinking to myself is, "Hello, why don't we ask those two actor chappies we saw tonight to teach me how to recite your speech?" Brilliant, eh?

**Blackadder**
No, your Highness. Feeble.

**Prince**
What?

**Blackadder**
I would advise against it. It's a *feeble* idea.

**Prince**
Well, tish-and-pish to your advice, Blackadder! Get them here at once! Damn it, I'd fed up with you treating me as if I'm sort of like some kind of a thickie! It's not me that's thick, it's you and you know why? Because I'm a bloody Prince and you're only a *butler*. And now go and get those actors here this minute, Mr. Thicky-Black-Thicky-Adder-Thicky.

## SCENE 4: MRS MIGGINS'S COFFEE SHOPPE

Mrs. Miggins is behind the counter. Blackadder enters.

**Blackadder**
Mrs. Miggins, I'm looking for a couple of actors.

 **Miggins**
Ooh. [SURPRISED; SHE PUTS DOWN THE BUN SHE WAS EATING]

Well, you've come to the right place, Mr. B. There's more Shakespearian dialogue in here than there are buns! [LAUGHS] All my lovely actors pop in on their way to rehearsals for a little cup of coffee and a big dollop of inspiration.

> She hands Blackadder a coffee.

**Blackadder**
You mean they actually rehearse? I thought they just got drunk, stuck on a silly hat and trusted to luck.

**Miggins**
Ohhh no. There's ever so much hard work that goes into the wonderful magic that is theatre today. Haa-haa... still I don't expect you'd know much about that, being only a little butler.

>She laughs and chucks Blackadder's cheek. He is getting annoyed.

**Blackadder**
They do say, Mrs. M, that verbal insults hurt *more* than physical pain. [HOLDS UP A THREE-PRONGED FORK] They are, of course, *wrong*, as you'll soon discover when I stick this toasting fork in your head.

> The door flies open— there is a bellow from outside.
[ACTORS KEANRICK AND MOSSOP ENTER.]

**Mossop**
[FROM OUTSIDE]
Ladies and gentlemen, will you please welcome Mr. David Keanrick.

**Miggins**
[SQUEALS AS USUAL] Oh hurrah!

[KEANRICK ENTERS, FOLLOWED BY MOSSOP.]

**Mossop**
And the fabulous Mr. Enoch Mossop.

> They enter to general cheers. Blackadder is not impressed.

**Miggins**
[APPLAUDS, CONTINUES TO SWOON]
Gentlemen, gentlemen!

**Keanrick**
Settle down, settle down, settle down.

**Mossop**
I'm sorry, no autographs.

**Keanrick**
My usual, Mrs. M.

**Miggins**
Oooh! Coming up, my lovley.

**Blackadder**
[NOTICING THERE'S NO ONE SURROUNDING THE ACTORSBLACKADDER MIMES WADING THROUGH A CROWD]
Ahh, if I can just squeeze through this admiring rabble...
Gentlemen, I've come with a proposition.

**Mossop**
How dare you, Sir. You think, just because we're actors, we sleep with *everyone*!

**Blackadder**
I think, being actors, you're lucky to sleep with *anyone*. I come here on behalf of my employer, to ask for some elocution lessons.

**Keanrick**
Haa-ha, I fear, that is quite impossible. We are in the middle of rehearsing our new play. And we could not possibly betray our beloved audience by taking time off.

**Mossop**
Oh no, mustn't upset the punters. Bums on seats, laddie, bums on seats.

**Blackadder**
And what play is this?

**Mossop**
It is a piece we penned ourselves, called "The Bloody Murder of the Foul Prince Romero and His Enormously Bosomed Wife".

**Blackadder**
A philosophical work then.

**Keanrick**
Indeed yes, Sir. The violence of the murder and the vastness of the bosom are entirely justified artistically.

**Blackadder**
Right, I'll tell the Prince that you can't make it.

> Blackadder stands to go, now he has their total attention.

**Keanrick**
Prince?

**Blackadder**
Sorry, yes. Didn't I mention that? It's the Prince Regent. Shame you can't make it. Still...

> Mossop pushed Keanrick off his chair and propels Blackadder into it.

**Mossop**
No, no, no, Sir, please, no. Please wait, Sir.
[TO KEANRICK, WHO IS CLUTCHING AT HIM]
Off, off! I think we *can* find some time, do you not, Mr. Keanrick?

**Keanrick**
[SOTTE VOCE] Definitely, Mr. Mossop.

**Blackadder**
No, no, you've got your beloved audience to think about.

**Keanrick**
Ah, sod the proles! We'll come.

**Mossop**
Yes, worthless bastards to a man.
**Blackadder**
It's nice to see artistic integrity thriving so strongly in the theatre. Well, this afternoon at four then, at the Palace. [EXITS]

## SCENE 5: PRINCE GEORGE'S CHAMBERS

> Blackadder knocks and enters. Prince George's face is made-up like the actors were earlier— ridiculous red cheeks and huge moustache. He is also wearing a toga.

**Prince**
Well, what do you think?

**Blackadder**
Are you ill or something?

**Prince**
No, I'm simply trying to look more like an actor.

**Blackadder**
Well, I'm sure you don't need the false moustache.

**Prince**
No?

**Blackadder**
No.

> Blackadder grabs the moustache and yanks it visciously. The prince screams with pain, and stumbled into a cupboard. He finds Baldrick inside clutching a feather duster.

**Prince**
Oowwwwh! Egads! It's that oppressed mass again!

> The prince drags Baldrick out of the cupboard and starts to strangle him.

**Blackadder**
No, Sir, that is Baldrick spring-cleaning.

**Prince**
Oh yes, so it is.

**Blackadder**
Ummpf, finish the job later, Baldrick.

**Baldrick**
Very well, Sir. The cleaning or the being strangled?

**Blackadder**
Either suits me.

**Prince**
Look Blackadder, this is all getting a bit hairy, isn't it? I mean, are you sure we can even trust these acting fellows? Last time we went to the theatre, three of them *murdered* Julius Caesar, and one of them was his best friend, Brutus.

**Blackadder**
As I've told you about *eight* times, the man playing Julius Caesar was *an actor* called Kemp.

**Prince**
Really?

**Blackadder**
[SHARPLY] Yes!

**Prince**
Thundering gherkins! Well, Brutus must have been pretty miffed when he found out.

**Blackadder**
[VERY SHARPLY] What?

**Prince**
That he hadn't killed Caesar after all, just some poxy actor called Kemp.

> This elicits a very exasperated look from Blackadder.

What, d'you think he went round to Caesar's place after the play and killed him then?

**Blackadder**

> Not very under his breath.

Oh, God! It's pathetic!

## SCENE 6: THE KITCHEN

> Baldrick is cleaning silver at the table. There is a loud sound of knocking above. He vaguely looks up. Blackadder enters.

**Baldrick**
Is that the door?

**Blackadder**
Oh, don't worry, it's just the actors.

**Baldrick**
My uncle Baldrick was in a play once.

[CONTINUED RAPPING. BLACKADDER POURS HIMSELF A CUP OF TEA.]

**Blackadder**
[DISINTERESTED] Really?

**Baldrick**
Yeah, it was called *Macbeth*.

**Blackadder**
And what did he play?

**Baldrick**
Second codpiece. Macbeth wore him in the fight scenes.

> Even more knocking, and even more ignoring of it.

**Blackadder**
So he was a *stunt* codpiece. [SIPS HIS TEA]

**Baldrick**
[NODS] Yes.

**Blackadder**
Did he have a large part?

**Baldrick**
Depends who's playing Macbeth.

**Blackadder**
Oh, incidentally, Baldrick— actors are very superstitious. On no account mention the word "*Macbeth*" this evening, alright?

**Baldrick**
Why not?

**Blackadder**
It brings them bad luck and it makes them very unhappy.

**Baldrick**
Oh, so you won't be mentioning it either?

**Blackadder**
No... well, not very often.

## SCENE 7: THE VESTIBULE AND PRINCE GEORGE'S CHAMBERS

> Keanrick and Mossop enter and brush past Blackadder.

**Blackadder**
You should have knocked.

**Keanrick**
Our knocks, impertinent butler, were loud enough to wake the hounds of hell!

[THE ACTORS GIVE BLACKADDER THEIR HATS. HE DROPS THEM AND KICKS THEM OUTSIDE.]

> The actors go into the chambers and Blackadder follows.

**Keanrick**
[TO MOSSOP] Lead on, Macduff.

**Mossop**
I shall...

(They enter. Blackadder dumps their hats on the floor and kicks them into
the hall.)

**Mossop**
..lest you continue in your quotation and mention the name of the "Scottish Play".

**Keanrick** Oh-ho! Never fear, I shan't do that. [LAUGHS]

**Blackadder**
By the "Scottish Play", I assume you mean *Macbeth*.

> A moment of utter horror from the two actors, followed by a violent and superstitious routine.

[THE ACTORS PERFORM A RITUAL WARDING OFF OF BAD LUCK.]

**Keanrick and Mossop**
Aahhhhh!
[SLAPPING EACH OTHERS HANDS, PAT-A-CAKE FASHION]
Hot potato, orchestra stalls, Puck will make amends.
[PINCH EACH OTHERS NOSES]
Aaahh!

**Blackadder**
What was that?

**Keanrick**
We were exorcising evil spirits. Being but a mere butler, you will not know the great theatre tradition that one does *never* speak the name of the "Scottish Play".

**Keanrick and Mossop**
Aahhhhh!
[SLAPPING EACH OTHERS HANDS, PAT-A-CAKE FASHION]

Hot potato, orchestra stalls, Puck will make amends.
[PINCH EACH OTHERS NOSES]
Aaahh!

**Blackadder**
Good lord, you mean you have to do *that* every time I say *Macbeth*?

**Keanrick and Mossop**
Aahhhhh!
[SLAPPING EACH OTHERS HANDS, PAT-A-CAKE FASHION]
Hot potato, orchestra stalls, Puck will make amends.
[PINCH EACH OTHERS NOSES]
Aaahh!

**Mossop**
Will you please stop saying *that*! Always call it the "Scottish Play".

**Blackadder**
So you want me to say the "Scottish Play"?

**Keanrick and Mossop**
YES!!!

**Blackadder**
Rather than *Macbeth*?

**Keanrick and Mossop**
Aahhhhh!
[SLAPPING EACH OTHERS HANDS, PAT-A-CAKE FASHION]
Hot potato, orchestra stalls, Puck will make amends.
[PINCH EACH OTHERS NOSES]
Aaahh!

[PRINCE GEORGE ENTERS FROM HIS BEDROOM]

**Prince**
For heaven's sake, what is all this hullabaloo? All this shouting and screaming and yelling blue murder? Why... it's like that play we saw the other day, what was it called... uhm...

**Blackadder**
*Macbeth*, Sir?

**Keanrick and Mossop**
Aahhhhh!
[SLAPPING EACH OTHERS HANDS, PAT-A-CAKE FASHION]
Hot potato, orchestra stalls, Puck will make amends.
[PINCH EACH OTHERS NOSES]
Aaahh!

**Prince**
No, no.It was called Julius Caesar.

**Blackadder**
Ah yes, of course. Julius Caesar... not *Macbeth*.

**Keanrick and Mossop**
Aahhhhh!
[SLAPPING EACH OTHERS HANDS, PAT-A-CAKE FASHION]
Hot potato, orchestra stalls, Puck will make amends.
[PINCH EACH OTHERS NOSES]
Aaahh!

**Blackadder**
Are you sure you want these people to stay?

**Prince**
Of course, I asked them, didn't I, Mr. Thicky-Butler.

**Keanrick**
Your Royal Highness, may I say what a great honour it is to be invited here?

**Prince**
Why certainly.

**Keanrick**
Thank you.

[DRAMATICALLY, AND EXTENDING HIS ARMS]

What a great honour that it is to be invited here to make merry in the halls of our King's loins' most glorious outpouring.

> Prince George screws up his face in disgust at these words.

**Prince**
Eeergh!

**Keanrick**
Now, your Highness, shall we begin straight away?

**Prince**
Absolutely, yes. Now, I've got this... um...

**Mossop**
Now, before we inspect the script, let us have a look at stance.

**Prince**
Right.

**Keanrick**
Yes. The ordinary fellow stands like... well... as you do now.

> He stands casually.

**Mossop**
Whereas your hero... stands thus.

[THE ACTORS ASSUME A HEROIC STANCE— LEGS SPREAD WIDE, HIPS THRUST FORWARDS.]

**Prince**
Right. Well, that's sort of like this...

[THE PRINCE FOLLOWS SUIT, UNINTENTIONALLY COMICALLY.]

**Keanrick**
Excellent, your Highness. Even more so...

**Prince**
What, oh, like that?

> All three stand with legs ridiculously apart. There is a creak.

What was that noise?

**Mossop**
It wasn't *me*! We are used to standing in this position.

[ANOTHER CREAK.]

**Prince**
It came from over here.

> He flings open a chest Baldrick is polishing. Prince George is terrified.

Anarchist!

**Baldrick**
Cleaner!

**Prince**
Alright, so you've had a wash, that's no excuse![THE PRINCE STARTS STRANGLING BALDRICK]

> Blackadder enters calmly. Prince George starts to strangle Baldrick again.

 Die Traitor!

**Blackadder**
No, Sir, that is Baldrick spring-cleaning.

**Prince**
But he's, look, he's got a bomb!

**Blackadder**
That's not a *bomb*, Sir, it's a sponge.

**Prince**
Oh yes, so it is. Well, get it out of here at once before it explodes.

[EXIT BALDRICK, CARRYING THE SPONGE VERY GINGERLY AT ARM'S LENGTH.]

**Prince**
[CONTINUING] Uhm, now, stance. I'm sorry about that. I think we really had something there.

> All three spread their legs even further.

**Keanrick**
Oh yes, your Highness. Why, your very posture tells me, "Here is a man of true greatness."

**Blackadder**
Either that or "Here are my genitals, please kick them."

**Mossop**
Sir, I really must ask that this ill-educated oaf be removed from the room.

**Keanrick**
Yes! Get out, Sir. Your presence here is as useful as fine bone china at a tea-party for drunken elephants.

**Prince**
Is that right? Well, yes, hang it all, get out Blackadder, and stop corking our juices.

**Blackadder**
Certainly, your Highness. I'll leave you to dribble in private.

## SCENE 8: THE KITCHEN

> Blackadder enters with an annoyed grunt, kicking over a bucket as he comes down the stairs. Baldrick is cleaning the floor

**Baldrick**
Is something wrong, Mr. B.?

**Blackadder**
[ANGRILY] I just about had it up to here with at that Prince. One more insult, and I'll be handing in my notice.

**Baldrick**
Oh, does that mean I'll be butler?

**Blackadder**
Not unless some kindly passing surgeon cut your head open with a spade and sticks a new brain in it.

**Baldrick**
Oh, right.

**Blackadder**
I don't know *why* I put up with it. I really don't. Every year at the Guild of Butlers' Christmas Party, I'm the one who has to wear the red nose and the pointy hat for winning the "Who's-Got-The-Stupidest-Master" Competition. Well, all I can say is, he'd better watch out! One more foot wrong and the contract between us will be as broken as this milk-jug.

**Baldrick**
But that milk-jug isn't broken.

**Blackadder**
You really do walk into these things, don't you, Baldrick?

> He smashes the jug over Baldrick's head, picks up a tray and walks out.

## SCENE 9: PRINCE GEORGE'S CHAMBERS

> Prince George is standing as before, legs spectacularly apart, and wearing the most contorted of grins. The actors are lounging.

**Mossop**
Excellent. And now, Sir, at last— the speech.

**Prince**
Right. [UNFOLDS HIS SPEECH AND PREPARES TO READ; COUGHS] Ahem...

**Keanrick**
No, no, no, no. Your Royal Highness. What have you forgotten?

**Prince**
Oh now look, if I stand any more heroically than *this*, I'm in danger of seriously disappointing my future Queen.

**Keanrick**
No, no, your Highness, not the stance... the *roar*.

**Prince**
You want me to roar?

**Mossop**
Well, of course we wish you to roar. All the great orators roar before commencing with their speeches. It is the way of things. Ah, Mr. Keanrick, from your *Hamlet*, please.

> Keanrick takes a stance, then...

**Keanrick**
Hh-hmm. "OOOOoooohhhhh, To be or not to be..."

> Defintely a roar.

**Mossop**
From your *Julius Caesar*...

**Keanrick**
"OOOOoooohhhhh, Friends, Romans, countrymen..."

[BLACKADDER ENTERS CASUALLY, CARRYING A TRAY.]

**Mossop**
From your leading character, in a play connected with Scotland.

**Blackadder**
That's *Macbeth*, isn't it?

**Keanrick and Mossop**
Aahhhhh!
[SLAPPING EACH OTHERS HANDS, PAT-A-CAKE FASHION]
Hot potato, orchestra stalls, Puck will make amends.
[PINCH EACH OTHERS NOSES]
Aaahh!

**Mossop**
[VERY NASALLY] Let's all roar together, shall we? One, two, three...

> They all roar, Prince George loudest and longest of all.

**Keanrick**
Excellent, our Highness. Now shall we try putting it all together?

> Prince George does the stance, the facial expression, gives the loudest roar of all.

**Prince**
RRROOOAAAAHHHHHHhhh...
[GLANCES AT HIS SPEECH] "Unaccustomed as I am to public speaking..."

**Keanrick**
No, no, no, Sirem no. Alas, I fear you mew it like a frightened tree. And may I see the speech?

[THE ACTORS READ, MUTTER TOGETHER WIT RIDICULING LAUGH.]

Who wrote this dribble?

[ALL LOOK AT EDMUND.]

**Blackadder**
Is there a problem with the speech?

[THE ACTORS LAUGH.]

**Prince**
Well, yes, there is a problem, actually. The problem is that you wrote it, Mr. Hopelessly-Drivelly-Can't-Write-For-Toffee-Crappy-Butler-Weed!

[THE ACTORS LAUGH AGAIN. THERE IS A LONG PAUSE, THEN BLACKADDER DROPS HIS TRAY, OBVIOUSLY INSULTED.]

**Blackadder**
Whoops!

> He exits. Perhaps for the last time.

## SCENE 10: THE KITCHEN

> Blackadder enters. Baldrick is scrubbing the steps.

**Baldrick**
Shall I get their supper, Sir?

**Blackadder**
Yes, preferably something that has first passed through the digestive system of the cat. And you'll have to take it up yourself.

**Baldrick**
Why?

**Blackadder**
Because I'm leaving, Baldrick. I'm about to enter the job market.Right, let's see...

> He picks up a newspaper and settles for a good read.

Situations Vacant: Mr. and Mrs. Pitt are looking for a baby-minder to take Pitt the Younger to Parliament. There's a fellow called George Stevenson has invented a moving kettle... wants someone to help with the marketing. Oh, and there's a foreign opportunity here— "Treacherous, malicious, unprincipled cad, preferably non-smoker wanted to be King of Sardinia. No time wasters please. By Napoleon Bonaparte, PO Box 1, Paris".
Right! We're on our way!

## SCENE 11: THE DINING ROOM

> Prince George and the actors are at supper.

**Mossop**
Oh, ah, Sir, about costume. Any thoughts?

**Prince**
Well, enormous trousers, certainly. Uhm. And I thought perhaps an admiral's uniform, because we know what all the nice girls love, don't we?

[THEY ALL LAUGH.]

**Prince**
I'll tell you what, why don't I go and try them on for you?

**Mossop**
Oh, what a super idea.

**Prince**
Help yourselves to wine. You'll need a stiff drink when you see the size of these damn trousers!

[THE ACTORS LAUGH AGAIN. THE PRINCE LEAVES; THE LAUGHTER DIES ABRUPTLY.]

**Keanrick**
Oh, my dear, what a ghastly evening!

**Mossop**
You're so right, love.

**Keanrick**
Look, while he's gone, why don't we have a quick read-through of "The Murder of Prince Romero and His Enormously Bosomed Wife"?

**Mossop**
Act one, Scene one?

**Keanrick**
Hmmm.

**Mossop**
"Spring has come, with all its gentle showers. Methinks it's time to hack the Prince to death..."

## SCENE 12: THE KITCHEN

> Blackadder is all packed and ready to go, with hat and cape on.

**Blackadder**
Baldrick, I would like to say how much I will miss your honest and friendly companionship.

**Baldrick**
Aaahh, thank you Mr. B.

**Blackadder**
But as we both know, it'll be an utter lie. I will therefore confine myself to saying simply, "Sod off and if I ever meet you again, it'll be twenty billion years too soon."

> And he leaves forever...

[BLACKADDER WALKS OUT OF THE ROOM...]

**Baldrick**
Goodbye, you lazy big-nosed, rubber-faced bastard.

> Or not. Blackadder reappears in the doorway.

[...BUT NOT OUT OF EARSHOT; HE COMES BACK IN. BALDRICK LOOKS WORRIED.]

**Blackadder**
I fear, Baldrick, that you will soon be eating those badly chosen words. I wouldn't bet you a single groat that you can survive five minutes here without me.

**Baldrick**
Oh come on, Mr. B., it's not as though we're going to get murdered or anything the minute you leave, is it?

**Blackadder**
Hope springs eternal, Baldrick.

> And he leaves again. The bell rings.

[BLACKADDER LEAVES; THE BELL RINGS.]

**Baldrick**
Coming!

## SCENE 13: THE DINING ROOM

> Baldrick walks along the corridor towards the dining room. He is about to knock but hears the actors talking very loudly. They don't notice him.

[THE ACTORS ARE STILL REHEARSING THEIR PLAY. BALDRICK OPENS THE DOOR AND LISTENS.]

**Keanrick**
Oooooaaahhh. Let's kill the Prince. Who will strike first?

**Mossop**
Let me, and let this dagger's point prick out his soft eyeball and sup with glee upon its exquisite jelly.

> Baldrick is terrified at what he has just heard.

**Keanrick**
Have you the stomach?

**Mossop**
I have not killed him yet, Sir,  
But when I do,  
I shall have the stomach  
And the liver, too,
And the floppily-doppolies
In their horrid glue.

**Keanrick**
What if a servant shall hear us in our plotting?

**Mossop**
Ah ha! Then shall we have servant sausages for tea,  
And servant rissoles shall our supper be!

[BALDRICK RUNS OFF IN TERROR, IN SEARCH OF THE PRINCE.]

## SCENE 14: PRINCE GEORGE'S CHAMBERS

> Prince George is trying on his big trousers. Baldrick charges in, terrified and yelling.

**Baldrick**
Murder! Murder! The Revolution's started!!

**Prince**
What?!

> He draws his sword and gets Baldrick by the throat.

**Baldrick**
A plot, a plot to kill you!

**Prince**
Ah, so you've come clean at last, have you, you bloody little poor person!

**Baldrick**
No, not me— the actors downstairs— they're anarchists!

**Prince**
Anarchists!?

**Baldrick**
Yeah, I heard them plotting. They're going to poke out your liver, turn me into rissole, and suck on your exquisite floppily-doppolies!

**Prince**
Oh, what are we going to do?

**Baldrick**
Well, Mr. Blackadder says, "when the going gets tough, the tough hide under the table".

**Prince**
Blackadder, of course! Where is he?

**Baldrick**
Oh, he's in Sardinia.

**Prince**
What? Why?

**Baldrick**
You were rude to him, so he left.

**Prince**
Oh no! What a mad, blundering, incredibly handsome young nincompoop I've been! What are we to do? If we go downstairs, they'll chop us up and eat us alive! We're doomed, doomed! Doomed!

[BALDRICK WHINES. SUSPENSE MUSIC STRIKES UP...]

**Baldrick**
SHhh! Shhh! Oh...

> They hear footsteps in the distance, getting closer. They both cower. Blackadder enters.

(Baldrick whimpers. We hear footsteps, getting closer. Baldrick and
George clutch each other. There is a creak, just before... Blackadder
enters.)

**Blackadder**
[HE CONSUTS HIS WATCH] Good evening, your Highness.

**Prince**
Oh, Blackadder!

**Blackadder**
Four minutes  twenty-two seconds. Baldrick, you owe me a groat.

**Prince**
Thank God you're here! We desperately need you!

**Blackadder**
Who? Me, Sir? Mr. Thicky-Black-Thicky-Adder-Thicky?

**Prince**
Oh! Tish! Nonsense!

**Blackadder**
Mr. Hopelessly-Drivelly-Can't-Write-For-Toffee-Crappy-Butler-Weed?

**Prince**
Yes, I'm...

**Blackadder**
Mr. Brilliantly-Undervalued-Butler who hasn't had a raise in a fortnight?

**Prince**
Take an extra thousand...

> Blackadder inspects his nails casually.

 ...Guineas?... Per month?

**Blackadder**
All right. What's your problem?

**Prince**
Well, the actors have turned out to be vicious anarchists!

> Blackadder is a touch skeptical.

They intend to kill us all!

**Blackadder**
What, are they going to do— *bore* us to death?

**Prince**
No, no, no! Stab us! Baldrick overheard them.

**Baldrick**
I did.

**Blackadder**
Are you sure they meant it, Sir?

**Prince**
Quite sure. Baldrick, how far apart were their legs?

**Baldrick**
Oh, this far.
[SPREADS HIS LEGS]

**Prince**
And their nipples?

**Baldrick**
That far.
[INDICATES ON HIS CHEST]

**Blackadder**
Alright, Sir, I'll see what I can do.

## SCENE 15: CORRIDOR AND DINING ROOM

> Blackadder reaches the door to the dining room and listens.

**Mossop**
To torture him, I lust  
Let's singe his hair,
And up his nostrils
Hot bananas thrust.

**Blackadder**
Rehearsal's going well, gentlemen?

**Mossop**
Begone!
A mere butler with the intellectual capacity of a squashed apricot can be of no use to us.

**Keanrick**
Yes indeed. Yes, Sir. Your participation is as irritating as a potted cactus in a monkey's pajamas.

**Blackadder**
Well, in that case, I won't interrupt you any longer. Sorry to disturb, Gentlemen.

## SCENE 16: PRINCE GEORGE'S CHAMBERS

> Prince George and Baldrick are hiding under the table. Blackadder enters.

**Prince**
Oh, Blackadder! Thank God you're safe! Well, what happened?

**Blackadder**
Sir, there was no need to panic. It was all perfectly straightforward.

**Prince**
Well?

**Blackadder**
They're traitors, Sir. They must be arrested, brutally tortured and executed forthwith.

[THE PRINCE AND BALDRICK CHEER IN UNISON; PRINCE BANGS HIS HEAD ON THE TABLE]

**Prince**
Bravo!

**Baldrick**
Hooray!

SCENE 17: THE DINING ROOM
> Two soldiers have Mossop and Keanrick bound. Blackadder, Baldrick, and Prince George look on.

**Mossop**
But, your Highness, there's been a terrible mistake.

**Blackadder**
That's what they were *bound* to say, Sir.

**Keanrick**
It was a play, Sir. A play! Look, all the words you heard written down on that paper.

**Blackadder**
Textbook, stuff again, you see. The criminals' vanity always makes them make one tiny, but fatal, mistake. Theirs was to have their entire conspiracy printed and published in plain manuscript.
[TO GUARDS]
Take them away!

> The actors are led out, bound— and bound for certain death.

**Mossop and Keanrick**
Mercy! We beg for mercy... please, Sir.

**Blackadder**
I have got only one thing to say to you... *Macbeth*!

**Keanrick and Mossop**
Aahhhhh!
[SLAPPING EACH OTHERS HANDS, PAT-A-CAKE FASHION]
Hot potato, orchestra stalls, Puck will make amends.
[PINCH EACH OTHERS NOSES]
Aaahh!

[THE ACTORS ARE DRAGGED AWAY.]

**Prince**
Well done, Bladder! How can I ever thank you?

**Blackadder**
Well, you can start by not calling me "Bladder", Sir.

> He shoult out into the hall.

*Macbeth*!

[DISTANT SOUNDS OF THE ACTORS' SUPERSTITIOUS RITUAL DRIFT IN FROM OUTSIDE...]

**Prince**
Of course, Bladder. No sooner said than done. No hard feelings?

**Blackadder**
No, Sir. It's good to be back in the saddle— did I say "saddle"? I mean "harness".

**Prince**
Bravo! So we're the best of friends as ever we were.

**Blackadder**
Absolutely, Sir.

**Prince**
Hurrah!

**Blackadder**
In fact now with the evil Mossop and Keanrick have got their comeuppance, the Drury Lane Theatre is free. I thought we might celebrate by staging a little play that I've written.

**Prince**
Oh, what an excellent idea! And with my new found acting skills, um, might there be a part in it for me, do you think?

**Blackadder**
I was hoping you might play the title role, Sir.

**Prince**
What a roaringly good idea! What's the play called?

**Blackadder**
"Thick Jack Clot Sits in the Stocks and Gets Pelted with Rancid Tomatoes".

**Prince**
Excellent!

[END CREDITS BEGIN]

-------------------------------------------------------------------------------

                                 For the
                        BENEFIT of SEVERAL VIEWERS
                         MR. CURTIS & MR. ELTON'S
                            Much admir'd Comedy
                           B L A C K   A D D E R
                             T h e   T H I R D
                                    OR
                            INK and INCAPABILITY
             was performed with appropriate Scenery Dresses etc.
                                    by
                             EDMUND BLACKADDER,
                           butler to the Prince,
                            Mr. ROWAN ATKINSON
                   Baldrick, a dogsbody, Mr. TONY ROBINSON
              The Prince Regent, their master, Mr. HUGH LAURIE
              Dr. Samuel Johnson, noted for his fat dictionary,
                            Mr. ROBBIE COLTRANE
                     Mrs. Miggins, a coffee shoppekeeper,
                         Miss. HELEN ATKINSON-WOOD
               Mossop,   \              Mr. HUGH PADDICK 
                          >  thespians
               Keanrick, /              Mr. KENNETH CONNOR 

             MUSIC [NEVER PERFORM'D BEFORE], Mr. HOWARD GOODALL

                 designer of graphics, Mr. GRAHAM McCALLUM
                    buyer of properties, Miss. JUDY FARR
                 designer of costumes, Miss. ANNIE HARDINGE
                  designer of make-up, Miss. VICKY POCOCK
                    mixer of vision, Miss. ANGELA WILSON
                    supervisor of cameras, Mr. RON GREEN
                  editor of videotape, Mr. CHRIS WADSWORTH
                   director of lighting, Mr. RON BRISTOW
               co-ordinator of technicalities, Mr. JOHN LATUS
                  supervisor of sound, Mr. PETER BARVILLE
               assistant to production, Miss. NIKKI COCKCROFT
               assistant manager of floors, Mr. DUNCAN COOPER
                  manager of production, Miss. OLIVIA HILL
                      the designer, Mr. ANTONY THORPE

                    the director, Miss. MANDIE FLETCHER

                          the producer, Mr. LLOYD

                To conclude with Rule Britannia in full chorus
                             NO MONEY RETURN'D
                            [C] BBC  MCMLXXXVII 
