Title:      Blackadder III: Episode 3: Nob and Nobility
Credit:     written by
Author:     Richard Curtis AND Ben Elton
Credit:     directed by
Director:   Mandie Fletcher
Source:     NULL
Notes:      UNOFFICIAL TRANSCRIPTION FROM AIRED SHOW
Original air date:   1 october 1987
Copyright:  (c) 1987, British Broadcasting Corporation
Summary:    Blackadder seeks to prove he's just as capable of rescuing people from France as The Scarlet Pimpernel.

# Blackadder III: Episode 3: Nob and Nobility

## DRAMATIS PERSONAE

E: Edmund Blackadder
B: Baldrick
G: Prince George
M: Mrs. Miggins
S: Lord Smedley
T: Lord Topper
F: Comte de Frou Frou
A: Evil Ambassador
MG: Madam Guillotine
P: The Scarlet Pimpernel

## SCENE 1: MRS MIGGINS'S COFFEE SHOPPE

> There is something different about the decoration— it is all in tricolour— red, white, and blue with strings of garlic hanging.

[MIGGINS IS DANCING ABOUT BY A TABLE OF TWO CUSTOMERS IN HER COFFEE SHOP]

**Miggins**:
Oh la la!  [LAUGHS HAPPILY]

[EDMUND BLACKADDER, BUTLER TO THE PRINCE REGENT, ENTERS]

**Edmund:**
Ah, good morning, Mrs Miggins.

**Miggins**:
Bonjour, monsieur.

**Edmund**:
What?

**Miggins**:
Bonjour, monsieur -- it's French.

**Edmund**:
So is eating frogs, cruelty to geese and urinating in the street, but that's no reason to inflict it on the rest of us.

**Miggins**:
But French is all the fashion!  My coffee shop is full of frenchies, and it's all because of that *wonderful* Scarlet Pimpernel.

**Edmund**:
The Scarlet Pimpernel is *not* wonderful, Mrs Miggins. There is no reason whatsoever to admire someone for filling London with a load of garlic-chewing French toffs, crying "Oh la la!" and looking for sympathy all the time just because their fathers had their heads cut off.

I'll have a cup of coffee and some shepherd's pie, please.

**Miggins**:
[PUT OFF] Oh! We don't serve *pies* anymore!  My French clientele consider *pies* uncouth.

**Edmund**:
I hardly think that a nation that eats snails and would go to bed with the kitchen sink if it put on a tutu is in any position to preach couthness. So what *is* on the menu?  [HE PICKS UP THE SMALL MENU AND FLIPS IT OVER LOOKING AT IT CASUALLY]

**Miggins**:
Well, today's hot choice is Chicken Pimpernel in a Scarlet Sauce, Scarlet Chicken in a Pimpernel Sauce, or *Huge* Suspicious-Looking Sausage... in a Scarlet Pimpernel Sauce.

**Edmund**:
What exactly is Scarlet Pimpernel sauce?

**Miggins**:
[SHE USES HER HANDS TO DEMONSTRATE AS SHE SPEAKS] You take a large ripe frog, squeeze it--

**Edmund**:
[INTERUPTS, PUTTING UP A HAND] Yes, yes, all right. I'm off to the pub

> He turns to go. A Frenchman passes him and bows elaborately.

**Frenchman**:
[BOWING] Ah, bonjour, monsieur!

**Edmund**:
Sod off.

## SCENE 2: THE KITCHEN

> Baldrick is in the kitchen. Blackadder storms in, picks up the cat and drop-kicks it.

**Baldrick**:
Oh, Sir!  Poor little Mildred the cat! What's he ever done to you?

**Edmund**:
It is the way of the world, Baldrick -- the abused always kick downwards. I am annoyed, and so I kick the cat... the cat [THERE IS A MOUSE 'EEK!'] pounces on the mouse, and, finally, the mouse--

**Baldrick**:
[SQUEALS IN PAIN] Agh!

**Edmund**:
...bites you on the behind.

**Baldrick**:
Well, what do I do?

**Edmund**:
Nothing.  You are last in God's great chain, Baldrick -- unless, of course, there's an earwig around here that you'd like to victimise.

[BALDRICK LEANS TOWARD EDMUND, TRYING TO GET HIM TO NOTICE SOMETHING]

**Edmund**:
Baldrick, what's happened to your nose?

> Baldrick is sporting the most hideous purple blister.

**Baldrick**:
Nice, init?

**Edmund**:
No it isn't.  It's revolting.

**Baldrick**:
Oh.  I'll take it off, then.

> Rather unexpectedly, Baldrick simply peels off the blister.

**Edmund**:
Baldrick, why are you wearing a false boil?  What are we to expect next: a beauty wart? a cosmetic verruca?

**Baldrick**:
It's a Scarlet Pimple, Sir.

**Edmund**:
Really?

**Baldrick**:
Yeah, they're all the rage down our way.  Everyone wants to express their admiration for the great Pimple and his brilliant disguises.  [SINGS] "They seek him here, they seek him there, those Frenchies seek him everywhere. Is he in Heaven? Or is he in Hell--"

**Edmund**:
...And what's revolting garlic smell?

**Edmund**:
[TAKES THE PIMPLE, SPEAKS ANGRILY]

What has this fellow done? Apart from pop over to France to grab a few French knobs from the ineffectual clutches [TOSSES PIMPLE INTO THE FIREPLACE] of some malnourished whingeing lefties, taking the opportunity while there, no doubt, to pick up some really good cheap wine and some of their marvelous open-fruit flans? Doesn't anyone know?  We hate the French!  We fight wars against them!  Did all those men die in vain on the field of Agincourt? Was the man who burned Joan of Arc simply wasting good matches?

[BELL RINGS.]

**Edmund**:
Ah, His Royal Highness, the Pinhead of Wales, summons me.  You know, I feel almost well-disposed towards him this morning. Utter chump though he may be, at least he's not French.

## SCENE 3: PRINCE GEORGE'S CHAMBERS

> Prince George is with two pals-- Lord Smedley and Lord Topper, utter prats. They are sitting on the bed, very refined in ruffled shirts.

**Prince**:
*Une toast*! *Encore une toast*! I say!  *Le Pimpernel Scarlet*!

**ALL**:
*Le Pimpernel Scarlet*!

> A knock on the door. Blackadder enters and stand in the doorway.

**Prince**:
Ah!  *Le Adder Noir*!  Come on au in!

        [EDMUND IS UPSET, BUT RESTRAINS IT.]

**Prince**:
[TO TOPPER AND SMEDLEY] This is the fellow to ask, you chaps: my butler -- terribly clever.  Brighter than a brain pie.
[TOPPER AND SMEDLEY CHUCKLE LIKE THE DANDIES THEY ARE]

Blackadder, we're trying to guess who the Scarlet Pimpernel is, so we can send him an enormous postal order to express our admiration.  Any ideas?

**Edmund**:
Well, I'm sure if you addressed the envelope to "The Biggest Show-off in London," it would reach him eventually.

[TOPPER AND SMEDLEY STAND UP FROM WHERE THEY WERE LYING (ON THE PRINCE'S BED) AND APPROACH EDMUND.]

**Topper**:
Tish and pish! Gadzooks! Milarky!  How dare you say such a thing? Dem me, Sir, if you're not the worst kind of swine!

**Smedley**:
Dem that swine!

**Edmund**:
I'm sorry, Sir.  I was merely pointing out that sneaking aristocrats out from under the noses of French revolutionaries is about as difficult as putting on a hat.

**Topper**:
Sink me, Sir!  This is treason!  The Scarlet Pimpernel's a hero, and the revolution is orchestrated by a ruthless band of highly organised killers, dem them!

**Smedley**:
Dem those organised killers!

**Topper**:
[TURNING TO PRINCE] George, if I remember rightly, we were just discussing the French Embassy ball in honour of the exiled aristocracy...

**Prince**:
We certainly were -- where I intend the wear the most magnificent pair of trousers ever to issue forth from the delicate hands of Messers Snibcock and Turkey, Couturiers to the Very Wealthy and the Extremely Fat.  If the Pimpernel does finally reveal himself, I don't want to get caught out wearing boring trousers!

**Smedley**:
Dem those boring trousers!

**Topper**:
Well, what say we bet your cock-sure domestic a thousand guineas he can't go to France, rescue an aristocrat, and present him at the ball?

[EDMUND LOOKS UP.]

**Topper**:
Hah!  That's turned you white, hasn't it?  That's frightened you, you lily-livered, caramel-kidneyed, custard-coloured cad? Not so bouyant now, are you, eh? *Eh*?

**Smedley**:
Eh?

> Little pause. Blackadder is cucumber cool.

**Edmund**:
On the contrary, Sir.  I'll just go and pack.

**Topper**:
Oh.

**Edmund**:
Perhaps Lord Smedley and Lord Topper will accompany me.  I'm sure it will be a fairly easy trip -- the odd death-defying leap and a modest amount of dental torture...  Want to come?

**Topper**:
[FRIGHTENED] Oh, no!

**Smedley**:
Oh, no...

**Topper**:
Dem!

**Smedley**:
Dem!

**Topper**:
Er, any... day... now... I've got an appointment at my doctor.  I've got a bit of a sniffle coming on -- I can feel it in my bones.

**Smedley**:
Dem bones, dem bones, dem...

**Prince**:
You know... what about next week?  Oh, come on, you chaps, get your diaries out, come on!

**Topper**:
Oh, all right.  Dem!

**Smedley**:
Dem!

**Topper**:
I left it behind!

**Smedley**:
Behind!

**Topper**:
...and, err, besides, I've just remembered: my father's just died! I've got to be at his funeral in ten minutes!  Dem sorry! Goodbye, Your Highness.

[HE BOWS, GIVING HIS DRINK TO EDMUND. EDMUND OPENS THE DOOR AND LETS HIM OUT.]

**Smedley**:
Err. Dem!  I'm the best man.  Dem! Dem that dead father! Dem him! Goodbye!
[GIVES HIS DRINK TO EDMUND, BOWS AND EXITS FOLLOWING TOPPER"]

**Edmund**:
[BEYOND THE DOOR TO THE EXITING PAIR] See you at the ball.

**Prince**:
Oh, what a shame they were so busy.  [WALKING INTO THE MAIN CHAMBER FROM THE BEDROOM] It would have been lovely to have had them with us.

**Edmund**:
[WORRIED] "Us"?

**Prince**:
Yes.

**Edmund**:
*You're* coming, Sir?

**Prince**:
Well, certainly.

**Edmund**:
Ah.  And nothing I can say about the mind-bending horrors of the revolution could put you off?

**Prince**:
Absolutely not!  Now, come on, Blackadder -- let's get packing. I want to look my best for those fabulous French birds.

**Edmund**:
Sir, the type of women currently favoured in France are toothless crones who just cackle insanely.

**Prince**:
Oh, ignore that -- they're just playing hard-to-get.

**Edmund**:
By removing all their teeth, going mad and aging forty years?

**Prince**:
That's right -- the little teasers!  Well, come on!  [HE RECLINES] Erm, I think a blend of silks and satins.

**Edmund**:
I fear not, Sir.  If we are to stand any chance of survival in France, [HE RINGS THE SERVANT BELL] we shall have to dress as the smelliest lowlife imaginable.

**Prince**:
Oh yes?  What sort of thing?

**Edmund**:
Well, Sir, let me show you our Paris Collection...

[BALDRICK BEGINS WALKING IN FROM THE OUTER DOOR.]

**Edmund**:
Baldrick is wearing a sheep's-bladder jacket, with matching dung-ball accessories.  Hair by Crazy Meg of Bedlam Hair.  Notice how the overpowering aroma of rotting pilchards has been woven cunningly into the ensemble.

**Edmund**:
Baldrick, when did you last change your trousers?

**Baldrick**:
[AS IF REHEARSED] I have never changed my trousers.

**Edmund**:
Thank you.  [TO PRINCE] You see, the ancient Greeks, Sir, wrote in legend of a terrible container in which all the evils of the world were trapped.  How prophetic they were.  All they got wrong was the name.  They called it "Pandora's Box," when, of course, they meant "Baldrick's Trousers."

**Baldrick**:
[TO PRINCE] It certainly can get a bit whiffy, there's no doubt about that!

**Edmund**:
We are told that, when the box was opened, the whole world turned to darkness because of Pandora's fatal curiosity.

[TO BALDRICK] I charge you now, **Baldrick**
for the good of all mankind, never allow curiosity to lead you to open your trousers.  Nothing of interest lies therein.

[TO PRINCE] However, Your Highness, it is trousers exactly like these that *you* will have to wear if we are to pass safely into France.

**Prince**:
Mmm, ahem, yes, well, you know, er, on second thought, I think I might give this whole thing a miss.  You know, my tummy's playing up a bit.  Er, wish... wish I could come, but just not poss with this tum.

**Edmund**:
I understand perfectly, Sir.

**Prince**:
Also, the chances of me scoring if I look and smell like him are *zero*.

**Edmund**:
Well, that's true, Sir.  We shall return presently to bid you farewell.

[PRINCE GEORGE TURNS TO ENTER HIS BEDROOM; BLACKADDER AND BALDRICK WALK OUT TOGETHER INTO THE VESTIBULE]

**Baldrick**:
Mr B, I've been having second thoughts about this trip to France.

**Edmund**:
Oh?  Why?

**Baldrick**:
Well, as far as I can see, looking and smelling like this, there's not much chance of *me* scoring, either.

[EDMUND THWAPS HIM ON THE HEAD.]

## SCENE 4: THE VESTIBULE

> Blackadder is in  his cloak ready to leavw. Baldrick has two huge bags, Blackadder asmall briefcase.
[THIS SCENE IS OVERPLAYED, COMPLETE WITH 'FAREWELL' HARP MUSIC.]

**Prince**:
Well, Blackadder, this is it.

**Edmund**:
Yes, Sir.  If I don't make it back, please write to my mother and tell her that I've been alive all the time; it's just that I couldn't be bothered to get in touch with the old bat.

**Prince**:
Well, of course, old man.  It's the very least I could do.

**Edmund**:
We must leave at once.  The shadows lengthen, and we have a long and arduous journey ahead of us.  [HE SHAKES PRINCE'S HAND.] Farewell, dear master and -- dare I say? -- friend.

[EDMUND AND PRINCE EMBRACE.  PRINCE SPEAKS AS THEY SEPARATE.]

**Prince**:
Farewell, brave liberator and -- dare I say it? -- butler!

[EDMUND AND BALDRICK LEAVE.  PRINCE STARTS TO CRY.]

## SCENE 5: THE KITCHEN

> Blackadder and Baldrick enter with their bags from the back door.

**Edmund**:
Right, stick the kettle on, Balders.

**Baldrick**:
What, aren't we going to France?

**Edmund**:
Of course we're not going to France -- it's incredibly dangerous there!

**Baldrick**:
Well, how are you going to win your bet?

**Edmund**:
As usual, **Baldrick**
by the use of the large thing between my ears.

**Baldrick**:
Oh, your nose...

**Edmund**:
No, **Baldrick**
my brain.  All we do is lie low here for a week, then go to Mrs Miggins's, pick up any old French aristocrat, drag him through a puddle, take him to the ball, and claim our thousand guineas.

**Baldrick**:
Well, what if the prince finds us here?

**Edmund**:
He couldn't find his own fly buttons, let alone the kitchen door.

## SCENE 6: PRINCE GEORGE'S CHAMBERS

> Prince George is unpackaing a stupendous pair of trousers.

**Prince**:
[TO HIMSELF] What a pair of trousers!  I shall be the Belle of the Embassy Ball!  Now, how do you put them on?  Er...  [SHOUTS] Blackadder!

Oh, no -- damn! -- he's gone to France.  Well, I'll do it myself; shouldn't be too difficult.  Erm... Er...

> He puts them on his arms. On second thoughts it is a pretty tricky manoeuvre.

## SCENE 7: THE KITCHEN (ONE WEEK LATER)

> It is one week later. Blackadder and Baldrick are sitting with ther feet up. Blackadder is smoking pipe and reading a newspaper.

**Edmund**:
 Well, Baldrick, what a very pleasant week.  We must do this more often.

**Baldrick**:
[SEEMING A BIT BORED] Yes, I shall certainly choose revolutionary France for my holiday again next year.

**Edmund**:
Still, time to go to work.  Off to Mrs Miggins' to pick up any old French toff--

[A CRASHING NOISE UPSTAIRS INTERRUPTS HIM.]

**Baldrick**:
What do you think that is?

**Edmund**:
Well, if I was feeling malicious, I would say it was the prince still trying to put his trousers on after a week.

## SCENE 8: PRINCE GEORGE'S CHAMBERS

> Prince George re-enters from his bedroom to the drawing room. The trousers are now on his head

**Prince**:
Damn!

> He bangs his head on the wall.

## SCENE 9: MRS MIGGINS'S COFFEE SHOPPE

> Enter Blackadder and Baldrick.

**Edmund**:
Ah, Mrs Miggins. I'd like a massive plate of pig's trotters, frogs' legs and snails' ears, please -- all drenched in your lovely Scarlet Pimpernel Sauce.

**Miggins**:
Not so hostile to the frenchies *now*, Mr B?

**Edmund**:
Certainly not, Mrs M. I'd sooner be hostile to my own servant.

[BAPS BALDRICK ON THE BACK OF THE HEAD]

In fact, I came here specifically to meet lovely frenchies.

**Miggins**:
Well, *vive* to that and an éclair for both of us!  [LAUGHS]

**Edmund**:
*Vive*, indeed.  Now, what I'm looking for, Mrs M, is a particular kind of Frenchie. Namely, one who is transparently of noble blood but also short on cash.

**Miggins**:
Ah, well, I've got just the fellow for you -- over there by the window: The Comte de Frou Frou.

[SHOT OF FROU FROU HOLDING -- AND LOOKING ODDLY AT -- A HUGE SUSPICIOUS-LOOKING SAUSAGE]

**Miggins**:
He's pretty down on his luck, and he's made that horse's willy last all morning.

> Blackadder and Baldrick move over to him. Blackadder is delighted by his find.

**Edmund**:
Oh, good.  Baldrick, we have struck garlic!

[EDMUND WIPES SOME CRUMBS OFF FROU FROU'S TABLE ONTO A PLATE AND HANDS THEM TO BALDRICK.]

**Edmund**:
Now you can some lunch, Baldrick.

**Baldrick**:
Thank you.

[BALDRICK EXITS WITH THE PLATE.]

**Edmund**:
Le Comte de Frou Frou, I believe?

**Frou Frou**:
[LOOKS UP] Eh?

**Edmund**:
[SITTING AT THE TABLE] Do you speak English?

**Frou Frou**:
A leetle.

**Edmund**:
Yes, when you say "a little," what exactly do you mean?  I mean, can we talk? Or are we going to spend the rest of the afternoon asking each other the way to the beach in very loud voices?

**Frou Frou**:
Ah, no.  I can, er, order coffee, deal with waiters, make sexy chit-chat with girls -- that type of thing.

**Edmund**:
Oh, good.

**Frou Frou**:
Just don't ask me to take a physiology class or direct a light opera.

**Edmund**:
No, no, I won't.  Now, listen, Frou Frou, would you like to earn some money?

**Frou Frou**:
No, I wouldn't. I would like other people to earn it and then *give* it to me, just like in France in the good old days.

**Edmund**:
Yes, but this is a chance to return to the good old days.

**Frou Frou**:
Oh, how I would love that!  I hate this life!  The food is filthy!  This huge sausage is very suspicious.  If I didn't know better, I'd say it was a horse's wi--

**Edmund**:
es, yes, yes, all right.  Now, listen; the plan is this. I have a bet on with someone that I can get a Frenchman out of Paris.  I want *you* to be that Frenchman.  All you have to do is come to the embassy with me, say that I rescued you, and then walk away with fifty guineas and all the vol-au-vents you can stuff in your pockets.  What do you say?

**Frou Frou**:
[SMILING] It will be a pleasure!  If there's one thing we aristocrats enjoy, it's a fabulous part!  Oh, the music! Oh, the laughter! Oh -- if only I'd brought my mongoose costume.

> Blackadder is not enthralled

## SCENE 10: THE EMBASSY

> Blackadder opens the door. Baldrick and Frou Frous follow him. The atmosphere is a bit odd, rather more like a prison cell than an embassy.

[IT IS DANK, AND SOME MOANS OF DESPAIR CAN BE HEARD.]

**Frou Frou**:
Yes, well, obviously it hasn't really got going yet.

**Edmund**:
I think that is a bit of an understatement, Frou Frou.  I've been at autopsies with more party atmosphere.

**Frou Frou**:
Don't worry!  In a moment we will hear the sound of music and happy laughter...

> A dramaatic sting of music and then a harsh, cruel laugh. A revolutionary appears from the shadows of the corridor.

**Ambassador**:
[TO FROU FROU] B*on soir, monsieur*.

**Frou Frou**:
*Bon soir*!

**Edmund**:
Ah, good evening, my man.  Do you speak English?

**Ambassador**:
Leetle. [GESTURES WITH HAND]

**Edmund**:
Good, well, just take me to the Ambassador, then, will you?

**Ambassador**:
Pardon?

> Blackadder takes stock, then proceeds slowly and clearly with actions to help the stupid foreigner.

**Edmund**:
I have rescued an [PUSHES THE END OF HIS NOSE UP] aristocrat, from [MAKES CLAW-LIKE HANDS] the clutches of the evil revolutionaries.  Please take me to the Ambassador.

> The Frenchman replies slowly and clearly with the same actions.

**Ambassador**:
No, I won't.  I *am* an [MAKES CLAW HANDS] "evil revolutionarie," and have [SLICES FINGER ACROSS HIS NECK] murdered the [PUSHES UP HIS NOSE] ambassadeur, and turned him into [SLAPS THE BACK AND FRONT OF ONE HAND AGAINST THE OTHER; MAKES EATING GESTURE] pâté.

**Edmund**:
Ah.

**Ambassador**:
[TO FROU FROU] And you, aristo-pig, are trapped!!

> The Ambassador and Frou Frou face up like mad things.

**Frou Frou**:
Pig? Hah!  You will regret your insolence, revolutionary deug!

**Ambassador**:
Dog? Hah!  You will regret your arrogance, royalist snake!

**Frou Frou**:
Snake? Hah!

**Edmund**:
[STEPPING IN] Look, I've very sorry to interrupt this very interesting discussion, but it really is none of my business, so I think I'll be on my way.  Come on, Baldrick.

> He sets off for the door.

**Ambassador**:
[STOPPING EDMUND] Ah ah ah ah ah ah ah!  Not so fast, English! In rescuing this, eu [MOTIONS AT FROU FROU], this, eu, '*boîte de stinkyweed*', you have attempted to pervert revolutionary justice. Do you know what they do to people who do that?

**Edmund**:
They're... given a little present and allowed to go free?

**Ambassador**:
No...

**Edmund**:
They're smacked and told not to be naughty, but basically let off?

**Ambassador**:
No.

**Baldrick**:
[RAISING HIS HAND] I think I know.

**Edmund**:
[QUITE UNHAPPY AND DEPRESSED] What?

**Baldrick**:
[QUITE HAPPY THAT HE KNOWS THE ANSWER] They're put in prison for the night, and brutally guillotined in the morning.

**Edmund**:
Well done, Baldrick.

**Ambassador**:
Your little g'nome is correct, *monsieur*.  Gentlemen! Welcome to the last day of your life!  

> He shuts and locks the door. Frou Frou peers through its bars. It's all gone horribly wrong. Far from attending a regal bash, they have ended up in a revolutionary prison in the heart of London

## SECENE 11: PRISON CELL IN THE EMBASSY

[OUR HEROES IN A CELL, WITH AMBASSADOR OUTSIDE.]

**Frou Frou**:
How dare you, you filthy weasel!

**Ambassador**:
Weasel? Hah!  You're one to talk, aristo-warthog!

**Frou Four**:
Warthog? Hah!

**Ambassador**:
Hah!

**Edmund**:
[PULLING FROU FROU AWAY FROM THE BARRED WINDOW] Excuse me, Frou Frou.

[CONFIDENTLY ADDRESSING THE AMBASSADOR] Look, mate, me old mate. We're both working class. We both hate these rich bastards. I mean, come on, come on, me old mucker, just, just let me go -- you've got nothing against me.

**Ambassador**:
On the contrary!  I *hate* you English with your boring trousers and your shiny toilet paper, and your ridiculous preconception that Frenchmen are great lovers -- [LOOKS BOTH WAYS, THEN SPEAKS A BIT SOFTLY] *I'm* French, and I'm hung like a baby carrot and a couple of petit-pois.

Farewell, "old muckeur" and [SHOUTS] death to the aristos!

**Baldrick**:
[JOINING IN HAPPILY] Death to the aristos!

**Edmund**:
Oh, shut up, Mouse-brain.

**Baldrick**:
Oh, yes.

[NOW INSIDE THE CELL.  BALDRICK SITS ON THE BED.]

**Frou Frou**:
Monsieur, why do you waste your words on this scum? Have no fear!  The Scarlet Pimpernel will save us.

**Edmund**:
Hah!  

[KNOCKS BALDRICK OFF THE BED; BALDRICK FALLS TO THE FLOOR, AND REMAINS SITTING WHERE HE LANDS]

Some hope.  [LIES DOWN] The Scarlet Pimpernel is the most overrated human being since Judas Iscariot won the A.D. 31 Best Disciple Competition.

**Frou Frou**:
Well, if he *should* fail us, here: I these have these suicide pills.  One for me [PLUCKS IT FROM HIS EAR]; one for you [FROM HIS NOSE]; and one for the dwarf [FROM HIS BOTTOM -- SILLY NOISES ACCOMPANY EACH].

**Edmund**:
Say "thank you", Baldrick.

**Baldrick**:
Thank you, Mr Frou.

[PUTS PILL TO HIS MOUTH

**Edmund**:
[EDMUND STOPS HIM.] Uh-huh!

[THE CELL DOOR OPENS]
**Frou Frou**:
Ah, the Pimpernel!!

**Baldrick**:
Hurray!

**Ambassador**:
[ENTERING]
Ah, the *ambassador*, hurray. [MOVES HIS FINGERS ABOUT, BOUNCES ON HIS TOES] Hmm, I've got nothing to do...  So I think I will torture...
[POINTS TO FROU FROU, FORCES HIM TO STAND, AND SHOUTS]
You, aristo-mongrel!

**Frou Frou**:
Mongrel? Hah!  I look forward to it, proletarian skunk!

**Ambassador**:
Skunk? Hah!  We'll see about that, aristocratic happypotamus!

**Frou Frou**:
[BEING LED OUTSIDE] Happypotamus? Hah!  We'll soon see who's the happypotamus, proletarian zebra!

[CELL DOOR IS SHUT AND LOCKED]

**Abassador**:
Zebra! Ha!...

**Baldrick**:
I'm glad to say, I don't think you'll be needing those pills, Mr B.

**Edmund**:
I'm I jumping the gun, Baldrick, or are the words "I have a cunning plan" marching with ill-deserved confidence in the direction of this conversation?

**Baldrick**:
They certainly are!

**Edmund**:
Well, forgive me if I don't jump up and down with glee. Your record in this department is not exactly a hundred percent. So, what's the plan?

**Baldrick**:
We do... nothing.

**Edmund**:
Yep, that's another world-beater.

**Baldrick**:
Wait, I haven't finished.  We do nothing until our heads have actually been cut off...

**Edmund**:
And then we... spring into action?

**Baldrick**:
Exactly!  You know how, when you cut a chicken's head off, it runs round and round the farmyard?

**Edmund**:
Yeeees...

**Baldrick**:
Well, we wait until our heads have been cut off, then we run round and round the farmyard, out the farm gate, and escape. What do you think?

**Edmund**:
Yes... My opinions are rather difficult to express in words, so perhaps I can put it this way...  [TWEAKS BALDRICK'S NOSE]

**Baldrick**:
It doesn't really matter, 'cause the Scarlet Pimpernel will save us, anyway.

**Edmund**:
No he won't, Baldrick.  Either I think up an idea, or, tomorrow, we die -- which, Baldrick, I have to tell you, I have no intention of doing, because I want to be young and wild, and then I want to be middle-aged and rich, and then I want to be old and annoy people by pretending that I'm deaf.  Just be quiet and let me think.

## SCENE 11: PRISON CELL IN THE EMBASSY [LATER THAT NIGHT]

> It is night. It is dark. It is quiet. Then...

**Baldrick**:
I can't sleep, Mr Blackadder...

**Edmund**:
I said "Shut up"!

**Baldrick**:
I'm so excited to think that the Scarlet Pimpernel will be here at any moment!

**Edmund**:
I wish you'd forget this ridiculous fantasy, Baldrick. Even if he did turn up, the guards would be woken by the scraping noise as he tried to squeeze his massive swollen head through the door.

**Baldrick**:
I couldn't sleep when I was little.

**Edmund**:
You still are little, Baldrick.

**Baldrick**:
Yeah, well, when I was even littler, see, we used to live in this haunted hovel.  Every night, my family were troubled by a visitation from this disgusting ghoul.  It was terrible. First there was this unholy smell, then this tiny, clammy, hairy creature would materialise in the bed between them. Fortunately, I could never see it, myself.

**Edmund**:
Yes. Tell me, **Baldrick**
when you left home, did this repulsive entity mysteriously disappear?

**Baldrick**:
That very day.

**Edmund**:
I think then that the mystery is solved.  Now shut up.  Either I think up an idea, or, tomorrow, we meet our maker -- in my case, God; in your case, God knows But I'd be surprised if he won any design awards.

[CAMERA VIEW PANS AWAY FROM THEM TO THE WINDOW]

**Edmund**:
Wait a minute!  I thought of a plan!

**Baldrick**:
Hurray!

**Edmund**:
Also, I thought of a way to get you to sleep!

**Baldrick**:
What?

[THUNK!]

**Baldrick**:
Oof!

## SCENE 12: PRISON CELL IN THE EMBASSY [NEXT MORNING]

> The next morning. The door opensand the Ambassador enters.

**Ambassador**:
'Morning, scum  Did we sleep well, eh?

**Edmund**:
Like a top, thank you.  But, by jiminee, you must be feeling thirsty after your long night's brutality!  [HE DROPS A SUICIDE PILL INTO A CUP OF LIQUID, THEN PROFFERS THE CUP.] Drink?

**Ambassador**:
Eu, non, merci... Not while I am on duty.

**Edmund**:
Oh.  Perhaps later.

**Ambassador**:
For you, *monsieur*, there is no later.  [GETS DRAMATIC] Because, gentlemen, I am proud to introduce France's most [PUTS A HAND ON HIS ABDOMEN] vicious woman.  Unexpectedly arrived from Paris this morning, would you please welcome Madame Guillotine herself!

[BOWS ASIDE, WITH AN ARM OUTSTRETCHED]

> A horrible hag enters, cackling wildly. Half her face is obscured by a large frilly cap.

**Guillotine**:
[ENTERS, CACKLING, CARRYING A CLUB WITH SPIKES, APPEARS TO HAVE BLOOD ON HER ARMS; HER FACE IS OBSCURED BY HER BONNET, AND SHE APPEARS TO BE MISSING A FRONT TOOTH]
Are these the English pigs?

**Edmund**:
Yes, that's us.

**Guillotine**:
Leave them with me, Monsieur Ambassadeur.  I intend to torture them in a manner so unbearably gruesome, even you will not be able to stand it!

**Ambassador**:
I don't think I will have a problem, madame.

**Guillotine**:
No, you will be sick.

**Ambassador**:
What if I stay for the first few minutes, and then I leave if I'm feeling queasy?

**Guillotine**:
No, you will be sick immediately.

**Ambassador**:
What if I am sick quietly in a bag?  I mean, what is in your mind?

[GUILLOTINE WHISPERS IN THE Ambassador'S EAR. He gags, and removing HIS HAT AS HE LEAVES, VOMITs INTO IT.]

**Guillotine**:
[TURNS TO EDMUND] So! Scum!  Prepare to be in pain!

**Edmund**:
Yes, certainly.  But first, perhaps, a toast to your beauty!
[GIVES GUILLOTINE THE POISONED CUP]

**Guillotine**:
[TOSSES CLUB ASIDE]
> She dances forward, takes the cup and swigs.
Oh, thank you.  OK.

**Edmund**:
Cheers.

**Guillotine**:
So, I expect you were expecting to be rescued, huh?!

**Edmund**:
Hah -- some bloody hope.

> The reply comes in a perfect cut-glass English accent.

**Guillotine**:
[VOICE SUDDENLY A MALE VOICE] On the contrary! I'm just sorry I'm so late!

**Edmund**:
What!

> Mme Guillotine tears off her frilly cap and wig. She is not Mme Guillotine at all-- she is Lord Smedley

[GUILLOTINE REMOVES HER BONNET, REVEALING HERSELF TO BE LORD SMEDLEY]

**Smedley**:
Yes, gentlemen, I have come to take you to freedom!

**Baldrick**:
Hurray!

**Edmund**:
My God!  Smedley!  But I thought you were an absolute fathead!

**Smedley**:
No -- just a damn fine actor!  Thank God I got here before you took any of those awful suicide pills!

**Edmund**:
[LOOKS DOWN AT THE CUPS] Errrrrr, yes... I suppose if someone had taken one and wished that he hadn't, he'd be able to do something about it?

**Smedley**:
No, no -- they're very odd things, you see. The symptoms are most peculiar. First of all, the victims become very, very depressed.  [SITS ON THE BED, FACE IN HIS HANDS] Oh, god!  [NEAR TO TEARS] This whole revolution is so depressing, I mean, sometimes I wonder why I bother... I mean, I'm so lonely, and nobody loves me...

**Edmund**:
And after the depression comes death.

> Smedley swaps back with irrational fury.

**Smedley**:
No -- after the depression comes [JUMPS OFF THE BED AND GRABS EDMUND'S LAPELS, SHOUTING] the loss of temper, you stuck-up bastard!!!
[TURNS TO BALDRICK] What you are staring at???
[PUNCHES BALDRICK]

**Edmund**:
And after the *temper* comes death.

**Smedley**:
No! After the temper comes the, err... comes the, err...

**Edmund**:
Forgetfulness?

**Smedley**:
Er, yes, that's it... err... comes the, err...

**Edmund**:
Forgetfulness.

**Smedley**:
Yes, yes.  Right in the middle of a... of a... thingy... you completely forget what it was you... ooh, nice pair of shoes!

**Edmund**:
And after the forgetfulness, you die.

**Smedley**:
Oh, no! I forgot one!  After the forgetfulness comes a moment of exquisite happiness! [LAUGHS, JUMPS UP AND DOWN, WAVING HIS ARMS IN THE AIR]

Jumping up and down, and waving your arms in the air, and knowing that in a minute we're all going to be free! free!! free!!!

**Edmund**:
[GETTING TIRED OF THIS]
And *then* death?

**Smedley**:
No -- you jump into a corner first.
[JUMPS INTO A CORNER; DIES]
> He does and falls down dead in a dark corner.

**Baldrick**:
Hurray!  It's the Scarlet Pimpernel!

**Edmund**:
Yes, Baldrick.

**Baldrick**:
And you killed him!

**Edmund**:
Yes, Baldrick.  I mean, what's the bloody point of being the Scarlet Pimpernel if you're going to fall for the old poisoned-cup routine?  Scarlet Pimpernel, my foot!  Scarlet Git, more like it!

[SEES THAT THE DOOR IS STILL AJAR]

But wait!  Here's our chance to escape!  Come on, quick!

**Baldrick**:
But what about Mr Frou?

**Edmund**:
Oh, forget Frou Frou. I wouldn't pick my nose to save his life. Now, come on.

[BEGINS TO EXIT, BUT RUNS INTO FROU FROU.]
> They run in to the corridor. And bump straight into Frou Frou.

Ah! Frou Frou, my old friend and comrade, w-what are you doing here?

**Frou Frou**:
I escaped!  What happened here?

**Edmund**:
[GUILTY] Oh, er, nothing, nothing.

**Frou Frou**:
Oh, I thought for a moment the Scarlet Pimpernel had saved you...!

[EDMUND CHUCKLES NERVOUSLY; LOOKS AT -- AND NUDGES -- BALDRICK.]

**Baldrick**:
[VERY BADLY FAKES A LAUGH.] Ha! Ha!

## SCENE 13: PRINCE GEORGE'S CHAMBERS

> Blackadder, Frou Frou and Baldrick Enter. PrinceGeorge is doing up his trousers-- the same trousers, back-to-front.

**Prince**:
Ah, chaps!  Good to see you.  Just trying on the new trousers

**Edmund**:
I return, Sir, as promised, plus one toff French aristocrat fresh from the Bastille.

**Prince**:
[AS FROU FROU BOWS] Ah! Please to meet you, *monsieur*.  Do sit down.

**Frou Frou**:
*Enchantée*.

**Prince**:
Damn sorry about the revolution and all that caper -- most awfully bad luck.  [TO EDMUND] So, tell me, Blackadder: how the devil did you get him out?

**Edmund**:
Sir, it is an extraordinary tale of courage and heroism which I blush from telling by myself, but seeing as there's no one else..

**Baldrick**:
I could try.

**Edmund**:
[BAPS BALDRICK ON THE BACK OF THE HEAD] We left England in good weather, but that was a far as our luck held.  In the middle of Dover Harbour, we were struck by a tidal wave.  I was forced to swim to Boulogne with the unconscious Baldrick tucked into my trousers.  Then, we were taken to Paris, where I was summarily tried and condemned to death, and then hung by the larger of my testicles from the walls of the Bastille. It was then that I decided I had had enough.

> The prince is completely engrossed.

**Prince**:
Bravo!

**Edmund**:
So, I rescued the count, killed the guards, jumped the moat, ran to Versailles -- where I climbed into Mr Robespierre's bedroom, leaving him a small tray of milk chocolates and an insulting note.  The rest was easy.

**Prince**:
That is an incredible story -- worthy of the Scarlet Pimpernel himself!

**Edmund**:
Well, I wouldn't know.

**Frou Frou**:
I, on the other hand, would.  [HE STANDS UP] Because, you see, Sir...

[REMOVES GLASSES, WIG AND FALSE NOSE, REVEALING HIMSELF TO BE LORD TOPPER]

> He takes off his wig and glasses with a flourish. Blackadder closes his eyes in startlement. Frou Frous takes off his false nose. Baldrick looks amazed. It is Lord Topper.

*I* am the Scarlet Pimpernel.

**Edmund**:
Uh—oh!

**Baldrick**:
Hurray!

**Prince**:
[STANDING] Good lord.  Topper!

**Topper**:
Yes, Your Highness.

**Prince**:
Well, by gads and by jingo with dumplings, steak and kidneys, and a good solid helping of sprouts!  I can't believe it! *You're* the fellow who has single-handedly saved all those damned Frenchies from the chop?

**Topper**:
Not quite single-handedly, Sir.  I operated with the help of my friend, Smedley, but he seems to have disappeared for the moment, slightly mysteriously.

[BALDRICK GETS READY TO SAY SOMETHING.]

**Edmund**:
Shut up, Baldrick.

**Baldrick**:
Yes, Mr.Blackadder.

**Prince**:
So... So Blackadder rescued the Scarlet Pimpernel!

**Topper**:
No, Sir, he did not.

**Prince**:
Eh?

**Topper**:
Prepare yourself for a story of dishonour and deceit that will make your stomach turn.

**Prince**:
Well, I say!  [FASCINATED;TO EDMUND] This is interesting, isn't it, Blackadder?

[EDMUND NODS SLOWLY.]

**Topper**:
Not only that [TURNING AND WALKING TOWARD EDMUND], but I trust it will lead to the imprisonment of a man who is a liar, a bounder, and a cad.

[BALDRICK TURNS TO LOOK, WITH TOPPER, AT EDMUND.]

[EDMUND TURNS TO LOOK BEHIND HIMSELF.]

> He staes at Blackadder. Blackadder looksbehind him, but he's not joking his way out of this one.

**Prince**:
Well, bravo! Bcause we hate liars, bounders and cads, don't we, Blackadder?

**Edmund**:
Generally speaking...yes, Sir.  [BEGINS TO SERVE DRINKS] But perhaps before Lord Topper starts to talk, he might like a glass of wine.  [HE HAS DROPPED A SUICIDE PILL INTO TOPPER'S GLASS] He's looking a little shaken.

> Blackadder goes to the tray. There is the pleasant plop of two suicide pills. Blackadder returns and looks on benignly as Topper quaffs his wine.

**Topper**:
[TAKING THE GLASS] Shaken, but not stirred.  [DRINKS]

[GIVES GLASS BACK TO EDMUND, WHO SNIFFS IT]
[TURNS TO PRINCE] It all began last week.  I was sitting in Mrs Miggins's Coffee Shop when... oh, God!  
[HOLDS HEAD IN HIS HANDS]
All this treachery is so depressing...  
[SHOUTS] I mean, the whole thing just makes you incredibly angry!!!
[SWINGS AT BALDRICK, MISSING; BALDRICK FALLS OVER ANYWAY]
And it just makes you want to...

> He grabs Prince George and starts shaking him. But suddenly he becomes forgetful.

Oh, that's a nice waistcoat, Your Majesty... err...I'm sorry; I've completely forgotten what I was talking about.

**Edmund**:
[GRINNING] Erm, a story of dishonour and deceit...

**Topper**:
[EXTREMELY HAPPY] Oh! That's a great story!  That's great!! Oh, that's a WONDERFUL STORY!!!  Let me just jump into this corner first.

[JUMPS INTO CORNER AND DIES]

**Prince**:
[STANDING] Roast my raisins!  He's popped it!  I say, Blackadder, do you think he really was the Scarlet Pimpernel?

**Edmund**:
Well, judging from the ridiculous ostentatiousness of his death, I would say that he was.

**Prince**:
Well, then, that's a damn shame, because I wanted to give him this enormous postal order.  [HOLDS IT UP]

**Edmund**:
Please, Sir, let me finish.  I would say that he was...*n't*.
[DEEPLY CONCENTRATING NOW] You see, the Scarlet Pimpernel would never ever reveal his identity -- that's his great secret. So, what you're actually looking for is someone who has, say, just been to France and rescued an aristocrat, but when asked
"Are you the Scarlet Pimpernel?" he replies, "Absolutely not," Sir.

**Prince**:
But, wait a minute! Blackadder, *you've* just been to France, and you've rescued a French aristocrat...  Oh, Blackadder! Are you the Scarlet Pimpernel?

**Edmund**:
Absolutely not, Sir.

**Baldrick**:
Hurray!

[PRINCE, TOO EXCITED FOR WORDS, HANDS THE POSTAL ORDER TO EDMUND, WHO ALREADY HAS HIS HAND WAITING TO TAKE IT.]

[END CREDITS BEGIN]

-------------------------------------------------------------------------------

                                 For the
                        BENEFIT of SEVERAL VIEWERS
                         MR. CURTIS & MR. ELTON'S
                            Much admir'd Comedy
                           B L A C K   A D D E R
                             T h e   T H I R D
                                    OR
                            INK and INCAPABILITY
             was performed with appropriate Scenery Dresses etc.
                                    by
                             EDMUND BLACKADDER,
                           butler to the Prince,
                            Mr. ROWAN ATKINSON
                   Baldrick, a dogsbody, Mr. TONY ROBINSON
              The Prince Regent, their master, Mr. HUGH LAURIE
              Dr. Samuel Johnson, noted for his fat dictionary,
                            Mr. ROBBIE COLTRANE
                     Mrs. Miggins, a coffee shoppekeeper,
                         Miss. HELEN ATKINSON-WOOD
               Lord Topper, \              Mr. TIM McINNERNY 
                              > fops
               Lord Smedley, /             Mr. NIGEL PLANER 
           Ambassasor, a fearsome revolutionary, Mr. CHRIS BARRIE

             MUSIC [NEVER PERFORM'D BEFORE], Mr. HOWARD GOODALL

                 designer of graphics, Mr. GRAHAM McCALLUM
                    buyer of properties, Miss. JUDY FARR
                 designer of costumes, Miss. ANNIE HARDINGE
                  designer of make-up, Miss. VICKY POCOCK
                    mixer of vision, Miss. ANGELA WILSON
                    supervisor of cameras, Mr. RON GREEN
                  editor of videotape, Mr. CHRIS WADSWORTH
                   director of lighting, Mr. RON BRISTOW
               co-ordinator of technicalities, Mr. JOHN LATUS
                  supervisor of sound, Mr. PETER BARVILLE
               assistant to production, Miss. NIKKI COCKCROFT
               assistant manager of floors, Mr. DUNCAN COOPER
                  manager of production, Miss. OLIVIA HILL
                      the designer, Mr. ANTONY THORPE

                    the director, Miss. MANDIE FLETCHER

                          the producer, Mr. LLOYD

                To conclude with Rule Britannia in full chorus
                             NO MONEY RETURN'D
                            [C] BBC  MCMLXXXVII
