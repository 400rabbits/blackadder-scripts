Title:      Blackadder III: Episode 5: Amy and Amiability
Credit:     written by
Author:     Richard Curtis AND Ben Elton
Credit:     directed by
Director:   Mandie Fletcher
Source:     NULL
Notes:      UNOFFICIAL TRANSCRIPTION FROM AIRED SHOW
Original air date: 15 october 1987
Copyright:   (c) 1987, British Broadcasting Corporation
Summary:   When Blackadder discovers that the Prince Regent is completely broke, he devises a plan to restore his fortunes through marriage.

# Blackadder III: Episode 5: Amy and Amiability

## DRAMATIS PERSONAE

E: Edmund Blackadder
B: Baldrick
G: Prince George
M: Mrs. Miggins
A:  Miss Amy Hardwood
DC: Duke of Cheapside
Mr. Hardwood
SC: Sally Cheapside
DC: The Duke of Cheapside
S:  The Shadow

## SCENE 1: THE KITCHEN

> Blackadder sits at breakfast and is overwhelmed by unfriendly mail. Baldrick is plucking a goose.

**Blackadder**
Oh God! Bills, bills, bills. One is born, one runs up bills, one dies!

And what have I got to show for it? Nothing. A butler's uniform and a slightly effeminate hairdo! Honestly Baldrick, I sometimes feel like a pelican
— whichever way I turn, I've still got an enormous bill in front of me. Pass the biscuit barrel.  [BALDRICK DOES SO] Let's see what's in the kitty shall we?  [SHAKES OUT A FEW COINS] Nine-pence! Oh God, what are we going to do?

**Baldrick**
Don't worry Mr B., I have a cunning plan to solve the problem.

**Blackadder**
Yes Baldrick. Let us not forget that you tried to solve the problem of your mother's low ceiling by cutting off her head.

**Baldrick**
But this is a really good one. You become a dashing highwayman, then you can pay all your bills and, on top of that, everyone'll want to sleep with you.

**Blackadder**
Baldrick, I could become a prostitute and pay my bills, and everyone would want to sleep with me— but I do consider certain professions beneath me. But besides which, I fail to see why a common thief should be idolized, just because he has a horse between his legs.

**Baldrick**
My favourite's The Shadow.

[ADMIRINGLY] What a man! They say he's half-way to being the new Robin Hood.

**Blackadder**
Why only half-way?

**Baldrick**
Well he steals from the rich, but he hasn't got round to giving it to the poor yet. Look! I've got a poster of him.

> He produces a poster of a man in a hat and a kerchief. It reads:

```
     WANTED FOR HANGING
        THE SHADOW
       REWARD £1,000
```

**Blackadder**
Baldrick, I have no desire to get hung for wearing a silly hat. If I want to get rich quick, all I have to do is go upstairs and ask Prince Fathead for a rise.

> A ring on the bell.

**Blackadder**
Oop! The bank's open!

## SCENE 2: PRINCE GEORGE'S CHAMBERS
>
Prince George is sitting at his desk— it is also a mess of papers.

**Blackadder**
Good morning, Sir. May I say how *immensely* rich you're looking? Now, was there anything you wanted? Anything at all? Absolutely anything?

**Prince**
Well yes, old fellow, I was wondering if you could possibly lend me a bit of cash.

**Blackadder**
But of course, Sir. I— cash?

**Prince**
Yes, I'm rotten stinking stony stinking broke!

**Blackadder**
But, Sir! What about the five thousand pounds that Parliament voted you only last week to drink yourself to death with?

**Prince**
All gone I'm afraid. You see, I've discovered this terrifically fun new game. It's called "cards". What happens is, you sit round the table, with your friends, and you deal out five "cards" each, and then the object of the game is to give away all your money as quickly as possible. Do you know it?

**Blackadder**
Vaguely, Sir, yes.

**Prince**
All the chaps say I'm terrific at it.

**Blackadder**
I seem to remember I was very bad at it. I always seemed to end up with more money than I started with.

**Prince**
Yes, well, it's all down to practise. I'm a natural, apparently. The only drawback, of course, is that it's pretty damned expensive. So, basically, I was wondering if you could lend me a couple of hundred.

**Blackadder**
I'm afraid that's impossible, Sir. I'm as poor as a church mouse that's just had an enormous tax bill on the very day his wife ran off with another mouse, taking all the cheese.

**Prince**
Well, what am I going to do?

**Blackadder**
Yes, it's a difficult one.

**Prince**
Hmm.

**Blackadder**
Let's see now. You can't borrow money, you're not going to inherit any money, and obviously you can't earn money. Sir, Sir, drastic situations call for drastic measures. If you can't make money, you'll have to marry it.

**Prince**
Marry? Never! I'm a gay bachelor, Blackadder. I'm a roarer, a rogerer, a gorger and a puker! I can't marry, I'm young, I'm firm buttocked, I'm...

**Blackadder**
Broke?

**Prince**
Well, yes, I suppose so.

**Blackadder**
And don't forget, Sir, that the modern Church smiles on roaring and gorging within wedlock. And indeed rogering is keenly encouraged.

**Prince**
And the puking?

**Blackadder**
Mmm, I believe still very much down to the conscience of the individual church-goer.

**Prince**
Well yes, tally-ho then Blackadder. Yes, you fix it up. You know the kind of girls I like, they've got to be lovers, laughers, dancers...

**Blackadder**
And bonkers!

**Prince**
That goes without saying!

## SCENE 3: THE KITCHEN

> Blackadder is consulting a large tome. Baldrick is gutting a chicken. Blackadder slams the book shut.

**Blackadder**
Oh God!

**Baldrick**
Something wrong, Mr B.?

**Blackadder**
I can't find a single person suitable to marry the prince.

**Baldrick**
Oh please keep trying. I love a royal wedding. The excitement, the crowds, the souvenir mugs, the worrying about whether the bride's lost weight.

**Blackadder**
Unlikely with this lot, I'm afraid. If the prince had stipulated "must weigh a quarter of a ton", we'd be laughing. Of the 262 princesses in Europe, 165 are over 80, they're out, 47 are under 10, they're out, and 39 are mad.

**Baldrick**
Well they sound ideal.

**Blackadder**
Well they would be if they hadn't all got married last week in Munich to the same horse. Which leaves us with two.

**Baldrick**
And what about them?

**Blackadder**
Well, there's Grand Duchess Sophia of Turin. We'll never get her to marry him.

**Baldrick**
Why not?

**Blackadder**
Because she's *met* him.

**Baldrick**
Which leaves?

**Blackadder**
Caroline of Brunswick as the only available princess in Europe.

**Baldrick**
And what's wrong with her?

**Blackadder**
[SUDDENLY SHOUTING]

"Get more coffee! It's horrid! Change it! Take me roughly from behind! No, not like that, like this! Trousers off! Tackle out! Walk the dog! Where's my presents?"

**Baldrick**
> Baldrick is very shaken by this outburst.

All right! Which one do you want me to do first?

**Blackadder**
No, that's what Caroline's like. She is famous for having the worst personality in Germany. And as you can imagine, that's up against some pretty stiff competition.

**Baldrick**
So you're stuck then.

**Blackadder**
Yes, I'm afraid I am. Unless, oh unless! Pass me the paper Baldrick, quick.

[HE OPENS THE PAPER]

Baldrick, why has half the front page been cut out?

**Baldrick**
I don't know.

**Blackadder**
You do know, don't you?

**Baldrick**
Yes.

**Blackadder**
You've been cutting out the cuttings about the elusive Shadow to put in your highwayman's scrapbook haven't you?

**Baldrick**
Oh, I can't help it Mr B. His life is so dark and shadowy and full of fear and trepidation.

**Blackadder**
So is going to the toilet in the middle of the night, but you don't keep a scrapbook on it.

**Baldrick**
 [SURPRISED]
 I do.

**Blackadder**
Let's see. Now let's see, society pages. You see, it needn't necessarily be a princess. All the Prince wants is someone pretty and rich.

**Baldrick**
Oh dear, that rules me out then.

**Blackadder**
Now, let me see. "Beau Brummel in purple pants probe." "King talks to tree. Phew what a loony!" God, *The Times* has really gone downhill recently hasn't it! Aha. Listen to this, listen to this: "Mysterious Northern beauty, Miss Amy Hardwood, comes to London and spends flipping great wadges of cash!" That's our baby!

## SCENE 4: PRINCE GEORGE'S CHAMBERS

> Blackadder is brushing down the prince's coat.

**Prince**
Honestly Blackadder, I don't know why I'm bothering to get dressed. As soon as I get to the Naughty Hellfire Club I'll be debagged and radished for non-payment of debts.

**Blackadder**
"Radished", Sir?

**Prince**
Yes, they pull your breeches down and push a large radish right up your—

**Blackadder**
Yes, yes, yes, all right. There's no need to hammer it home.

**Prince**
Well as a matter of fact they do often have to...

**Blackadder**
No, no! No! Your em, your money worries are, are, are over, Sir.

**Prince**
Well hoorah for that!

**Blackadder**
I have found you a bride. Her name is Amy, daughter of the noted industrialist, Mr Hardwood.

**Prince**
Oh dammit, Blackadder, you know I loathe industrialists. Sad, balding, little proles in their "damn your eyes" weskits. All puffed up just because they know where to put the legs on a pair of trousers.

**Blackadder**
Eh, believe me, these people are the future. This man probably owns half of Lancashire. His family's got more mills than, than you've got brain cells.

**Prince**
How many mills?

**Blackadder**
Seven, Sir.

**Prince**
Quite a lot of mills then.

**Blackadder**
Yes. He has patented a machine called "The Ravelling Nancy".

**Prince**
Mmm, what does it do?

**Blackadder**
It ravels cotton, Sir.

**Prince**
What for?

**Blackadder**
That I cannot say, Sir. I am one of these people who are quite happy to wear cotton, but have no idea how it works. She is also a beauty, Sir.

**Prince**
Well if she's gonna be my bird, she'd better be! Right, so what's the plan?

**Blackadder**
Well I thought I could take her a short note expressing your honourable intentions.

**Prince**
Yes, yes, I think so too. All right then, well take this down. Eh, "From His Royal Highness, the Prince of Wales to Miss Amy Hardwood. Tally-ho my fine, saucy young trollop! Your luck's in! Trip along here with all your cash, and some naughty night attire, and you'll be staring at my bedroom ceiling from now till Christmas, you lucky tart! Yours with the deepest respect etc, signed George. PS Woof woof!" Well, what do you think?

**Blackadder**
It's very *moving* sir. Would you mind if I change just one tiny aspect of it?

**Prince**
Which one?

**Blackadder**
The words.

**Prince**
Oh yes, I'll, I'll, I'll leave the details to you Blackadder. Just make  sure she knows I'm all man... with a bit of animal thrown in. Rrrrgh!

**Blackadder**
Certainly, Sir.  

[SCORES OUT THE PRINCE'S LETTER]

## SCENE 5: AMY HARDWOOD'S ROOM

> The room is very fluffy and pretty. Blackadder is reading to Amy, who is a total drip. It is not quite the same letter.

**Blackadder**
"From his Royal Highness, the Prince of Wales to Miss Amy Hardwood:— The upturned tilt of you tiny, wee nosey, smells as sweet as a great big posy." Fanciful stuff of course madam, but, but from the heart.

**Amy**
He says my nosey is tiny ?

**Blackadder**
And wee, madam.

**Amy**
Wel,l he must be an awful clever clogs, because you see, my nosey is tiny, and so wee, that I sometimes think the pixies gave it to me!

> She gives a tiny tinkling laugh.
> Blackadder is stony-faced. Not really his type of woman.

**Blackadder**
He continues... "Oh Lady Amy, queen of all your sex"... I apologize for the word, Madam, but Prince George is a man of passion.

**Amy**
Oh, don't worry, I can get pretty cross myself sometimes. Tell me, Mr. Blackadder, I've heard a teensy rumour that the Prince has the manners of a boy cow's dingle-dangle. What do you have to say to that?

**Blackadder**
Oh, that is a lie, Madam. Prince George is shy and just pretends to be bluff and crass and unbelievably thick and gittish, whilst deep down he is a soft little marshmallowy, pigletty type of creature.

**Amy**
Oh I'm so glad, because you see, I'm a delicate tiny thing myself, weak and silly and like a little fluffy rabbit. So I could never marry a horrible heffalump, or I might get squished. Tell me, when can I meet the lovely Prince?

**Blackadder**
 [SURPRISED]
 You want to meet him?

**Amy**
Well if we're going to get married I think I probably ought to. I know! Tell him to come and serenade me tonight. I'll be on my balcony in my jim-jams.

**Blackadder**
Certainly madam.

> He is about to go when Mr Hardwood enters. A definitively bluff Northerner.

**Hardwood**
Eh up! Who's this big girl's blouse, then ?

**Amy**
Father, this is Mr. Blackadder, he's come a-wooing from the Prince.

**Blackadder**
You have a beautiful and charming daughter, Sir.

**Hardwood**
Indeed I do. I love her more than any pig, and that's saying summat!

**Blackadder**
It certainly is.

**Hardwood**
And let me tell you, I'd no more place her in the hands of an unworthy man than I'd place my John Thomas in the hands of a lunatic with a pair of scissors.

**Blackadder**
An attitude that does you credit, Sir.

**Hardwood**
I'd rather take off all my clothes and paint my bottom blue than give her to a man who didn't love her!

**Blackadder**
What self-respecting father could do more?

**Hardwood**
On the other hand, if he's a prince, he can have her for ten bob and a pickled egg.

**Blackadder**
I can see where your daughter gets her ready wit, Sir.

**Hardwood**
I thank you.

**Blackadder**
Although where she gets her good looks and charm is perhaps more of a mystery.

**Hardwood**
No one ever made money out of good looks and charm.

**Blackadder**
You obviously haven't met Lady Hamilton, Sir.

[HE BOWS SLIGHTLY AND LEAVES]

# SCENE 6: THE KITCHEN

> Baldrick is stuffing a chicken whilst Blackadder wanders around the room behind him.

**Blackadder**
I tell you, Baldrick, I'm not looking forward to this evening. Trying to serenade a light fluffy bunny of a girl in the company of an arrogant half-German yob with a mad dad.

**Baldrick**
Well, he is the Prince of Wales.

**Blackadder**
Have you ever been to Wales, Baldrick?

**Baldrick**
No, but I've often thought I'd like to.

**Blackadder**
Well don't. It's a ghastly place. Huge gangs of tough, sinewy men roam the valleys terrifying people with their close-harmony singing. You need half a pint of phlegm in your throat just to pronounce the place-names. Never ask for directions in Wales, Baldrick— you'll be washing spit out of your hair for a fortnight.

**Baldrick**
So, eh, being Prince of it isn't considered a plus?
[HAMMERS A LARGE ORANGE INTO THE CHICKEN]

**Blackadder**
I fear not, no. But the crucial thing is that they must never be left alone together before the marriage.

**Baldrick**
But isn't that a bit unfair on her?

**Blackadder**
Well it's not exactly fair on him either. The girl is wetter than a haddock's bathing costume. But you know, Baldrick, the world isn't fair. If it was, things like this wouldn't happen would they?

[HITS BALDRICK AROUND THE BACK OF THE HEAD]

## SCENE 7: THE HARDWOOD'S BALCONY

> The Prince and Blackadder are hiding behind some bushes underneath the balcony. They speak in whispers.

**Prince**
All right, so what's the plan? Shin up the drain and ask her if she'll take delivery of a consignment of German sausage?

**Blackadder**
No,  Sir, As we rehearsed, poetry first, sausage later.

**Prince**
Right. So what do you think? "Harold the Horny Hunter" should do the trick.

**Blackadder**
Just remind me of it, Sir?

**Prince**
 [LOUDLY] "Harold the Horny hunter, had an enormous horn..."

 > He'd love to finish this delicate masterpiece, but Blackadder corrects him..

**Blackadder**
Shh. Yes, yes. It is absolutely excellent, Sir, however, might I suggest an alternative?

> Blackadder takes the lyrics from his pocket and hands them to Prince George.

**Prince**
"Lovely little dumpling,
How in love I am.
Let me be your shepardkins,
You can be my lamb."

Well, I think we'll be very lucky if she doesn't just come out onto the balcony and vomit over us, but still, let's give it a whirl.

**Blackadder**
Just stand right here, Sir. Right. Call for her romantically.

**Prince**
Right.  

> Steadies himself— then with huge and hideous volume...

OI! COME ON OUT HERE, YOU ROLLICKING, TROLLOPING SAUCE BOTTLE!

**Amy**
George?

**Prince**
WOOF WOOF!

> Blackadder clamps his hand over Prince George's mouth and pushes him behind a stone plant pot. Amy comes out on to the balcony.

**Amy**
Is that you?

> Blackadder feels he has to intercede.

**Blackadder**
Y-y-yes, yes. 'Tis I, your gorgeous little love bundle.

**Amy**
Oh George, I think you must be the snuggly wuggliest lambkin in the whole of Toyland!

**Prince**
Yuch!  

[BLACKADDER SILENCES HIM AGAIN]

**Amy**
What was that?

**Blackadder**
Am, em. Nothing, there was just a little fly in my throaty. Yuch! Yuchh!

**Amy**
Do you want a hanky-wanky to gob the phlegmy-wemmy-woo into?

> Amy takes a handkerchief from her bosom.

**Prince**
Phwoah! Crikey!

**Amy**
Oh, what was that? Is there someone down there with you?

**Blackadder**
No, no, no, it was just the wind whistling through the trees and making a noise that sounded like "phwoaaaah...crikeeeeee".

**Amy**
Oh joy! Then come Prince Cuddly Kitten, climb up my ivy.

> Prince George stands up and stride towards the balcony.

**Prince**
Sausage time!

**Amy**
There *is* someone down there with you!

**Blackadder**
Oh my God, yes! Yes, so there is, a filthy intruder spying on our love.

**Amy**
Oh hit him, George! Hit him!

**Blackadder**
Very well.  
[WHISPERS TO THE PRINCE]
Would you mind screaming, Your Highness.

[LOUDLY]

Take that.  [PUNCHES HIM IN THE FACE]

And that!  [KNEES HIM IN THE GROIN]

And that!

[HITS HIS BACK; THE PRINCE FALLS TO THE GROUND]

**Amy**
Oh, oh, oh, you're so brave! And I'm so worn out with all the excitement that I'd better go sleepy-bobos, otherwise I'll be all cross in the morning. Nighty-night Georgy Porgy!

**Blackadder**
Nighty-wighty Amy-wamy.

> She goes in. Blackadder helps the prince up.

I think it worked, Sir. In the morning I shall go in and ask her father. You go out and start spending his money. I can't stand meanness when it comes to wedding presents. And well done, Sir, you were brilliant.

**Prince**
Was I?

**Blackadder**
Yes, Sir.

**Prince**
But I'm in agony!

**Blackadder**
Well, that's love for you.

## SCENE 8: AMY HARDWOOD' ROOM

> Blackadder stands before Mr Hardwood.

**Blackadder**
Sir, I come as emissary of the Prince of Wales with the most splendid news. He wants your daughter, Amy, for his wife.

**Hardwood**
Well his wife can't have her! Outrageous, Sir, to come here with such a suggestion!  [STANDS UP ANGRILY]

Mind, Sir, or I shall take off my belt... and by thunder me trousers will fall down!

**Blackadder**
No, Sir. Sir, you misunderstand. He wants to marry your lovely daughter.

**Hardwood**
Ah, ah.

[FALLS BACK INTO HIS CHAIR, AMAZED]

Can it be possibly true? Surely love has never crossed such boundaries of class?  

[CLUTCHES AMY'S HAND]

**Amy**
But what about you and Mum?

**Hardwood**
Well, yes, yes— I grant thee when I first met her I was the farmer's son, and she was just the lass who ate the dung. But that was an exception.

**Amy**
And Aunty Dot and Uncle Ted.

**Hardwood**
Yes, yes alright— he was a pig poker and she was the Duchess of Argyle, but...

**Amy**
And Aunty Ruth and Uncle Isiah. She was a milkmaid and he was...

**Hardwood**
The Pope! Yes, yes, all right. Don't argue. Suffice it to say if you marry we need never be poor or hungry again. Sir, we accept.

**Blackadder**
Good. So obviously you'll be wanting an enormous cer-e-mon-y... What did you say?

**Hardwood**
Well obviously, eh, now we're marrying quality... we'll never be poor or hungry again.

**Blackadder**
Meaning that you're poor and hungry at the moment?

**Hardwood**
[WITH FEELING]

Oh, yes! We've been living off lard butties for five years now. I'm so poor I use my underpants for drying dishes.

**Blackadder**
So, you're skint?

**Hardwood**
Aye.

**Blackadder**
Well, in that case, the wedding's off. Good day.

**Amy**
Oh but what about Georgy's lovey-wovey poems that won my hearty-wearty?

**Blackadder**
All writteny-witteny by mee-wee, I'm afraidy-waidy. Goodbye.

> He exits with callous abruptness.

**Amy**
Aaahh!

> She beats the door in love-lorn despair.

## SCENE 9: PRINCE GEORGE'S CHAMBERS

> Blackadder enters, worried.

**Blackadder**
Sir, you know I told you to go out and spend a lot of money on wedding presents, well appa-

> Blackadder looks around— the entire room is utterly full of golden clutter. The prince is idly inspecting a golden candlestick.

**Prince**
Yes?

**Blackadder**
Nothing.

## SCENE 10: THE KITCHEN

[BLACKADDER ENTERS, PUTTING ON A LARGE BLACK CAPE.]

**Blackadder**
Crisis Baldrick! Crisis! No marriage, no money, more bills! For the first time in my life I've decided to follow a suggestion of yours. Saddle Prince George's horse.

**Baldrick**
Oh, Sir, you're not going to become a highwayman, are you?

**Blackadder**
No, I'm auditioning for the part of Arnold the Bat in Sheridan's new comedy.

**Baldrick**
Oh, that's all right then.

**Blackadder**
Baldrick, have you no idea what irony is?

**Baldrick**
Yeah, it's like goldy and bronzy, only it's made of iron.

> Blackadder picks up a tricornered hat and puts it on.

**Blackadder**
Never mind, ever mind. Just saddle the prince's horse.

**Baldrick**
That'll be difficult, he wrapped it round that gas lamp in the Strand last night.

> Blackadder takes two pistols out of a box.

**Blackadder**
Well, saddle my horse then.

**Baldrick**
What do you think you've been eating for the last two months?

**Blackadder**
Well, go out into the street and hire me a horse.

**Baldrick**
Hire you a horse? For ninepence? On Jewish New Year in the rain? A bare fortnight after the dreaded horse plague of old London Town? With the blacksmith's strike in its fifteenth week and the Dorset Horse-Fetishists Fair tomorrow?

**Blackadder**
Right, well get this on then.

> He chucks a bit and blinkers to Baldrick.

It looks as though you could do with the exercise.

## SCENE11: A STAGECOACH

> A dark and stormy night on a remote highway. The Duke off Cheapside and his daughter, Sally, are inside the stagecoach.

**Sally**
Honestly Papa. Ever since Mother died you've tried to stop me growing up. I'm not a little girl, I'm a grown woman. In fact, I might as well tell you now Papa: I'm pregnant. And I'm an opium fiend, and I'm in love with a poet called Shelley who's a famous whoopsy, and mother didn't die, I killed her!

**Cheapside**
[CHEERILY] Oh, well, never mind.

> There is a shot

**Blackadder**
Stand and deliver!

[THE COACH STARTS TO PULL UP]

**Cheapside**
Oh no! Oh no no no no no! Disaster! It's the Shadow. We're doomed, doomed!

> Blackadder's head appears at the window of the coach.

**Blackadder**
Ah, good evening, Duke. And the lovely Miss Cheapside. Your cash bags please.  

[THE DUKE HANDS HIM A BAG OF MONEY]
There we are.

**Cheapside**
You'll never get away with this, you scoundrel, you'll be caught and damn well hung!

**Sally**
[FLIRTILY, TO CAMERA]
I think he looks pretty well-

**Blackadder**
Madam, please, no jests about me looking pretty well hung already, we have no time.

**Sally**
Pity.

**Blackadder**
Now, Sir, turn out your pockets.

**Cheapside**
Never, Sir. A man's pockets are his own private kingdom. I'll protect them with my life!

**Blackadder**
Oh I see, you've got something embarrassing in there, have you?
Perhaps a particularly repulsive handkerchief, hmm? One of those fellows who has a big blow and then doesn't change it for a week? Let's have a look shall we?

> He takes the handkerchief and pulls out a jewel

Aha!

**Sally**
Highwayman, I also have a jewel. I fear however that I have placed it here, beneath my petticoats, for protection.

**Blackadder**
Well in that case, Madam, I think I'll leave it. I'm not sure I fancy the idea of a jewel that's been in someone's pants. A single kiss of those soft lips is all I require.

**Cheapside**
Never, Sir! A man's soft lips are his own private kingdom. I shall defend them with my life.

**Blackadder**
I'm not talking to you, Grandad.

**Sally**
[KISSES HIM LONG AND HARD]

Oh, I'm overcome. Take me with you to live the life of the wild rogue, cuddling under haystacks and making love in the branches of tall trees!

**Blackadder**
Madam, sadly I must decline. I fear my horse would collapse with you on top of him as well as me!

**Baldrick**
[APPEARS NEXT TO BLACKADDER, WEARING HIS HARNESS]
I could try!

**Blackadder**
No, Quicksilver, you couldn't.

**Baldrick**
But that's not fair then. I've had you on my back for ten miles and I haven't even got a kiss out of it.

**Blackadder**
Oh, alright, very well then.

> Sally looks worried, but Blackadder kisses Baldrick.

All fair now?

**Baldrick**
Not really, no.

**Blackadder**
Chh! There's no pleasing some horses.

Hi-ho, Quicksilver!

**Baldrick**
Neiighh!

> They disappear.

**Sally**
[ACCUSINGLY]

Papa, you did nothing to defend my honour.

**Cheapsidw**
Oh, shut your face, you pregnant junky fag-hag!

## SCENE 12: GRASSY KNOLL IN A LEAFY COPSE

> Blackadder and Baldrick sit down to count the loot in a convenient copse.

**Blackadder**
Well, Baldrick, a good night's work I think. It's time to divide the loot. And I think it's only fair that we should share it equally.

**Baldrick**
Which I suppose is highwayman's talk for you get the cash, I get the snotty hanky.

**Blackadder**
No, no. No, we did this robbery together, so you get half the cash.

> He hands over one of the bags

**Baldrick**
Oh, thank you, Mr B.

**Blackadder**
*This* robbery, on the other hand, I'm doing alone.

> He aims the pistol at Baldrick's head.

Hand it over. Your money or your life!  

> Baldrick hands the bag back.

You see? All fair and above board.

**Baldrick**
Fair enough. As long as I haven't been cheated, I don't mind.

> At which point, suddenly out of the blue, another highwayman enters. It is the Shadow.

**Shadow**
Hands up! I am the Shadow and I never miss.

**Blackadder**
[VERY DEPRESSED] Oh, no.

**Shadow**
You, the one that looks like a pig.

**Blackadder**
He's talking to you, Baldrick.

**Shadow**
Skedaddle!

> He fires at Baldrick's feet as he scuttles off.

So, who have we here?

> He takes off Blackadder's tricorne hat.

Well! ! well set-up fellow indeed. Sir, a kiss.

**Blackadder**
Sorry, I'm not sure I heard that correctly.

**Shadow**
Oh dear, maybe your ears need unblocking.  

> He puts his pistol to Blackadder's head.

**Blackadder**
Oh, I see, a kiss! Oh, of course, of course, of course... and then perhaps a little light supper, some dancing, who, who knows where it might lead?

> So, The Shadow envelopes Blackadder in his cloak. A big kiss it is, during which the Shadow pulls off his hat. Long, golden hair cascades down. He is a she, and she is Amy Hardwood.
At the end of the kiss, she removes her mask.

**Blackadder**
Good lord! It's you!

**Shadow**
[DEEP SHADOW VOICE]
Of course.

**Blackadder**
But your voice, it's-

**Amy**
[NORMAL VOICE]
Clever, isn't it?

**Blackadder**
Does your father know you're out?

**Amy**
He had to go.

**Blackadder**
You mean he's dead?

**Amy**
Yes, dead as that squirrel!

**Blackadder**
Which squirrel?  

> She shoots at a tree. There is a little squak, followed by a little thud.

Oh, *that* squirrel. Of course, you killed him for ruining your chances of marrying Prince George.

**Amy**
[SCOFFS]
Huh, I despise the Prince. Don't you know it's you I want? I want a real man. A man who can sew on a button. A man who knows where the towels are kept.

> Blackadder smiles bashfully.

And yes— I crave your fabulous sinewy body.

**Blackadder**
Well, you're only human.

**Amy**
Here's the plan, Brown Eyes: you rob the Prince of everything he's got, right down to the clothes he's standing in. I'll get my stash and meet you here... and then we'll run away to the West Indies!

**Blackadder**
Well, I don't know I'll have to think about it.

[NO PAUSE]

I've thought about it, it's a brilliant plan. I'll see you here tomorrow.

[AMY SHOOTS ANOTHER SQUIRREL - "EEP", THUD.]

SCENE 13: THE KITCHEN
> Blackadder is packing silver into a chest.

**Blackadder**
Right, I'm off.

**Baldrick**
Oh, Sir, but what about the danger? Look, the reward is going up day by day.

> He reveals a poster which now reads:

```
     WANTED FOR HANGING
        THE SHADOW
       REWARD £5,000
```

**Blackadder**
Pah! I laugh in the face of danger. I drop ice cubes down the vest of fear. Things couldn't be better, Baldrick. She'll get me abroad and make me rich, then I'll probably drop her and get two hundred concubines to share my bed.

**Baldrick**
Won't they rather prickly?

**Blackadder**
Concubines, Baldrick, not porcupines.

**Baldrick**
Oh. I still can't believe you're leaving me behind.

**Blackadder**
Oh, don't you worry. When we're established on our plantation in Barbados, I'll send for you. No more sad little London for you, Balders, from now on you will stand out in life as an individual.

**Baldrick**
Will I?

**Blackadder**
Well of course you will, all the other slaves will be black.

> As Blackadder starts to leave with his chests of silver he bumps into Mrs Miggins rughing in

**Miggins**
Oh! Mr Blackadder. Oh, what's all this I hear about you buying a bathing costume and forty gallons of coconut oil? Are you going abroad then, Sir?

**Blackadder**
Yes, I'm off.

**Miggins**
Oh, Sir! What a tragic end to all my dreams. And I'd always hoped that you'd settle down and marry me and that together we might await the slither of tiny Adders.

> She catches hold of him and sobs into his waitcoat.

**Blackadder**
Mrs M., if we were the last three humans on Earth, I'd be trying to start a family with Baldrick!

[MRS. MIGGINS SCREAMS AND CRIES.]

> That cuel remark breaks her heart.

## SCENE 14: GRASSY KNOLL IN A LEAFY COPSE

> It is dawn at the same leafy location as the night before. Blackadder wheels his cart in front of him.

**Blackadder**
Well, here I am, all packed and ready to go.

**Amy**
Darling! I'm so pleased to see you, and I've got a little surprise for you. Close your eyes and open your mouth.

**Blackadder**
[EXPECTING A KISS]
Mmmm.

> Blackadder opens his mouth and she puts her pistol in it.

**Amy**
Ha ha! Hand over the loot, goat brains!

**Blackadder**
Ha ha ha ha ha!. I, I always said the bedrock of a good relationship was being able to laugh together. Good, well done. So, which way to Barbados?

**Amy**
You're not going to Barbados. Get away from that cart, Mr Slimey, or I'll fill you so full of lead we could sharpen your head and call you a pencil.

**Blackadder**
This is turning into a really rotten evening.

**Amy**
[SHE BEGINS TO TIE HIM UP.]
Yes, well you better make the most of it— Because it's your last. And it's a pity, because it's usually against my principles to shoot dumb animals.

**Blackadder**
Except squirrels?

**Amy**
Yes! Bastards!

> She looks wildly about her.

I hate them with their long tails and their stupid twitchy noses.

[SHE SHOOTS TWO SQUIRRELS: "EEP" -THUD, "EEP" -THUD.]

I shall return at midnight to collect the loot, when I'll fill you so full of holes I could market you as a new English cheese!  

[SHADOW VOICE] Ha ha ha ha ha!

> She exits, laughing

**Blackadder**
Oh God! What a way to die! Shot by a transvestite on an unrealistic grassy knoll!

>Blackadder has hours ahead of him— he cannot escape. At which moment of total despair...

 [BALDRICK WANDERS UP.]

**Baldrick**
Morning, Mr B.

**Blackadder**
Bal- Baldrick? Baldrick! Thank you for introducing me to a genuinely new experience.

**Baldrick**
What experience is that?

**Blackadder**
Being pleased to see you! Now what are you doing here, you revolting animal?

**Baldrick**
I've come for the Shadow's autograph. You know I'm a great fan of the Shadow's.

**Blackadder**
Yes, yes, just untie me Baldrick, come on.

**Baldrick**
> Baldrick begins to untie Blackadder.

What, has he gone? Oh what a pity, I wanted him to autograph my new poster. Look, his reward has gone up to ten thousand pound.

**Blackadder**
Good lord! Ten thousand pounds.

**Baldrick**
Yep.

**Blackadder**
That gives me an idea. Baldrick, take this cartload of loot back to the palace and meet me back here at midnight, with ten soldiers, a restless lynch mob and a small portable gallows.

## SCENE 15: PRINCE'S CHAMBERS

> Blackadder enters with a breakfast tray. Prince George is reading a paper.

**Prince**
Aha, brekkers! I could eat fourteen trays of it this morning and still have room for a dolphin on toast!

**Blackadder**
Any particular reason for this gluttonous levity, Sir?

**Prince**
Well, what do you think Blackadder, I'm in love! I'm in love, I'm in love, I'm in love. Oh Amy, bless all ten of your tiny little pinkies. Oh, let's see what's in the paper?

[READS]

Oh my God!! She's been arrested and hanged!

**Blackadder**
[CASUALLY]
Oh really?

**Prince**
It turns out she was a highwayman!

**Blackadder**
Tsk. These modern girls.

**Prince**
Apparently someone tipped off the authorities and collected the ten thousand pound reward. What a greasy sneak. Oh, if only I could get my hands on him.

**Blackadder**
Tsk. You can't trust anyone these days, Sir.

**Prince**
It says here that she had an accomplice.

[ALARMED, BLACKADDER DROPS THE BREAKFAST TRAY.]

**Prince**
But they don't know who it was.

[THE TRAY FLIES BACK UP UNTO BLACKADDER'S HANDS.]

**Prince**
Amy, Amy, Amy, I shall never forget you, never ever, ever ever!

[SOBS INTO HIS PILLOW]

Right, what's for breakfast?

**Blackadder**
Kedgeree, Sir.

**Prince**
Great. Actually, come to think of it Blackadder, I didn't need to get married anyway. I've got pots of money.

**Blackadder**
Really?

**Prince**
Mmm. The most extraordinary thing happened. I was a bit peckish during the night, so I nipped downstairs to the biscuit barrel.

**Blackadder**
[WORRIED]
The biscuit barrel?

**Prince**
And do you know what I found inside?

[BLACKADDER NODS DESPAIRINGLY]

Ten thousand pounds that I never knew I had! I've got so much money now I don't know what to do with it!

**Blackadder**
How about a game of cards, Sir?

**Prince**
Excellent idea!

[END CREDITS BEGIN]

-------------------------------------------------------------------------------

                                 For the
                        BENEFIT of SEVERAL VIEWERS
                         MR. CURTIS & MR. ELTON'S
                            Much admir'd Comedy
                           B L A C K   A D D E R
                             T h e   T H I R D
                                    OR
                             AMY and AMIABILITY
             was performed with appropriate Scenery Dresses etc.
                                    by
                             EDMUND BLACKADDER,
                           butler to the Prince,
                            Mr. ROWAN ATKINSON
                   Baldrick, a dogsbody, Mr. TONY ROBINSON
              The Prince Regent, their master, Mr. HUGH LAURIE
                     Mrs. Miggins, a coffee shoppekeeper,
                         Miss. HELEN ATKINSON-WOOD
                      Amy Hardwood, the elusive Shadow,
                          Miss. MIRANDA RICHARDSON
                 Mr. Hardwood, her father, Mr. WARREN CLARKE
               Sally Cheapside, a young lady of dubious virtue,
                             Miss BARBARA HORNE
                      The Duke of Cheapside, her father,
                               Mr. ROGER AVON

             MUSIC (never perform'd before), Mr. HOWARD GOODALL

                 designer of graphics, Mr. GRAHAM McCALLUM
                    buyer of properties, Miss. JUDY FARR
            supervisor of production operatives, Mr. ALLAN FLOOD
               designer of visual effects, Mr. STUART MURDOCH
                 designer of costumes, Miss. ANNIE HARDINGE
                  designer of make-up, Miss. VICKY POCOCK
                     mixer of vision, Miss. SUE COLLINS
                    supervisor of cameras, Mr. RON GREEN
                  editor of videotape, Mr. CHRIS WADSWORTH
                   director of lighting, Mr. RON BRISTOW
               co-ordinator of technicalities, Mr. JOHN LATUS
                  supervisor of sound, Mr. PETER BARVILLE
               assistant to production, Miss. NIKKI COCKCROFT
               assistant manager of floors, Mr. DUNCAN COOPER
                  manager of production, Miss. OLIVIA HILL
                      the designer, Mr. ANTONY THORPE
                    the director, Miss. MANDIE FLETCHER

                          the producer, Mr. LLOYD

                To conclude with Rule Britannia in full chorus
                             NO MONEY RETURN'D
                            (C) BBC  MCMLXXXVII
