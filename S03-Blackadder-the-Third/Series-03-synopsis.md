# Blackadder the Third: Mr Curtis and Mr Elton's much admir'd Comedy

## Dish and Dishonesty

The rotten borough of Dunny-on-the-Wold (three cows, a daschund named Colin and a small hen in its late forties) elects a whelk-brained MP with a distinctly turnippy aroma.

## Ink and Incapability

After Baldrick incinerates Dr Johnson's new dictionary, Edmund's plan to rewrite it over the weekend proves to be like fitting wheels to a tomato-- time-consuming and completely unnecessary.

## Nob and Nobility

At Mrs Miggin's Coffee Shoppe, huge suspicious sausages are all the rage and French aristocrats are all the rouge in this nail-painting tale of revolutionary derring-do and lace hankies.

## Sensse and Senility

The prince's palace crawls with actors and anarchists and onlu one man can eliminate them-- that renowned vermin-expert Mr Hopelessly-Drivelo-Can't-Write-For-Toffee-Crappy-Buttler-Weed.

## Amy and Amiability

In a last-ditch attempt to secire a wealthy (if insane) bride for the Prince Regent, Blackadder accidentally plumps for popular highwayman and squirrel-murderer 'The Shadow'.

## Duel and Duality

To save his master's life and thus acquire his huge collection of French pornography, Blackadder enlists the prince's jacket, the Duke of Wellington's cigarillo case and a crazed bloater salesman.
