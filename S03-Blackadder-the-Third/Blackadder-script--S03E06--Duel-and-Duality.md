Title:      Blackadder III: Episode 6: Duel and Duality
Credit:     written by
Author:     Richard Curtis AND Ben Elton
Credit:     directed by
Director:   Mandie Fletcher
Source:     NULL
Notes:      UNOFFICIAL TRANSCRIPTION FROM AIRED SHOW
Original air date: 22 october 1987
Copyright:   (c) 1987, British Broadcasting Corporation
Summary:   Blackadder's militant cousin McAdder arrives in London as the Prince Regent seduces some relatives of the fiery Duke of Wellington.

# Blackadder III: Episode 6: Duel and Duality

## DRAMATIS PERSONAE

B:  Baldrick
E:  Edmund Blackadder
PR: Prince Regent George
**Miggins**
Mrs. Miggins
W:  The Duke of Wellington
**MacAdder**
McAdder
S:  King's Servant
**King**
 King George III

## SCENE 1: THE KITCHEN

> Blackadder walks in and goes straight to hang his coat, ignoring Baldrick.

**Baldrick**
Ooh! Mr. Blackadder.

**Blackadder**
Leave me alone, Baldrick. If I'd wanted to talk to a vegetable I'd have bought one at the market.

**Baldrick**
Well, don't you want this message?

> He holds up a piece of paper.

**Blackadder**
No, thank you. God, I'm wasted here.

> He flops into his chair. Life is just not worth the living.

It's no life for a man of noble blood being servant to a master with the intellect of a jugged walrus and all the social graces of a potty.

**Baldrick**
I'm wasted too. I've been thinking of bettering myself.

**Blackadder**
Oh, really? How?

**Baldrick**
I applied for the job of village idiot of Kensington.

**Blackadder**
Oh. Get anywhere?

**Baldrick**
I got down to the last two, but I failed the final interview.

**Blackadder**
Oh, what went wrong?

**Baldrick**
I turned up. The other bloke was such an idiot he forgot to.

**Blackadder**
Yes, well I'm afraid my ambitions stretch slightly further than professional idiocy in West London. I want to be remembered when I'm dead. I want books written about me. I want songs sung about me. And then hundreds of years from now I want episodes from my life to be played out weekly at half past nine by some great heroic actor of the age.

**Baldrick**
Yeah, and I could be played by some tiny tit in a beard.

**Blackadder**
Quite. Now, what's this message?

**Baldrick**
I thought you didn't want it?

**Blackadder**
Well I may do. It depends what it is.

**Baldrick**
So you do want it?

**Blackadder**
Well I don't know, do I? It depends on what it is.

**Baldrick**
[GETTING PANNICKY]
Well, I can't tell you unless you want to know, and you said you didn't want to know, and now I'm so confused I don't know where I live or what my name is.

**Blackadder**
Your name is of no importance and you live in the pipe in the upstairs water-closet.

> He gets up and snatches the note from the kitchen table.

 Oh God! Was the man who gave you this, by any chance, a red-headed lunatic with a kilt and a claymore?

**Baldrick**
Yeah, and the funny thing is, he looked exactly like you.

**Blackadder**
My mad cousin, McAdder. The most dangerous man ever to wear a skirt in Europe.

**Baldrick**
Yeah, he come in here playing the bag-pipes, then he made a haggis, sang "Auld Lang Syne" and punched me in the face.

**Blackadder**
Why?

**Baldrick**
Because I called him a knock-kneed Scottish pillock.

**Blackadder**
An unwise action, Baldrick, since Mad McAdder is a homicidal maniac.

**Baldrick**
My mother told me to stand up to homicidal maniacs.

**Blackadder**
Yes. If this is the same mother who confidently claimed that you were a tall handsome stallion of a man, I should treat her opinions with extreme caution.

**Baldrick**
I love my mum.

**Blackadder**
And I love chops and sauce but I don't seek their advice. I hate it when McAdder turns up. He's such a frog-eyed, beetle-browed basket-case.

**Baldrick**

[IN BLACKADDER'S EAR]
 He's the spitting image of you.

**Blackadder**
[RATTLED]
No he's not. We're about as similar as two completely... dissimilar things in a pod. What's the old tartan throw-back banging on about this time?

[READS, SKIMMING THE LETTER]

 "Have come South for the rebellion." Oh God! Surprise, surprise... "Staying with Miggins. The time has come. Best sword and Scotland. Insurrection... Blood... Large bowl of porridge... Rightful claim to throne..." He's mad! He's mad. He's madder than Mad Jack McMad the winner of last year's Mr Madman competition.

[THE PRINCE'S BELL RINGS.]

**Blackadder**
Ah! The walrus awakes.

## SCENE 2: PRINCE GEORGE'S CHAMBERS

> Blackadder enters carrying the breakfast tray, which he sets down on the table in the living room.

**Prince**
Ah Blackadder. Notice anything unusual?

**Blackadder**
Yes, Sir, it's 11:30 in the morning and you're moving about. Is the bed on fire?

**Prince**
Well, I wouldn't know, I've been out *all night*. Guess what I've been doing? Wraaarrhhhh...

**Blackadder**
Beagling, Sir?

**Prince**
Better even than that. Sink me Blackadder if I, if I haven't just had the most wonderful evening of my life.

> Blackadder could not be less enthusiastic.

**Blackadder**
Tell me all, Sir.

>He takes off the prince's cloak.

**Prince**
Well as you know when I set out I looked divine. At the party as I passed all eyes turned.

**Blackadder**
[Flatteringly]
And I daresay quite a few stomachs.

**Prince**
Well that's right. And then these two ravishing beauties came up to me and whispered in my ear... that they loved me.

[LICKS HIS LIPS]
> He leans provocatively against the door, and then wanders into the bedroom.

**Blackadder**
And what happened after you woke up?

> The prince throws himself luxuriously onto the bed.

**Prince**
Oh, this was no dream Blackadder. Five minutes later I was in a coach flying through the London night bound for the ladies' home.

**Blackadder**
And which home is this? A home for the elderly or a home for the mentally disadvantaged?

**Prince**
Oh no no no no no. This was Apsley House. Do you know it?

**Blackadder**
Yes, Sir. It is the seat of the Duke of Wellington. Those ladies, I fancy, would be his nieces.

**Prince**
Ooh, so you fancy them too? Well, I don't blame you. Bravo! I spent a night of ecstasy with a pair of Wellingtons and I loved it.

**Blackadder**
Sir, it may interest you to know that the Iron Duke has always let it be known that he will kill in cold blood anyone who takes sexual advantage of any of his relatives.

**Prince**
Well, yes, but Big-nose Wellington is in Spain fighting the French, he'll never know.

**Blackadder**
On the contrary, Sir. Wellington triumphed six months ago.

**Prince**
I'm dead.

**Blackadder**
It would seem so, Sir.

**Prince**
I haven't got a prayer, have I Blackadder?

**Blackadder**
Against throat-slasher Wellington? The finest blade His Majesty commands? Not really no.

> He gets up— his life now in danger.

**Prince**
Then I shall flee. How's your French Blackadder?

**Blackadder**
*Parfait, monsieur*. But I fear France would not be far enough.

**Prince**
Well, how's your Mongolian?

**Blackadder**
Mmm, *chang hatang motzo moto*. But I fear Wellington is a close personal friend of the chief Mongol. They were at Eton together.

**Prince**
I'm doomed. Doomed as a dodo.

> There is a knock on the door

**Prince**
Oh my God, he's here, Wellington's here already!

> Baldrick enters with a note. Prince George throws himself at Baldrick's feet.

**Prince**
Oh, your Grace, your Grace, forgive me. Forgive me. I didn't know what I was doing. I was a mad, mad, sexually over-active fool.

**Blackadder**
Sir, it's Baldrick. You're perfectly safe.

> Blackadder has taken Baldrick's message

**Prince**
Well, hurrah!

**Blackadder**
 Ah, until 6 o'clock tonight.

**Prince**
[SUBDUED]
Hurrooh.

**Blackadder**
[READS LETTER]
"From the Supreme Commander, Allied Forces Europe. Sir, Prince or pauper, when a man soils a Wellington he puts his foot in it. Open paranthesis This is not a joke. I do not find my name remotely funny, and people who do end up dead. Close bracket. I challenge you to a duel tonight at eighteen hundred hours in which you will die. Yours with sincere apologies for your impending violent slaughter, Arthur Wellesley, Duke of Wellington."

**Baldrick**
Sounds a nice, polite sort of bloke.

> Prince George starts to cry.

**Prince**
[CRIES]
 Ahh ah ahhh haaa haaawww.

**Blackadder**
Oh, don't worry, Sir, please. Just consider that life is a valley of woe filled with pain, misery, hunger and despair.

**Prince**
Well not for me it bloody isn't! As far as I'm concerned life is a big palace full of food, drink, and comfy sofas.

> Baldrick has been lurking by the door. He has an idea.

**Baldrick**
May I speak, Sir?

**Blackadder**
Certainly not, Baldrick! The Prince is about to die. The last thing he wants to do in his final moments is exchange pleasantries with a certified plum-duff.

**Prince**
Easy, Blackadder, let's hear him out.

**Blackadder**
Very well, Baldrick. We shall hear you out, then throw you out.

**Baldrick**
Well, your Majesty. I have a cunning plan which could get you out of this problem.

**Blackadder**
Don't listen to him, Sir. It's a cruel proletarian trick to raise your hopes. I shall have him shot the moment he's finished clearing away your breakfast.

**Prince**
No wait, Blackadder. Perhaps this disgusting degraded creature is some sort of blessing in disguise.

**Blackadder**
Well if he is, it's a very good disguise.

**Prince**
After all, did not our Lord send a lowly earthworm to comfort Moses in his torment?

**Blackadder**
[FIRMLY]
 No.

**Prince**
Well, it's the sort of thing he might have done. Well, come on Mr. Spotty, speak.

**Baldrick**
Well, your Majesty, I just thought— this Welliton bloke's been in Europe for years. You don't know what he looks like. He don't know what you looks like. So why don't you get someone else to fight the duel instead of you?

**Prince**
But I'm the Prince Regent! My portrait hangs on every wall!

**Blackadder**
Answer that, Baldrick.

**Baldrick**
Well, my cousin Bert Baldrick, Mr Gainsborough's butler's dogsbody, says that he's heard that all portraits look the same these days, 'cause they're painted to a romantic ideal rather than as a true depiction of the idiosycratic facial qualities of the person in question.

**Blackadder**
[IMPRESSED]
Your cousin, Bert, obviously has a larger vocabulary than you do, Baldrick.

**Prince**
No, now, he's right, damn him. Anybody could fight the duel and Wellers would never know.

**Blackadder**
All the same, Sir, Baldrick's plan does seem to hinge on finding someone willing to commit suicide on your behalf.

**Prince**
Oh, yes, yes, yes, but he would be fabulously rewarded. Money, titles, castles...

**Blackadder**
A coffin, erm...

**Baldrick**
That's right. I thought maybe Mr. Blackadder himself would fancy the job.

**Prince**
What a splendid idea!

**Blackadder**
Excuse me, your Highness. Trouble with the staff.

[BALDRICK AND BLACKADDER LEAVE THE ROOM. BLACKADDER GRABS BALDRICK BY THE
LAPELS.]

## SCENE 3: THE VESTIBULE

> Blackadder enters with Baldrick held firmly by the scruff of the neck.

**Blackadder**
Baldrick, does it have to be this way? Our valued friendship ending with me cutting you into long strips and telling the Prince that you walked over a very sharp cattlegrid in an extremely heavy hat?

**Baldrick**
Mr. Blackadder, you was only just saying in the kitchen how you wanted to rise again— now here the Prince is offering you the lot.

**Blackadder**
But, tiny, tiny brain, the Iron Duke will kill me. To even think about taking him on you'd have to be some kind of homicidal maniac who was fantastically good at fighting, like McAdder, like McAdder...

> And into his head pops a cunning plan.

[EXCITED]
Like McAdder could fight the duel for me!

[BLACKADDER RE-ENTERS THE PRINCE'S BEDROOM.]

## SCENE 4: PRINCE GEORGE'S CHAMBERS

**Blackadder**
[CALMLY]
My apologies, Sir. I was just having a word with my insurance people. And obviously I would be delighted to die on your behalf.

**Prince**
God's toenails, Blackadder, I'm most damnably grateful. You won't regret this you know.

**Blackadder**
Well that's excellent. There's just one point though, Sir, re: the suicide policy. There's an unusual clause which states that the policy holder must wear a big red wig and affect a Scottish accent in the combat zone.

**Prince**
Small print eh? Huh.

## SCENE 5: MRS MIGGINS'S COFFEE SHOPPE

> It is in chaos— full or tartans and swords, and Mrs Miggins is lying disheveled against the bar as Blackadder enters.

**Blackadder**
Ah, Mrs. Miggins. Am I to gather from your look of pie-eyed exhaustion and the globules of porridge hanging off the walls that my cousin McAdder has presented his credentials?

**Miggins**
Oh yes indeed, Sir. You've just missed him.

**Blackadder**
I trust he has been practising with his claymore.

**Miggins**
Oooooh, I should say so! I'm as weary as a dog with no legs that's just climbed Ben Nevis.

**Blackadder**
A claymore is a sword, Mrs. Miggins.

> Mrs Miggins points to a wood carving which is just behind her up on the bar.

**Miggins**
See this intricate wood carving of the infant Samuel at prayer? He whittled that with the tip of his mighty weapon with his eyes closed.

**Blackadder**
[UNIMPRESSED]
Yes, exquisite.

**Miggins**
He bid me bite on a plank, there was a whirlwind of steel, and within a minute three men lay dead and I had a lovely new set of gnashers.

[GRINS WOODENLY]

**Blackadder**
Really. Just tell him to meet me here at 5 o'clock, will you? To discuss an extremely cunning plan. If all goes well by tomorrow the clan of McAdder will be marching the high road back to glory.

**Miggins**
Ooh, lovely. I'll do you a nice packed lunch.

## SCENE 6: PRINCE GEORGE'S CHAMBERS

> Blackadder enters, looks for the Prince in the sitting room and walks through
into the bedroom.

**Blackadder**
Good news, your Majesty. This evening I will carve the Duke into an attractive piece of furniture with some excellent dental work.

> He then realizes Prince George isn't about.

Your Highness?

> He suddenly notices Prince George behind the bedroom door with his fingers in his ears.

Your Highness!

**Prince**
Ooohh! Oh, thank God it's you, Blackadder. I've just had word from Wellington. He's on his way here now.

**Blackadder**
Ah, that's awkward. The Duke must believe from the very start that I am you.

**Prince**
Hmm, well, hmm, any ideas?

**Blackadder**
There's no alternative, Sir— we must swap clothes.
[STARTS TO TAKE OFF HIS JACKET]

**Prince**
Oh, fantastic, yes— dressing up. I love it. It's just like that story, ah, "The Prince and the Porpoise".

**Blackadder**
"...and the Pauper", Sir.

**Prince**
Oh yes! Yes yes yes, "The Prince and the Porpoise and the Pauper". Jolly good stuff.

[THEY EXCHANGE CLOTHES AND WIGS.]

## SCENE 7: PRINCE GEORGE'S CHAMBERS

> Blackadder is now wearing the prince's coat.

**Prince**
Excellent, excellent. Why, my own father wouldn't recognize me.

**Blackadder**
Your own father never can. He's mad.

**Prince**
Oh yes, yes.

[THEY WALK THROUGH INTO THE LOUNGE.]

**Blackadder**
Unfortunately, Sir, you do realize that I shall have to treat you like a servant?

**Prince**
Oh, I think I can cope with that, thank you, Blackadder.

**Blackadder**
And you will have to get used to calling me "your Highness", your Highness.

**Prince**
"Your Highness, your Highness."

**Blackadder**
No, just "your Highness", your Highness.

**Prince**
That's what I said, "your Highness, your Highness", your Highness, your Highness.

**Blackadder**
Yes, let's just leave that for now, shall we? Complicated stuf,f obviously.

> Baldrick rushes in.

**Baldrick**
Big Nose is here...

> He is totally flummoxed by the cunning switch of identities.

But what?.. Who?.. Where?.. How?

**Blackadder**
Don't even try to work it out, Baldrick. Two people you know well have exchanged coats and now you don't know which is which.

**Prince**
I must say I'm pretty confused myself! Which one of us is Wellington?

**Blackadder**
[EXASPERATED]
 Wellington is the man at the door.

**Prince**
Oh. And the porpoise?

**Blackadder**
Hasn't arrived yet, Sir. We'll just have to fill in as best we can without it. Sir, if you would let the Duke in.

**Prince**
Certainly, your Highness, your Highness.
[LEAVES]

**Blackadder**
And you'd better get out too, Baldrick.

**Baldrick**
Yes, Your Highness, Your Highness.
[LEAVES]

**Blackadder**
Oh God! If only they had a brain cell between them.

> Enter the Duke of Wellington, a tall, red-faced, arrogant-looking soldier. Prince George announces him.

**Prince**
The Duke of Wellington!

**Wellington**
[BOWING TO BLACKADDER]
Have I the honour of addressing the Prince Regent, Sir?

**Blackadder**
You do.

**Wellington**
Hmm. Congratulations, Highness, your bearing is far nobler than I'd been informed...

[TO THE PRINCE]

Take my hat at once, Sir, unless you want to feel my boot in your throat! And be quicker about it than you were with the door.

**Prince**
Yes, my ord.

**Wellington**
 I'm a Duke, not a Lord!

[CLOUTS THE PRINCE]

 Where were you trained? A Dago dancing class? Shall I have my people thrash him for you, Highness?

> Blackadder pauses. Prince George is getting very worried.

[THE PRINCE SIGNALS "NO" FROM BEHIND WELLINGTON.]

**Blackadder**
Errm.. No, he's very new. At the moment I'm sparing the rod.

**Wellington**
Ah! Fatal error. Give them an inch and before you know it they've got a foot, much more than that and you don't have a leg to stand on.

> He whacks Prince George again.

Get out!

Now, Sir, to business. I am informed that your royal father grows ever more eccentric and at present believes himself to be...

> Reads from a paper he is holding.

"a small village in Lincolnshire, commanding spectacular views of the Nene valley." I therefore pass on my full account of the war on to you, the Prince of Wales.

> He hands over a heavy saddle bag. Blackadder takes out a very small piece of paper and reads...

**Blackadder**
Ah that's excellent. Thank you.
[FEELS IN BAG, TAKES OUT A NOTE]
"We won, signed Wellington." Well, that seems to sum it up very well. Was there anything else?

**Wellington**
Two other trifling things, Highness. The men had a whip-round and got you this. Well, what I mean is I had the men roundly whipped until they got you this. It's a cigarillo case engraved with the regimental crest of two crossed dead Frenchmen, emblazoned on a mound of dead Frenchmen motif.

**Blackadder**
Thank you very much. And the other trifling thing?

**Wellington**
 Your impending death, Highness.

**Blackadder**
Oh, yes, of course. Mind like a sieve.

**Wellington**
Mmm, I can not deny I'm looking forward to it. Britain has the finest trade, the finest armies, the finest navies in the world. And what do we have for royalty? A mad Kraut sausage-sucker and a son who can't keep his own sausage to himself. The sooner you're dead the better.

**Blackadder**
You're very kind.

**Wellington**
 Now, you're no doubt anxious to catch up with the news of the war. I have here the most recent briefs from my general in the field.

**Blackadder**
Yes, well if you would just like to pop them in the laundry basket on the way out.

> He gets up, rings the bell, and they both sit down at the table where Wellington is spreading out a map.

Tea?

**Wellington**
 Yes, immediately.

[BLACKADDER RINGS THE BELL.]

**Wellington**
 Now, let's turn to the second front, my lord.

[UNFOLDS A MAP ON THE TABLE]

**Blackadder**
Ah yes.

[INSPECTS MAP]
Now, as I understand it Napoleon is in North Africa. And Nelson is stationed in...

**Wellington**
Alaska, your Highness. In case Boney should try to trick us by coming via the North Pole.

**Blackadder**
Yes... Perhaps a preferable stratagem, your Grace, might be to harry him amid-ships as he leaves the Mediterranean. Uhm, *Trafalgar* might be quite a good spot...

**Wellington**
[RATHER TAKEN ABACK]
Trafalgar? Well, I'll mention it to Nelson. I must say I'm beginning to regret the necessity of killing you, your Highness. I'd been told by everybody that thepPrince was a confounded moron.

**Blackadder**
Oh, no no no no no.

[PRINCE ENTERS, WHISTLING. HE PUTS THE TEA-TRAY HE IS CARRYING ON THE MAP.]

**Wellington**
Oh, Hell and buckshot! It's that tiresome servant of yours again!

**Prince**
Ooh, budge up, budge up.

[SITS DOWN NEXT TO BLACKADDER]

**Wellington**
 How dare you, Sir, sit in the presence of your betters! Get up!

**Prince**
Oh yes, cripes. I forgot.

> Wellington really smacks him.

**Wellington**
You speak when you're spoken to. Unless you want to be flayed across a gun carriage. Well?

> He hits him again. Prince George can scarcely believe it. He comes and stands next to Blackadder, hoping for protection

**Blackadder**
Sir, Sir, I fear you have been too long a soldier. We no longer treat servants that way in London society.

**Wellington**
 Why, I hardly touched the man!

**Blackadder**
Aah, I think you hit him very hard.

**Wellington**
Nonsense! *That* would have been a hard hit!
[HITS THE PRINCE, HARD]

I just hit him like that.
[ONCE MORE HITS HIM]

**Blackadder**
No, Sir, a soft hit would be like this.
[HITS THE PRINCE]

 Whereas you hit him like this.
[AND AGAIN, HARD]

**Prince**
[GETS BACK TO HIS FEET]
Please, um, I wonder if I might be excused,your Highness, your Highness.

**Blackadder**
Certainly.

[ASIDE]
I'm sorry about that, Sir, but one has to keep up the pretence.

**Prince**
No, no. I quite understand. You carry on the good work.

**Blackadder**
Very well, Sir.
[ONCE MORE HITS HIM]

**Wellington**
Hang on, this is bloody coffee! I ordered tea!

[HE GRABS THE PRINCE BY THE EAR AND HAULS HIM BACK TO THE TABLE.]

You really are a confounded fool. Aren't you? I'd heard everywhere that the Prince was an imbecile whereas his servant Blackadder was respected about town. Now that I discover the truth I'm inclined to beat you to death. TEA!!

> Prince George picks up the tray and scuttles out, almost crying. Wellington kicks his retreating behind.

**Blackadder**
Tell me, do you ever stop bullying and shouting at the lower orders?

**Wellington**
[BELLOWING]
NEVER! There's only one way to win a campaign: shout, shout and shout again.

**Blackadder**
You don't think, then, that inspired leadership and tactical ability have anything to do with it?

**Wellington**
NO! It's all down to shouting. BAAGGHH!

> A might roar!

**Blackadder**
I hear that conditions in your army are appalling.

**Wellington**
Well, I'm sorry, but those are my conditions and you'll just have to accept them. That is until this evening when I shall kill you.

**Blackadder**
Hmm, who knows, maybe I shall kill you.

**Wellington**
Dyaa. Nonsense. I've never been so much as scratched, my skin is as smooth as a baby's bottom. Which is more than you can say for my bottom.

**Blackadder**
Yes. One point, Sir. I should, perhaps, warn you that while duelling I tend to put on my lucky wig and regimental accent.

**Wellington**
That won't help you. It would take a homicidal maniac in a claymore and a kilt to get the better of me!

**Blackadder**
Well, that's handy.

## SCENE 8: THE KITCHEN

> Prince George is behaving like a tempermental diva.

**Prince**
I tell you, Baldrick, I'm not leaving the kitchen until that man is out of the house.

[THERE IS A KNOCK ON THE DOOR AND THE BELL RINGS.]

**Baldrick**
It's all right, your Majesty, don't worry, I'll deal with this.

[THE PRINCE HIDES BEHIND THE SCULLERY DOOR.]

> Enter Mrs Miggins.

**Miggins**
Ah hello, Baldrick. I've brought your buns. Where's Mr. Blackadder? Oh, not upstairs still, running around after that port-swilling, tadpole-brained smelly-boots?

**Baldrick**
[CAREFULLY]
 I don't know who you mean.

**Miggins**
Prince George, Baldrick. His boots smell so bad a man would need to have his nose amputated before taking them off. Well, that's what Mr. Blackadder says.

**Baldrick**
As a joke.

**Miggins**
Didn't you write a little poem about him last week?

**Baldrick**
No, I didn't.

**Miggins**
Ooh, you did:
In the Winter it's cool,
In the Summer it's hot,
But all the year round,
Prince George is a clot.
[LAUGHS]

**Baldrick**
A lovely. I said Prince George is a lovely.

**Miggins**
Oh, well. I'd better be off anyway. Tell Mr. Blackadder to expect Mr. McAdder at five o'clock.

> She gets ready to leave.

Just as soon as that fat Prussian truffle pig has got his snout wedged into a bucket of tea-cakes.

> She makes porcine snorting noises and leaves.

**Baldrick**
[CALLS AFTER HER]
I think it must be next door you're wanting, Strange-Woman-Who-I've-Never-Seen-Before, Mrs. Miggins.

> The prince reappears.

**Prince**
[SHARPLY]
Baldrick!

**Baldrick**
Yes, your Highness?

**Prince**
[VERY MOVED]
Is it true? Did you really write a poem about how lovely I am?

**Baldrick**
[SMILES SWEETLY]
Yes, and Mr. Blackadder loves you too.

**Prince**
Well I must say. I find that very touching. I do.

[THE BELL RINGS AGAIN.]

**Prince**
I wish they wouldn't keep on doing that.

## SCENE 9: PRINCE GEORGE'S CHAMBERS

> Wellington and Blackadder are standing by the door. Wellington shakes Blackadder's hand.

**Wellington**
Well, goodbye, Sir. And may the best man win. I.e. me.

> Enter Prince George with tea tray.

**Prince**
Your tea, Sir.

**Wellington**
You're late! Where the Hell have you been for it, India?

> He slaps Prince George

**Blackadder**
Or Ceylon?
> He also slaps Prince George

**Wellington**
Or China?

>He kicks Prince George who crashes into the table with his tray.

And don't bother to show me the way out. I don't want to die of old age before I get to the front door.

> And he exits. It's not been a good day for the Prince.

## SCENE 10: MRS MIGGINS'S COFFEE SHOPPE

> Blackadder enters dressed in his usual clothes.

**Blackadder**
Ah! Miggins. So where's McAdder? I thought he was going to be here at five o'clock.

**Miggins**
Yes, I'm sorry. He's just popped out. You look ever so similar to each other you know, it's quite eerie.

**Blackadder**
[ANNOYED]
Look, did you tell him to be here or not?

**Miggins**
I did, Idid. You just keep missing each other. I can't imagine why.

> MacAdder appears at the door— red-haired, wild and kilted. They really are astonishingly similar. If it wasn't physically impossible, a person would swear they were the same person in slightly different wigs— one of whom is putting on a rather unconvincing Scottish accent.

**MacAdder**
I'll tell you why. It's because there's no coffee shop in England big enough for two Blackadders.

**Blackadder**
Ah! Good day, cousin MacAdder. I trust you are well.

**MacAdder**
Aye, well enough.

**Blackadder**
And Morag?

**MacAdder**
She bides fine.

**Blackadder**
And how stands that mighty army, the clan MacAdder?

**MacAdder**
They're both well.

**Blackadder**
I always thought that Jamie and Angus were such fine boys.

**MacAdder**
Angus is a girl.

**Blackadder**
Of course.

**MacAdder**
So tell me cousin, I hear you have a cunning plan.

**Blackadder**
I do, I do. I want you to take the place of the Prince Regent and kill the Duke of Wellington in a duel.

**MacAdder**
Aye, and what's in it for me?

**Blackadder**
Enough cash to buy the Outer Hebrides. What do you think?

**MacAdder**
Fourteen shillings and six-pence? Well, it's tempting. But I've got an even better plan. Why don't I pretend to be the Duke of Wellington and kill the Prince of Wales in a duel? Then I could kill the King and be crowned with the ancient stone bonnet of MacAdder.

**Miggins**
[CLASPING MACADDER'S ARM]
And I shall wear the granite gown and limestone bodice of MacMiggins, Queen of all the Herds.

**Blackadder**
Look, for God's sake, MacAdder, you're not Rob Roy. You're a top kipper salesman with a reputable firm of Aberdeen fishmongers. Don't throw it all away. If you kill the Prince they'll just send the bailiffs round and arrest you.

**MacAdder**
Oh blast, I forgot the bailiffs.

**Blackadder**
So we can return to the original plan then?

**MacAdder**
No, I'm not interested. I'd rather go to bed with the Loch Lomond monster. And besides I have to be back in the office on Friday. I promised Mr. McNulty I'd shift a particularly difficult bloater for him. Forget the whole thing. I'm off home with Miggsie.

**Miggins**
Yes, yes. Show me the glen where the kipper roams free. And forget Morag forever.

**MacAdder**
No, never. Oh, I must do right by Morag. We must return to Scotland and you must fight in the old Highland way — bare breasted and each carrying an eight pound baby.

**Miggins**
Oh, yes, yes. I love babies.
[KISSES MACADDER]

**MacAdder**
You're a woman of spirit! I look forward to burying you in the old Highland manner. Farewell Blackadder, you spineless goon!
[THEY LEAVE]

**Blackadder**
Oh God! Fortune vomits on my eiderdown once more.

## SCENE 11: PRINCE GEORGE'S CHAMBERS

> Enter Blackadder wearing trhe Prince's coat. The prince is deep in thought.

**Prince**
Ah, Blackadder. It has been a wild afternoon full of strange omens. I dreamt that a large eagle circled the room three times and then got into bed with me and took all the blankets. And then I saw that it wasn't an eagle at all but a large black snake. Also, Duncan's horses did turn and eat each other. As usual. Good portents for your duel, do you think?

**Blackadder**
Not very good, Sir. I'm afraid the duel is off.

**Prince**
OFF?

**Blackadder**
As in "sod". I'm not doing it.

**Prince**
By thunder, here's a pretty game. You will stay, Sir, and do duty by your Prince. Or I shall...

**Blackadder**
Or what? You port-brained twerp. I've looked after all my life. Even when we were babies I had to show which bit of your mother was serving the drinks.

**Prince**
[KNEELS]
Ooh! Please, please. You've got to help me. I don't want to die. I've got so much to give. I want more time.

**Blackadder**
A poignant plea, Sir. Enough to melt the stoniest of hearts. But the answer, I'm afraid, must remain: "You're going to die, fat pig."

**Prince**
Oh, wait, wait, wait. I'll give you everything.

**Blackadder**
Everything?

**Prince**
Everything!

**Blackadder**
The money, the castles,the jewellery?

**Prince**
Yes.

**Blackadder**
The highly artistic but also highly illegal set of French lithographs?

**Prince**
Everything.

**Blackadder**
The amusing clock where the little man comes out and drops his trousers every half hour?

**Prince**
Yes, yes, alright.

**Blackadder**
Very well, I accept. A man may fight for many things: his country, his principles, his friends, the glistening tear on the cheek of a golden child. But personally, I'd mud wrestle my own mother for a ton of cash, an amusing clock, and a sack of French porn. You're on.

**Prince**
Hoorah!

## SCENE 12: A COLONNADE

> Blackadder, still wearing the prince's wig and coat, and Baldrick are first to arrive for the duel. It is a misty morning, the kind of weather in which momentous deeds are done.

**Blackadder**
Right, Baldrick, now here's the plan. When he offers me the swords, I kick him in the nuts and you set fire to the building. In the confusion we claim a draw.

**Baldrick**
Yes.

> At which moment the Duke of Wellington enters.

**Wellington**
Ah, Your Highness.

[HE BOWS TO BLACKADDER]

Let's be about our business.

**Blackadder**
Now don't forget Baldrick. You...
[MIMES THE LIGHTING OF A MATCH]

when I...
[MIMES KNEE JERK]

**Wellington**
 Come, Sir. Choose your stoker.

> He holds out a box. Blackadder takes out one of the furry plungers.

**Blackadder**
What! Are we going to tickle each other to death?

**Wellington**
No, Sir. We fight with cannon!

> Blackadder notices two small cannon at each end of the half-assembled colonnade.

**Blackadder**
But I thought we were fighting with swords.

**Wellington**
 Swords! What do you think this is, the Middle Ages? Only girls fight with swords these days. Stand by your gun, Sir. Hup two three, hup two three.

**Blackadder**
Wait a minute, what the...

**Wellington**
Stand by cannon for loading procedure!

> He shouts out bewildering technicallingo with movements like the Royal tournament and withbits of metal snapping into place

Stoke! Muzzle! Wrench!
Crank the staunch pedal! Pull tee bar!

> Blackadder is not keeping up at all. He reads from the cannon's manual.

**Blackadder**
"Congratulations on choosing the Armstrong Whitworth four-pounder cannonette. Please read the instructions carefully and it should give you years of trouble free maiming."

**Wellington**
Check elevation! Chart trajectory!

> Blackadder doesn't know where to start. Meantime, Wellington is kneeling by a perfect little cannon, pointed at Blackadder, three feet away. He lights it with a taper at arm's length.

Prime fuse! Aiiim...!

**Blackadder**
Look, wait a minute.

**Wellington**
F-I-R-E!

> Huge cannon shot. Blackadder is blown over. When the smoke clears, he lies with his head in Baldrick's lap, distinctly dying.
> Sad, mournful music wafts through the still air.

**Baldrick**
Mr B. Oh, Mr B! Sir, please help me get his coat off.

**Blackadder**
Leave it Baldrick. It doesn't matter.

**Baldrick**
Yes it does. Blood's Hell to shift. I want to get it in to soak.

> Wellington kneels on one knee beside the heroic and doomed figure.

**Wellington**
 You die like a man, Sir. In combat.

**Blackadder**
You think so? Dammit, we must build a better world. When will the killing end?

**Wellington**
 You don't think I too dream of peace? You don't think that I too yearn to end this damn dirty job we call soldiering?

**Blackadder**
Frankly, no. My final wish on this Earth is that Baldrick be sold, to provide funds for a Blackadder foundation to promote peace, and to do research into the possibility of an automatic machine for cleaning shoes. Alsoo I charge...

> He dies. Woe upon woe!

**Wellington**
His Highness is dead.

> The music jerks to a sudden stop.

**Blackadder**
Actually, I'm not sure I am.

> Miracle upon miracle, he springs to his feet.

Fortunately that cigarillo box you gave me was placed exactly at the point where the cannon-ball struck.

[PRODUCES A VERY DENTED CASE]

I always said smoking was good for you.

> Wellington rises.

**Wellington**
Ah ha ha. Honour is satisfied. God clearly preserves you for greatness. His Highness is saved. Hurrah!

> And he drops to one knee aagin. At which point, Prince George enters,in Blackadder's coat.

**Prince**
Umm, no actually. It's me. I'm his Highness. Well done, Bladders, glad you made it.

> Wellington is red with fury.

**Wellington**
What in the name of Bonaparte's balls is this fellow doing now?

**Prince**
Ahh, no no, I really am the prince. It was all just larks, and darn fine larks at that I thought.

**Wellington**
I have never, in all my campaigns, encountered such insolence! Your master survives an honourable duel and you cheek him like a French whoopsy! I can contain myself no longer!

> He shoots Prince George cursorily. Sad music starts up again. Prince George lies in the laps of Blackadder and Baldrick.

**Prince**
I die. I hope men will say of me that I did duty by my country.

**Blackadder**
I think that's pretty unlikely, Sir. If I was you I'd try for something a bit more realistic.

**Prince**
Like what?

**Blackadder**
That you hope men will think of you... as a bit of a thicky.

**Prince**
All right, I'll hope that then. Toodle-oo everyone. Let you know, and all that.

> Prince George dies.

> There is a fanfare of trumpets, and a shout, "Kneel for his Majesty, the King of England". Enter the king, a big, mad German carrying a plant. They all kneel.

**King**
Someone told me my son was here. I wish him to marry this rose bush. I want to make the wedding arrangements.

> A shortmeaningful pause

**Blackadder**
[THINKING QUICKLY]
Here I am, Daddy. This is the Iron Duke, Wellington, commander of all your armed forces.

**King**
Yes I recognized the enormous conk. Ha ha ha.

> Wellington puts his arm around Blackadder as they face the king. He is very complimentary of Blackadder, who laps it up.

**Wellington**
He's a hero. A man of wit and discretion.

**King**
Bravo. You know, my son, for the first time in my life I have a real fatherly feeling about you. People may say I'm stark raving mad and say the word "Penguin" after each sentence, but I believe that we two can make Britain Great— you as the Prince Regent and I as King Penguin.

**Blackadder**
Well, let's hope eh?

Wellington, will you come and dine with us at the palace? My family have a lot to thank you for.

**Wellington**
Dyahh! With great pleasure. Your father may be as mad as a balloon, but I think you have the makings of a great king.

**King**
*Und eine wunderbare Hochzeit! Ja!*

**Blackadder**
Oh, and, Baldrick? Clear away that dead butler will you.

> They leave. Baldrick is left alone with Prince George. He kneels beside him, full of sorrow.

**Baldrick**
[LOOKS UP]
There's a new star in heaven tonight. A new freckle on the nose of the giant pixie...

> Prince George suddenly opens his eyes and sits up.

**Prince**
Erm! No, actually Baldrick, I'm not dead. You see I had a cigarillo box too, look.

> He searches for it in his pocket, but to no avail.

Oh damn, I must have left it on the dresser...

> And he falls back down dead again. It is very much the end.

[END CREDITS BEGIN]

-------------------------------------------------------------------------------

                                 For the
                        BENEFIT of SEVERAL VIEWERS
                         MR. CURTIS & MR. ELTON'S
                            Much admir'd Comedy
                           B L A C K   A D D E R
                             T h e   T H I R D
                                    OR
                              DUEL and DUALITY
             was performed with appropriate Scenery Dresses etc.
                                    by
                             EDMUND BLACKADDER,
                           butler to the Prince,
                            Mr. ROWAN ATKINSON
                   Baldrick, a dogsbody, Mr. TONY ROBINSON
              The Prince Regent, their master, Mr. HUGH LAURIE
                     Mrs. Miggins, a coffee shoppekeeper,
                          Miss. HELEN ATKINSON-WOOD
                  The Duke of Wellington, a famous soldier,
                               Mr. STEPHEN FRY
                       King George III, a Mad Monarch,
                             Mr. GERTAN KLAUBER

             MUSIC (never perform'd before), Mr. HOWARD GOODALL

                 designer of graphics, Mr. GRAHAM McCALLUM
                    buyer of properties, Miss. JUDY FARR
            supervisor of production operatives, Mr. ALLAN FLOOD
               designer of visual effects, Mr. STUART MURDOCH
                 designer of costumes, Miss. ANNIE HARDINGE
                  designer of make-up, Miss. VICKY POCOCK
                     mixer of vision, Miss. SUE COLLINS
                    supervisor of cameras, Mr. RON GREEN
                  editor of videotape, Mr. CHRIS WADSWORTH
                   director of lighting, Mr. RON BRISTOW
             co-ordinator of technicalities, Mr. RICHARD WILSON
                  supervisor of sound, Mr. PETER BARVILLE
               assistant to production, Miss. NIKKI COCKCROFT
               assistant manager of floors, Mr. DUNCAN COOPER
                  manager of production, Miss. OLIVIA HILL
                      the designer, Mr. ANTONY THORPE
                    the director, Miss. MANDIE FLETCHER

                          the producer, Mr. LLOYD

                To conclude with Rule Britannia in full chorus
                             NO MONEY RETURN'D
                            (C) BBC  MCMLXXXVII
