Title:      Blackadder III: Episode 2:Ink and Incapability
Credit:     written by
Author:     Richard Curtis AND Ben Elton
Credit:     directed by
Director:   Mandie Fletcher
Source:     
Notes:      
            UNOFFICIAL TRANSCRIPTION FROM AIRED SHOW
            Original air date:   24 september 1987
Copyright:  [C] 1987, British Broadcasting Corporation

Baldrick burns the only copy of Samuel Johnson's dictionary, and Blackadder has only one weekend to rewrite it.

===========

DRAMATIS PERSONAE
-----------------

E: Edmund Blackadder
B: Baldrick
G: Prince George
M: Mrs. Miggins
J: Dr. Samuel Johnson
By: Lord George Gordon Byron
C: Samuel Taylor Coleridge
S: Percy Bysshe Shelley

NOTES:
In reality, only one of those last three was alive at the same time as
Johnson: Coleridge was about 12 years old when Johnson died. The others
hadn't been born yet. I'm not sure of the exact years of the Prince Regent.

The words in the gobbledygook scene here are best guesses. Nearly all of
them have been looked up in dictionaries -- or at least the parts of them
have. Anyone with a closed-caption decoder who can help with the words,
though, please do.

SCENE 1: [PRINCE GEORGE'S CHAMBERS]
-------
> Prince George is in bed asleep. Blackadder attends. Prince George wakes violently.

G: [WAKES] Oh, oh, oh, Blackadder!  BLACKADDER!

E: [ENTERS] Your Highness.

G: Wha--wha--what time is it?

E: Three o'clock in the afternoon, Your Highness.

G: Oh, thank God for that. I thought I'd overslept.

E: I trust you had a pleasant evening, Sir...?

G: Well, no, actually. The most extraordinary thing happened. Last night,
   I was having a bit of a snack at the Naughty Hellfire Club, and some
   fellow said that I had the wit and sophistication of a donkey.

E: Oh, an absurd suggestion, Sir.

G: You're right, it is absurd.

E: Unless, of course, it was a particularly *stupid* donkey.

G: You see? If only *I'd* thought of saying that!

E: Well, it is so often the way, Sir, too late one thinks of what
   one *should* have said. Sir Thomas More, for instance -- burned alive
   for refusing to recant his Catholicism -- must have been kicking him-
   self, as the flames licked higher, that it never occurred to him
   to say, "I recant my Catholicism."

G: Well, yes, you see, only the other day, Prime Minister Pitt called
   me an "idle scrounger," and it wasn't until ages later that I thought
   how clever it would've been to have said, "Oh, bugger off, you old fart!"
   I need to improve my mind, Blackadder. I want people to say, "That George,
   why, he's as clever as a stick in a bucket of pig swill."

E: And how do you suggest this miracle is to be achieved, Your Highness?

G: Easy! I shall become best friends with the cleverest man in England.
   That renowned brainbox, Dr. Samuel Johnson, has asked me to be patron
   of his new book, and I intend to accept.

E: Would this be the long-awaited "Dictionary", Sir?

G: Oh, who cares about the title as long as there's plenty of juicy murders
   in it. I hear it's a masterpiece.

E: No, Sir, it is not. It's the most pointless book since "How To Learn
   French" was translated into French.  [MOVES INTO LIVING AREA]

G: [FOLLOWS] You haven't got anything personal against Johnson, have you
   Blackadder?

E: Good Lord, Sir, not at all. In fact, I had never heard of him until
   you mentioned him just now.

G: But you do think he's a genius?

E: No, Sir, I do not. Unless, of course, the definition of 'genius' in his
   ridiculous Dictionary is "a fat dullard or wobblebottom; a pompous ass with
   sweatly dewflaps."

G: Oh! Close shave there, then! Lucky you warned me. I was about to embrace
   this unholy arse to the royal bosom.

E: I'm delighted to have been instrumental of keeping your bosom free of
   arses, Sir.

G: Bravo -- don't want to waste my valuable time with wobblebottoms.
   Er, fetch some tea, will you, Blackadder?

E: Certainly, Sir.

G: Oh, and make it two cups, will you? That splendid brainbox, Dr. Johnson, is
   coming round.

> Blackadder cannot believe the prince's stupidity.

SCENE 2: [THE KITCHEN]
-------
> Blackadder enters, slamming the door behind him. He walks down the stairs.

E: [IN DISGUST] Ugh!

B: Something wrong, Mr. B?

E: Oh, something's always wrong, Balders.

> Blackadder reaches the bottom of the stairs and casually throws the prince's tea-tray and its expensive ceramic contents into an upturned barrel.

E: The fact that I'm not a millionaire aristocrat with the sexual capacity
   of a rutting rhino is a constant niggle. But, today, something's even
   wronger. That globulous fraud, Dr. Johnson, is coming to tea.

B: I thought he was the cleverest man in England.

E: Baldrick, I'd bump into cleverer people at a lodge meeting of the Guild of
   Village Idiots.

B: That's not what you said when you sent him your navel.

E: *Novel*, Baldrick -- not navel. I sent him my novel.

B: Well, novel or navel, it sounds a bit like a bag of grapefruits to me.

E: The phrase, Baldrick, is "a case of sour grapes," and yes it bloody well
   is. I mean, he might at least have written back, but no, nothing, not even
   a "Dear Gertrude Perkins: Thank you for your book. Get stuffed.
   --Samuel Johnson."

B: Gertrude Perkins?

E: Yes, I gave myself a female pseudonym. Everybody's doing it these days:
   Mrs. Ratcliffe, Jane Austen--

B: What, Jane Austen's a man?

E: Of course -- a huge Yorkshireman with a beard like a rhododendron bush.

B: Oh, quite a small one, then?

E: Well, compared to Dorothy Wordsworth's, certainly. James Boswell is the
   only real woman writing at the moment, and that's just because she
   wants to get inside Johnson's britches.

B: Perhaps your book really isn't any good.

E: Oh, codswallop! It's taken me seven years, and it's perfect. "Edmund:
   A Butler's Tale" -- a giant rollercoaster of a novel in four hundred
   sizzling chapters. A searing indictment of domestic servitude in the
   eighteenth century, with some hot gypsies thrown in. My magnum opus,
   Baldrick. Everybody has one novel in them, and this is mine.

B: And *this* is mine. [TAKES A SMALL PIECE OF PAPER FROM THE FRONT OF HIS
   TROUSERS] My magnificent octopus.

E: [TAKES AND UNFOLDS IT] This is your novel, Baldrick? 

B: Yeah -- I can't stand long books.

E: [READS] "Once upon a time, there was a lovely little sausage
   called 'Baldrick', and it lived happily ever after."

B: It's semi-autobiographical.

E: And it's completely utterly awful. Dr. Johnson will probably love it.

[A BELL RINGS]

E: Oh, speak of the devil. Well, I'd better go and make the great Doctor
   comfortable. Let's just see how damned smart Dr. Fatty-Know-It-All
   really is. [GOES UP STAIRWAY] Oh, and prepare a fire for the Prince,
   will you, Baldrick?

B: What shall I use?

E: Oh, any old rubbish will do. Paper's quite good. Here, [CRUMPLES UP
   BALDRICK'S 'NOVEL'] try this for starters. [THROWS PAPER AT BALDRICK]


SCENE 3: [THE PRINCE'S CHAMBERS]

[KNOCK AT DOOR]

G: Enter!

> Blackadder opens the door for Dr. Johnson. He is sixty, fat and pompous.

E: Dr. Johnson, Your Highness.

G: Ah, Dr. Johnson! Damn cold day!

J: Indeed it is, Sir -- but a very fine one, for I celebrated last night the
   encyclopaedic implementation of my pre-meditated orchestration of demotic
   Anglo-Saxon.

G: [NODS, GRINNING, THEN SPEAKS] Nope -- didn't catch any of that.

J:  Well, I simply observed, Sir, that I'm felicitous, since, during the
    course of the penultimate solar sojourn, I terminated my uninterrupted
    categorisation of the vocabulary of our post-Norman tongue.

G: Well, I don't know what you're talking about, but it sounds damn saucy,
   you lucky thing! I know some fairly liberal-minded girls, but I've
   never penultimated any of them in a solar sojourn, or, for that matter,
   been given any Norman tongue!

E: I believe, Sir, that the Doctor is trying to tell you that he is happy
   because he has finished his book. It has, apparently, taken him ten years.

G: [SYMPATHETICALLY] Yes, well, I'm a slow reader myself.

J: Here it is, Sir.

[HE PRODUCES A SHEAF OF MANUSCRIPT; PLACING IT ON THE TABLE AND TAKING UP THE TOP PART]   

J: The very cornerstone of English scholarship. This book, Sir, contains every
   word in our beloved language.

G: Hmm.

E: Every single one, Sir?

J: [CONFIDENTLY] Every single word, Sir!

E: [TO PRINCE] Oh, well, in that case, Sir, I hope you will not object if
   I also offer the Doctor my most enthusiastic contrafibularities.

J: What?

> Johnson takes a pencil from behind his ear. He is furious.

E: 'contrafibularities', Sir? It is a common word down our way.

J: Damn!

> He starts writing in the dictionary

E: Oh, I'm sorry, Sir. I'm anaspeptic, phrasmotic, even compunctuous to have
   caused you such pericombobulations.

J: What? What? WHAT?

> He's now frantic, scribbling down all these new words.

G: What are you on about, Blackadder? This is all beginning to sound a bit
   like dago talk to me.

E: I'm sorry, Sir. I merely wished to congratulate the Doctor on not having
   left out a single word. [JOHNSON SNEERS] Shall I fetch the tea, Your Highness?

G: Yes, yes -- and get that damned fire up here, will you?

E: Certainly, Sir. I shall return interphrastically. [EXITS][JOHNSON WRITES SOME MORE]

G: So, Dr. Johnson. Sit ye down. Now, this book of yoursl Tell me, what's
   it all about?

J: It is a book about the English language, Sir.

G: I see! And the hero's name is what?

J: There is no hero, Sir.

G: No hero? Well, lucky I reminded you. Better put one in pronto! Ermm...
   call him 'George'. 'George' is a good name for a hero. Er, now, what about
   heroines?

J: There is no heroine, Sir... unless it is our Mother Tongue.

G: Ah, the *mother's* the heroine. Nice twist. So, how far have we got, then?
   Old Mother Tongue is in love with George the Hero. Now what about murders?
   Mother Tongue doesn't get murdered, does she?

J: No she doesn't. No-one gets murdered, or married, or in a tricky situation
   over a pound note!

G: Well, now, look, Dr. Johnson, I may be as thick as a whale omelette, but
   even I know a book's got to have a plot.

J: Not this one, Sir. It is a book that tells you what English words mean.

G: I *know* what English words mean; I *speak* English! You must be a bit
   of a thicko.

J: [STANDS UP] Perhaps you would rather not be patron of my book if you can see
   no value in it whatsoever, Sir!

G: [STANDS UP] Well, perhaps so, Sir! As it sounds to me as if my being patron
   of this complete cowpat of a book would set the seal once and for all on
   my reputation as an utter turnip-head!

J: Well! It is a reputation well deserved, Sir! [SARCASTICALLY] Farewell!
   [OPENS DOOR TO FIND EDMUND WITH TEA TRAY]

E: Leaving already, Doctor? Not staying for your pendigustatery
   interludicules?

J: No, Sir! Show me out!

E: Certainly, Sir -- anything I can do to facilitate your velocitous
   extramuralisation.

J: [TO PRINCE GEORGE] You will regret this doubly, Sir. Not only have you
   impecuniated [TURNS TO EDMUND AND MAKES] A BOASTING NOISE, THEN CONTINUES]
   my Dictionary, but you've also lost the chance to act as patron to the only
   book in the world that is even better.

E: Oh, and what is that, Sir? "Dictionary II: The Return of the Killer
   Dictionary"?

J: No, Sir! It is "Edmund: A Butler's Tale" [EDMUND KNOCKS OVER SOME OF THE
   TEACUPS] by Gertrude Perkins -- a huge rollercoaster of a novel crammed
   with sizzling gypsies. [TO PRINCE GEORGE] Had you supported it, Sir, it would
   have made you and me and Gertrude millionaires.

E: [SHOCKED] MILLIONAIRES!!

J: But it was not to be, Sir. I fare you well. I shall not return.

> He exits in a fury.

E: Excuse me, Sir.

> He shoots out.


SCENE 4: [THE VESTIBULE]
-------
> Outside the door

E: Er, Dr. Johnson... A word, I beg you.

J: A word with you, Sir, can mean seven million syllables. You might start
   now and not be finished by bedtime! [PAUSES, REALISED HE'S FORGOTTEN
   SOMETHING] Oh, blast my eyes! In my fury, I have left my Dictionary
   with your foolish master! Go fetch it, will you?

E: Sir, the Prince is young and foolish, and has a peanut for a brain. Give
   me just a few minutes and I will deliver both the book and his patronage.

J: Oh, will you, Sir... I very much doubt it. A servant who is an influence
   for the good is like a dog who speaks: very rare.

E: I think I can change his mind.

J: Hmpf! Well, I doubt it, Sir. A man who can change a prince's mind is
   like a dog who speaks *Norwegian*: even rarer! I shall be at Mrs. Miggins'
   Literary Salon in twenty minutes. Bring the book there. [EXITS]

SCENE 5: [PRINCE GEORGE'S CHAMBERS]
> Prince George is standing in front of a raging fire of Baldrick's making.

> Blackadder enters.

E: Your Highness, may I offer my congratulations?

G: Well, thanks, Blackadder! That pompous babboon won't be back in a hurry!

E: Oh, on the contrary, Sir. Dr. Johnson left in the highest of spirits.

G: What?

E: He is utterly thrilled at your promise to patronise his Dictionary.

G: I told him to sod off, didn't I?

E: Yes, Sir, but that was a joke... surely.

G: Was it?

E: Certainly! and a brilliant one, what's more.

G: [HAPPY AT THE IDEA HE MANAGED TO PULL OFF A JOKE, PRETENDS THAT IT WAS
   HIS INTENTION ALL ALONG] Yes, yes! I...er...suppose it was, rather,
   wasn't it?

E: So may I deliver your note of patronage to Dr. Johnson as promised?

G: Well, of course. If that's what I promised, then that's what I must do.
   ...and I remember promising it distinctly.

E: Excellent. [TO BALDRICK] Nice fire, Baldrick.

B: Thank you, Mr. B.

E: Right, let's get the book. Now, Baldrick, where's the manuscript?

B: You mean the big papery thing tied up with string?

E: Yes, Baldrick -- the manuscript belonging to Dr. Johnson.

B: You mean the baity fellow in the black coat who just left?

E: Yes, Baldrick -- Dr. Johnson.

B: So you're asking where the big papery thing tied up with string belonging
   to the baity fellow in the black coat who just left is.

E: Yes, Baldrick, I am, and if you don't answer, then the booted bony thing
   with five toes at the end of my leg will soon connect sharply with the
   soft dangly collection of objects in your trousers. For the last time,
   Baldrick: Where is Dr. Johnson's manuscript?

B: On the fire.

E: On the *what*?

B: The hot orangy thing under the stony mantlepiece.

E: [AGHAST] You *burned* the Dictionary?

B: Yup.

E: You burned the life's work of England's foremost man of letters?

B: Well, you did say "burn any old rubbish."

> Blackadder has nearly turned to stone.

E: Yes, fine.

G: Isn't it, er... Isn't it going to be a bit difficult for me to patronise
   this book if we've burnt it?

E: Yes, it is, Sir. If you would excuse me a moment...

G: Oh, of course, of course. Now that I've got my lovely fire, I'm as happy
   as a Frenchman who's invented a pair of self-removing trousers.

E: Baldrick, will you join me in the vestibule?

SCENE 6: [IN THE VESTIBULE]
> Blackadder grabs Baldrick by the collar

E: *We* are going to go to Mrs. Miggins', we're going to find out where Dr. Johnson keeps a copy of that Dictionary, and then *you* are going to steal it.

B: Me?

E: Yes, you!

B: Why me?

E: Because you burnt it, Baldrick.

B: But then I'll go to Hell forever for stealing.

E: Baldrick, believe me: eternity in the company of Beezlebub and all his
   hellish instruments of death will be a picnic compared to five minutes
   with me -- and this pencil -- if we can't replace this Dictionary.


SCENE 7: [MRS. MIGGINS' COFFEE SHOPPE]
-------

> Mrs. Miggins' Coffee Shoppe has become fashionable with the litterati. Lounging in poses of boredom, sensitive, ill-health are Byron and Shelly. Coleridge has passed out at the table.

[Shelley, Coleridge, and Byron are at a table. Shelley sits up holding a
handkerchief; Byron stands very erect, staring straight ahead at nothing;
Coleridge appears dead. As Shelley begins to speak, the person at the next
table stands and moves to a table as far away as possible.]

S: O, Love-bourne ecstasy that is Mrs. Miggins, wilt thou bring me but one
   cup of the browned juicings of that naughty bean we call 'coffee', ere I
   die?

M: [SWOONS] Ooohhhh, you do have a way of words with you, Mr. Shelley!

BY: To Hell with this fine talking. Coffee, woman! My consumption grows
    evermore acute, and Coleridge's drugs are wearing off.

M: Ohh, Mr. Byron, don't be such a big girl's blouse!

> She hands Shelley a coffee.

SCENE 8: [OUTSIDE MRS MIGGINS' COFFEE SHOPPE]
-------
> Blackadder and Baldrick are standing outside.

E: Don't forget the pencil, Baldrick.

B: Oh, I certainly won't, Sir.


SCENE 7: [MRS. MIGGINS' COFFEE SHOPPE]
-------
> Blackadder enters. Baldrick sits at a table.

E: Ah, good day to you, Mrs. Miggins.

[MRS MIGGINS SWOONS AND GIGGLES]

E: A cup of your best hot water with brown grit in it -- unless, of course,
   by some miracle, your coffee shop has started selling coffee.

> The three poets sit up.

BY: Be quiet, Sir. Can't you see we're dying?

> They slump down again. Blackadder gives them a withering look.

M: Don't you worry about my poets, Mr. Blackadder. They're not dead;
   they're just being intellectual.

E: Mrs. Miggins, there's nothing intellectual about wandering around Italy
   in a big shirt, trying to get laid. Why are they *here* of all places?

By: We are here, Sir, to pay homage to the great Dr. Johnson, as, Sir,
    should you!

E: Oh, well, absolutely! Erm... I intend to. Er, you wouldn't happen to have a
   copy of his Dictionary on you, would you, so I can do some revising before
   he gets here?

[JOHNSON ENTERS]

J: Friends, I have returned.

[POETS WELCOME HIM; EDMUND SAYS 'HURRAY']

By: So, Sir, how was the Prince?

J: [ADJUSTING HIS POWDERED WIG] The Prince was and is an utter fool, and his
   household filled with cretinous servants. [HIS GAZE THEN FALLS UPON EDMUND,
   AND HE DOES A DOUBLE-TAKE WHILE THE POETS LAUGH]

E: [SUAVELY] Good afternoon, Sir.

J: And you are the worst of them, Sir. After all your boasting, have you
   my Dictionary and my patronage?

E: Not quite. The Prince begs just a few more hours to really get to
   grips with it.

J: Bah!!

Poets: Bah!!

E: However, I was wondering if a lowly servant such as I might be permitted
   to glance at a copy.

J: COPY?!

Poets and Johnson: COPY?!

J: There is no copy, Sir.

E: No copy?

> Blackadder does turn to stone.

J: No, Sir. Making a copy is like fitting wheels to a tomato, time consuming
   and completely unnecessary.

[POETS LAUGH]

E: But what if the book got lost?

J: I should not lose the book, Sir, [STANDS, COFFEE CUP IN HAND, APPROACHING
   EDMUND MENACINGLY] and if any other man should, I would tear off
   his head with my bare hands and feed it to the cat! [BREAKS COFFEE CUP
   BY SQUEEZING]

E: Well, that's nice and clear.

By: And I, Lord Byron, [UNSHEATHING A SWORD] would summon up fifty of my men,
    lay siege to the fellow's house and do bloody murder on him. [RESTS
    SWORD ON BALDRICK'S SHOULDER]

C: [POINTING A BLADE AT EDMUND] And I would not rest until the criminal was
   hanging by his hair, with an Oriental disembowelling cutlass thrust up his
   ignoble behind.

E: I hope you're listening to all this, Baldrick.


SCENE 10: [PRINCE GEORGE'S CHAMBERS]

[PRINCE GEORGE IS PEELING AN APPLE]

E: Sir, I have been unable to replace the Dictionary. I am therefore leaving
   immediately for Nepal, where I intend to live as a goat.

G: Why?

E: Because if I stay here, Dr. Johnson's companions will have me brutally
   murdered, Sir.

G: Good God, Blackadder, that's terrible! [ASIDE] Do you know any other
   butlers?

E: And, of course, when the people discover you have burnt Dr. Johnson's
   Dictionary, they may go round saying, "Look! There's thick George. He's
   got a brain the size of a weasel's wedding tackle."

G: In that case, something *must* be done!

B: I have a cunning plan, Sir.

G: Hurrah! Well, that's that, then.

E: I wouldn't get overexcited, Sir. I have a horrid suspicion that Baldrick's
   plan will be the stupidest thing we've heard since Lord Nelson's famous
   signal at the Battle of the Nile: "England knows Lady Hamilton's a virgin.
   Poke my eye out and cut off my arm if I'm wrong."

G: Great! Let's hear it, then.

> Blackadder resigns himself to the worst and looks at the ceiling.

B: It's brilliant. You take the string -- that's still not completely
   burnt -- you scrape off the soot, and you shove the pages in again.

E: Which pages?

B: Well, not the same ones, of course.

E: Yes, I think I'm on the point of spotting the flaw in this plan, but do
   go on. Which pages are they?

B: Well, this is the brilliant bit: You write some new ones.

E: Some new ones. You mean rewrite the Dictionary. I sit down tonight and
   rewrite the Dictionary that took Dr. Johnson ten years.

B: Yup.

E: Baldrick, that is by far and away, and without a shadow of doubt, the
   worst and most comtemptible plan in the history of the universe. On the
   other hand, I hear the sound of disembowelling cutlasses being sharpened,
   and it's the only plan we've got, so if you will excuse me, gentlemen...

G: Perhaps you'd like me to lend a hand, Blackadder. I'm not as stupid as I
   look.

B: I *am* as stupid as I look, Sir, but if I can help, I will.

E: Well, it's very kind of you both, but I fear your services might be as
   useful as a barber shop on the steps of the guillotine.

G: Oh, come on, Blackadder, give us a try!

E: Very well, Sir, as you wish. Let's start at the beginning, shall we?
   First: 'A'. How would you define 'a'?

B: Ohh...'a' [CONTINUES THIS IN BACKGROUND]

G: Oh, I love this! I love this: quizzies...Errmmm, hang on, it's coming... ooohh, crikey, errmm, oh yes, I've got it!

E: What?

G: [PROUDLY] Well, it doesn't really mean anything, does it?

E: Good. So we're well on the way, then. " 'a'; impersonal pronoun;
   doesn't really mean anything." Right! Next: 'A'... 'A-B'.

[BALDRICK AND PRINCE PONDER OVER THIS]

B: Well, it's a buzzing thing, isn't it. "A buzzing thing."

E: Baldrick, I mean something that starts with 'A-B'.

B: Honey? Honey starts with a bee.

G: He's right, you know, Blackadder. Honey does start a bee... and a flower,
   too.

E: Yes, look, this really isn't getting anywhere. And besides, I've left out
   'aardvark'.

G: Oh well, don't say we didn't give it a try.

E: No, Your Highness, it was a brave start, but I fear I must proceed on my
   own. Now, Baldrick, go to the kitchen and make me something quick and sim-
   ple to eat, would you? Two slices of bread with something in between.

B: What, like Gerald, Lord Sandwich, had the other day?

E: Yes -- a few rounds of Geralds.

SCENE 11: [PRINCE GEORGE'S CHAMBERS]
-------
> It is very late at night. Blackadder is surrounded by scrunched paper in Prince George's study. Prince George wanders out in his dressing gown.

G: How goes it, Blackadder?

E: Not all that well, Sir.

G: Well, let's have a look...[READS] "Medium-sized insectivore with
   protruding nasal implement." [PAUSES] Doesn't sound much like a bee to me.

E: [SHOUTS] It's an aardvark! Can't you see that, Your Highness? It's a
   bloody aardvark!!

G: Oh dear -- still on 'aardvark', are we?

E: Yes, I'm afraid we are. And if I ever meet an aardvark, I'm going to
   step on its damn protruding nasal implement until it couldn't suck
   up an insect if its life depended on it.

G: Got a bit stuck, have you?

E: I'm sorry, Sir. It's five hours later, and I've got every word in the
   English language except 'a' and 'aardvark' still to do. And I'm not
   very happy with my definition of either of them.

G: Well, don't panic, Blackadder, because I have some rather good news.

E: Oh? What?

G: Well, we didn't take 'no' for an answer, and have, in fact, been working
   all night. I've done 'B'.

E: Really? And how have you got on?

G: Well, I had a bit of trouble with 'belching', but I think I got it
   sorted out in the end. [BURPS] Oh no! There I go again! [LAUGHS]

E: You've been working on that joke for some time, haven't you, Sir?

G: Well, yes, I have, as a matter of fact, yes.

E: Since you started...

G: Basically.

E: So, in fact, you haven't done any work at all.

G: Not as such, no.

E: Great. Baldrick, what have you done?

B: I've done 'C' and 'D'.

E: Right, let's have it, then.

B: Right. "Big blue wobbly thing that mermaids live in."

E: What's that?

B: 'Sea'.

E: Yes -- tiny misunderstanding. Still, my hopes weren't high. Now;
   what about 'D'?

B: I'm quite pleased with 'dog'.

E: Yes, and your definition of 'dog' is...?

B: "Not a cat."

E: Excellent. Excellent! Your Highness, may I have a word?

G: Certainly.

E: As you know, Sir, it has always been my intention to stay with you until
   you had a strapping son and I one likewise to take over the burdens
   of my duties.

G: That's right, Blackadder, and I thank you for it.

E: But I'm afraid, Sir, that there has been a change of plan. I am off to
   the kitchen to hack my head off with a big knife.

G: Oh, come on, Blackadder, it's only a book. Let's just damn the fellow's
   eyes, strip the britches from his backside and warm his heels to Putney
   Bridge! HURRAH!

E: Sir, these are not the days of Alfred the Great. You can't just lop
   someone's head off and blame it on the Vikings.

G: Can't I, by God!

E: No.

G: Oh, well, all right, then let's just get on with it! I mean, boil my
   brains, it's only a dictionary. No-one's asked us to eat ten raw pigs
   for breakfast. Good Lord, I mean, we're *British*, aren't we? [EXITS]

E: [MUTTERS] You're not, you're German. [TO BALDRICK] Get me some coffee,
   Baldrick. If I fall asleep before Monday, we're doomed!


SCENE 12: [PRINCE GEORGE'S CHAMBERS]
-------
> Blackadder is asleep and snoring. Baldrick wakes him.

B: Mr. Blackadder, time to wake up!

E: What time is it?

B: Monday morning.

E: [PANICS] Monday morning?! Oh my God! I've overslept! Where's the quill?
   Where's the parchment?

B: I don't know. Maybe Dr. Johnson's got some with him.

E: WHAT??!

B: He's outside.

E: [SCREAMS] AAAOOOOHHHH!

[JOHNSON ENTERS]

J: Are you ill, Sir?

E: No, you can't have it. I know I said Monday, but I want Baldrick to read
   it, which, unfortunately will mean teaching him to read, which will take
   about ten years -- but time well spent, I think, because it's such a very
   good dictionary.

J: I don't think so.

E: [EXCLAIMS] Oh God!! We've been burgles!! [PAUSES] What?

J: I think it's an awful dicitonary, full of feeble definitions and ridiculous
   verbiage. I've come to ask you to chuck the damn thing in the fire.

E: Are you sure?

J: I've never been so sure of anything in my life, Sir.

E: I love you, Dr. Johnson, and I want to have your babies. [THEY EMBRACE;
   EDMUND NOTICES A WOMAN STANDING BEHIND JOHNSON] Oh, sorry, excuse me, Dr.
   Johnson, but my Auntie Marjorie has just arrived. [LOOKS AT BALDRICK, WHO
   HAS AN DOG'S HEAD] Baldrick, who gave you permission to turn into an
   Alsatian? [BALDRICK WAVES; EDMUND REALISES THE ABSURDITY OF THE SCENE]
   Oh God, it's a dream, isn't it? [JOHNSON, BALDRICK AND AUNTIE TWIRL OUT
   THE DOOR] It's a bloody dream! [SOUND OF HARPS IS HEARD] Dr. Johnson
   doesn't want us to burn his Dictionary at all.

SCENE 13: [PRINCE GEORGE'S CHAMBERS]
-------

> Baldrick is waking up Blackadder. It is exactly the same setup as a few seconds earlier.

B: Mr. Blackadder, time to wake up...

E: What time is it?

B: Monday morning.

E: [PANICS] Monday morning?! Oh my God! I've overslept! Where's the quill?
   Where's the parchment?

B: I don't know. Maybe Dr. Johnson's got some with him.

E: [TERRIFIED] WHAT??!

B: He's outside.

E: Ah-- Now, hang on. Hang on. If we go on like this, you're going to turn
   into an Alsatian again.

[JOHNSON AND OTHER POETS BANG NOISILY AT THE DOOR]

E: Oh my God! Quick, Baldrick-- we've got to escape!


SCENE 14: [THE VESTIBULE OF PRINCE GEORGE'S CHAMBERS]
-------
> Dr Johnson and the poets are standing outside the prince's rooms.

S: Ho, Sir! Bring out the Dictionary at once.

BY: Bring it out, Sir, or, in my passion, I shall kill everyone by giving
    them syphilis!

C: Bring it out, Sir, and also any opium plants you may have around there.

J: Bring it out, Sir, or we shall break down the door!

E: [OPENS THE DOOR] Ah, good morning. Dr. Johnson, Lord Byron--

J: Where is my Dictionary?

E: And what dictionary would this be?\

> Dr Johnson starts waling forwards, pushing Blackadder backwards with his cane followed by the poets brandishing their swords. They end up at the doors of the prince's bed-chamber.

J: The one that has taken eighteen hours of every day for the last ten years.
   My mother died; I hardly noticed. My father cut off his head and fried it
   in garlic in the hope of attracting my attention; I scarcely looked up from
   my work. My wife brought armies of lovers to the house, who worked in
   droves so that she might bring up a huge family of bastards-- I cared not.

E: Am I to presume that my elaborate bluff has not worked?

J: Dictionary!

E: Right, well, the truth is, Doctor -- now, don't get cross, don't over-
   react -- the truth is... we burnt it.

J: Then you die!

Blackadder is about to die. At which moment, Prince George enters from the bedroom casually carrying a manuscript.

G: 'Morning, everyone. You know, this Dictionary really is a cracking good
   read. It's an absolutely splendid job!

J: My Dictionary! [TO EDMUND] But you said you burned it!

E: Erm...

G: I think it's a splendid book, and I look forward to patronising it
   enormously!

J: Oh, well, thank you, Sir. Well, I think I'm man enough to sacrifice the
   pleasure of killing to maintain the general good humour. [TO POETS]
   There's to be no murder today, gentlemen. 
   
[THE POETS GROAN WITH DISSAPOINTMENT] 
   
   But prepare to Mrs. Miggins' -- I shall join you there later for a roister you will never forget!

[POETS CHEER AND EXIT]

J: [TO GEORGE] So, ahem, tell me, Sir, what words particularly interested
   you?

G: Oh, er, nothing... Anything, really, you know...

J: Ah, I see you've underlined a few [TAKES DICTIONARY AND READS]: 'bloomers';
   'bottom'; 'burp'; [TURNS A PAGE] 'fart'; 'fiddle'; 'fornicate'?

G: Well...

J: Sir! I hope you're not using the first English dictionary to look up
   rude words!

E: I wouldn't be too hopeful-- that's what all the other ones will be
   used for.

B: [TO EDMUND] Sir, can I look up 'turnip'?

E: 'Turnip' isn't a rude word, Baldrick.

B: It is if you sit on one.

J: Really, Sir, we have more important business in hand. I refer, of course,
   to the works of the mysterious Gertrude Perkins.

E: Mysterious no more, Sir. It is time for the truth. I can, at last, reveal
   the identity of the great Gertrude Perkins.

J: Sir, who is she?

E: She, Sir, is me, Sir. I am Gertrude Perkins.

G: Good Lord!!

E: And what's more: I can prove it. Bring out the manuscript, and I will show
   you that my signature corresponds exactly with that on the front.

J: Why, I must have left it here when I left the Dictionary.

G: This is terribly exciting!!!

E: Baldrick, fetch my novel.

B: Novel?

E: Yes -- the big papery thing tied up with string.

B: What, like the thing we burnt?

E: Exactly like the thing we burnt.

B: So you're asking for the big papery thing tied up with string, exactly
   like the thing we burnt.

E: Exactly.

B: We burnt it.

E: So we did. Thank you, Baldrick -- seven years of my life up in smoke.
   Your Highness, would you excuse me a moment?

G: By all means.

[EDMUND EXITS]

E: [FROM OUTSIDE] OH GOD, NO!!!!! [RE-ENTERS] Thank you, Sir.

J: Burned, you say? That's most inconvenient. A burned novel is like a
   burned dog: You--

E: Oh shut up!

B: [TO JOHNSON] Sir, I have a novel. [GIVES JOHNSON THE BIT OF PAPER SEEN
   EARLIER]

J: [READS] "Once upon a time there was a lovely little sausage called 'B--"
   'Sausage'?! 'SAUSAGE'?!!!!! Oh, blast your eyes! [THROWS PAPER DOWN AND
   EXITS ANGRILY]

B: Oh, well, I didn't think it was that bad!

E: [LOOKING INSIDE THE DICTIONARY] I think you'll find he left 'sausage'
   out of his Dictionary, Baldrick. [SHUTS THE DICTIONARY, BUT NOTICES
   SOMETHING ON THE FIRST PAGE] Oh, and 'aardvark'...

G: Oh, come on, Blackadder; it's not all that bad -- nothing a nice roaring
   fire can't solve. Er, Baldrick, do the honours, will you?

B: Certainly, Your Majesty.

[PRINCE AND EDMUND EXIT. BALDRICK PICKS UP EDMUND'S CRUMPLED PAPERS FROM
TRYING TO WRITE THE DICTIONARY, AND THE REAL DICTIONARY. HE THUMBS THROUGH
THE DICTIONARY, THEN TOSSES IT INTO THE FIRE.]

[END CREDITS BEGIN]

-------------------------------------------------------------------------------

                                 For the
                        BENEFIT of SEVERAL VIEWERS
                         MR. CURTIS & MR. ELTON'S
                            Much admir'd Comedy
                           B L A C K   A D D E R
                             T h e   T H I R D
                                    OR
                            INK and INCAPABILITY
             was performed with appropriate Scenery Dresses etc.
                                    by
                             EDMUND BLACKADDER,
                           butler to the Prince,
                            Mr. ROWAN ATKINSON
                   Baldrick, a dogsbody, Mr. TONY ROBINSON
              The Prince Regent, their master, Mr. HUGH LAURIE
              Dr. Samuel Johnson, noted for his fat dictionary,
                            Mr. ROBBIE COLTRANE
                     Mrs. Miggins, a coffee shoppekeeper,
                         Miss. HELEN ATKINSON-WOOD
              Shelley,\                          Mr. LEE CORNES
                Byron, > romantic junkie poets   Mr. STEVE STEEN
            Coleridge,/                          Mr. JIM SWEENEY

             MUSIC [NEVER PERFORM'D BEFORE], Mr. HOWARD GOODALL

                 designer of graphics, Mr. GRAHAM McCALLUM
                    buyer of properties, Miss. JUDY FARR
                 designer of costumes, Miss. ANNIE HARDINGE
                  designer of make-up, Miss. VICKY POCOCK
                    mixer of vision, Miss. ANGELA WILSON
                    supervisor of cameras, Mr. RON GREEN
                  editor of videotape, Mr. CHRIS WADSWORTH
                   director of lighting, Mr. RON BRISTOW
               co-ordinator of technicalities, Mr. JOHN LATUS
                  supervisor of sound, Mr. PETER BARVILLE
               assistant to production, Miss. NIKKI COCKCROFT
               assistant manager of floors, Mr. DUNCAN COOPER
                  manager of production, Miss. OLIVIA HILL
                      the designer, Mr. ANTONY THORPE

                    the director, Miss. MANDIE FLETCHER

                          the producer, Mr. LLOYD

                To conclude with Rule Britannia in full chorus
                             NO MONEY RETURN'D
                            [C] BBC  MCMLXXXVII