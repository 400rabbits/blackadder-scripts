# The Black Adder: A Most Bloody and Most Gripping Historical Tale

## The Foretelling

From out of the swirling mists of the Dark Ages comes a lone horseman cursed from youth by a deformed haircut and sporting a particularly evil pair of tights.

## Born To Be King

Treachery, murder and Morris-dancing break out in all their full horror when an orange-faced stranger arrives at court.

## The Archbishop

The landscape is littered with dead Archbishops of Canterbury. Edmund's cunning plan is to get his deadliest rival appointed to the vacancy. It fails.

## The Queen of Spain's Beard

The king's international treachery gives the hideous Edmund a chance to press his clammy body againsy one of Europe's most eligible princesses.

## Witchsmeller Pursuivant

The king is a bit under the weather with the Black Death. Withcraft diagnosed by the Black Adder and and only man who can smell it out.

## The Black Seal

In a final gesture of defiance, Edmund rides forth to seek out the Seven Most Evil Men in the land and return with them to seize the throne.
