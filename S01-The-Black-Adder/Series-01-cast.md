# Cast (IMDB order)

Rowan Atkinson                         Edmund, Duke of Edinburgh
Elspet Gray                            The Queen
Tim McInnerny                          Percy
Patrick Allen                          Narrator
Brian Blessed                          King Richard IV
Tony Robinson                          Baldrick
Robert East                            Harry, Prince of Wales
Bert Parnaby                           Cain, A Blind Beggar
Roy Evans                              Abel, A Blind Beggar
David Nunn                             Messenger
Perry Benson                           Daft Ned, A Peasant
Alex Norton                            McAngus
Forbes Collins                         Dopey Jack, A Peasant
Barbara Miller                         Jane Firkettle
Howard Lew Lewis Howard                Mr. Applebottom
Natasha King                           Princess Leia of Hungary
Stephen Frost                          Soft, A Guard
Peter Cook                             Richard III
John Carlisle                          Murdered Lord
Paul McDowell                          Herbert, Archbishop of Canterbury
Frank Finlay                           The Witchsmeller Pursuivant
Arthur Hewlett                         Godfrey, Archbishop of Canterbury / ...
John Savident                          The King
Peter Benson                           Henry VII
Joyce Grant                            Mother Superior
Robert Bathurst                        Prince Henry
Carolyn Colquhoun                      Sister Sara
Des Webb                               Person of Unrestricted Growth
John Barrard                           Retired Morris Dancer
Richard Murdoch                        Ross, A Lord
Philip Fox                             Baldrick
Miriam Margolyes                       Infanta Maria Escalosa of Spain
Rik Mayall                             Self - Mad Gerald
Valentine Dyall                        Angus, A Lord
Jim Broadbent                          Don Speekingleesh, An Interpreter
William Russell                        The Duke of Winchester
Angus Deayton                          Jumping Jew of Jerusalem
Peter Schofield                        Fife, A Lord
Philip Kendall                         Painter
Simon Gipps-Kent                       Rudkin
Paul Brooke                            Friar Bellows
Jane Freeman                           Mrs. Applebottom
Joolia Cappleman                       Celia, Countess of Cheltenham
Kathleen St. John                      Goneril
Oengus MacNamara                       Jesuit
Mark Arden                             Anon, A Guard
Mick Walter                            Jack Large
John Rapley                            Rev. Lloyd
Martin Clarke                          Sir Dominick Prique of Stratford
Roger Sloman                           Three Fingered Pete
Gretchen Franklin                      Cordelia
Martin Soan                            2nd Wooferoonie
Patrick Malahide                       Guy of Glastonbury
Bill Wallis                            Sir Justin de Boinod
Stephen Tate                           Lord Chiswick
Malcolm Hardee                         3rd Wooferoonie
John Hallam                            Sir Wilfred Death
David Delve                            Sir George de Boeuf
Kenn Wells                             1st Messenger
Leslie Sands                           Lord Graveney
Richard Mitchley                       2nd Messenger
Ron Cook                               Sean, The Irish Bastard
Patrick Duncan                         Officer, An Officer
Willoughby Goddard                     Archbishop
Harriet Keevil                         Lady on Ramparts
Sarah Thomas                           Mrs. Field, A Goodwife
Louise Gold                            Mrs. Tyler, A Goodwife
Charlie Dean                           Countess Caroline of Luxembourg (uncredited)
Salo Gardner                           Devil (uncredited)
Nicola Critcher                        Princess (uncredited)
Stephen Gressieux                      Lord (uncredited)
Terry Duran                            Guard (uncredited)
Jay Bura                               Prince in the Tower (uncredited)
Joan Harsant                           Nun (uncredited)
David Fieldsend                        King's Knight (uncredited)
Tan Bura                               Prince in the Tower (uncredited)
Gideon Kolb                            Jumping Jew (uncredited)
