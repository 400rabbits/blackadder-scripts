        [begin theme music]

                The sound of hoofbeats cross the glade
                Good folk, lock up your son and daughter
                Beware the deadly flashing blade
                Unless you want to end up shorter

                Black Adder! Black Adder!
                He rides a pitch-black steed
                Black Adder! Black Adder!
                He's very bad indeed

                Black: His gloves of finest mole
                Black: His codpiece made of metal
                His horse is blacker than a vole
                His pot is blacker than his kettle

                Black Adder! Black Adder!
                With many a cunning plan
                Black Adder! Black Adder!
                You horrid little man


        Cast in Order of Precedence

        Richard III  . . . . . . . . . . .  Peter Cook
        Richard IV   . . . . . . . . . . .  Brian Blessed
        Henry VII  . . . . . . . . . . . .  Peter Benson
        Harry, Prince of Wales   . . . . .  Robert East
        Edmund, Duke of Edinburgh  . . . .  Rowan Atkinson
        Percy, Duke of Northumberland  . .  Tim McInnerny
        The Queen  . . . . . . . . . . . .  Elspet Gray
        Painter  . . . . . . . . . . . . .  Philip Kendall
        Goncril  . . . . . . . . . . . . .  Kathleen St. John
        Regan  . . . . . . . . . . . . . .  Barbara Miller
        Cordelia   . . . . . . . . . . . .  Gretchen Franklin
        Baldrick   . . . . . . . . . . . .  Tony Robinson

        Written by Richard Curtis and Rowan Atkinson

        With additional dialogue by William Shakespeare

        A BBC TV Production in association with The Seven Network, Australia

        Music composed by Howard Goodall
        Graphic Designer  Steve Connelly

        Property Buyers           Penny Rollinson   Tricia Ruddell
        Visual Effects Designer   Chris Lawson

        Production Assistant      Jan Hallett
        Assistant Floor Manager   Hilary Bevan-Jones

        Film Camerman    Willian Dudman
        Film Recordist   Clive Derbyshire
        Film Editor      Mike Jackson

        Camera Supervisor   Ron Peverall
        Vision Mixer        Angela Wilson
        VT Editor           Mykola Pawluk

        Costume Designer   Odile Dicks-Mireaux
        Make-Up Designer   Deanne Turner

        Technical Manager   Terry Brett
        Lighting            Brian Clemett
        Sound               Richard Chamberlain

        Production Manager   Marcus Mortimer

        Designers   Nigel Curzon  Chris Hull

        Director   Martin Shardlow

        Producer   John Lloyd

Goncril:  He wasn't as I expected him.

Regan:  I thought he was very rude.

Goncril:  I thought Henry Tudor would be better looking.

Cordelia:  Yes -- not so Jewish.

Regan:  ...more like that man who rode by just before.

Cordelia:  Oops.

Regan:  Oops.

Goncril:  Oops.

Regan:  We've done it again...

Cordelia:  Silly witching...

Goncril:  [??????]

        MADE IN GLORIOUS TELEVISION
        (C) BBC MCMLXXXIII
