Title:      The Blackadder: Episode 1: The Foretelling
Credit:     by
Author:     Richard Curtis
Credit:     by
Author:     Rowan Atkinson
Credit:     additional dialogue
Author:     William Shakespeare
Credit:     directed by
Director:   Martin Shardlow
Source:     NULL
Notes:      UNOFFICIAL TRANSCRIPTION FROM AIRED SHOW
Original air date: 15 jun 1983
Copyright:   (c) 1983, British Broadcasting Corporation
Summary:   Prince Edmund (Black Adder) arrives late to the Battle of Bosworth Field, accidentally killing King Richard III, thinking he was stealing his horse.

## DRAMATIS PERSONAE

Philip Kendall ... Painter of the scene

## INTRODUCTOARY NARRATION

**Painter**  
History has known many great liars. Copernicus, Goebbels, St Ralph the Liar. But there have been none quite so vile as the Tudor king Henry VII.

[HE IS SHOWN HOLDING A SIGN WHICH READS 'ST BENEDICT THE LIAR']

It was he who rewrote history to portray his predecessor Richard III as a deformed maniac who killed his nephews in the Tower.

But the real truth is that Richard was a kind and thoughtful man who cherished his young wards.  In particular: Richard, Duke of York, who grew into a big, strong boy.

Henry also claimed he won the Battle of Bosworth Field and killed Richard III.  

Again, the truth is very different; for it was Richard, Duke of York, who became king after Bosworth Field, and reigned for thirteen glorious years.

As for who really killed Richard III and how the defeated Henry Tudor escaped with his life, all is revealed in this, the first chapter of a history never before told: the history of The Black Adder!

[OPENING THEME]

## SCENE 1: THE GREAT HALL

> The Eve of The Battle of Bosworth Field, 21st August, 1485

[SCENE IS A FEAST]

> A vast room with bare stone walls and a flagstone floor. Light filters down from the windows set high up. A couple of rich tapestries hang. Very sparse on decoration: a few candelabras are lit.

>Various nobles and lords are gathered for a great feast. Richard III is addressing an enthusiastic gathering of lords. On his left sits Richard, Duke of York, a huge man with an enormous beard, who will soon be king, and his son, Prince Harry.

**Richard, Duke of York**
[BANGS HIS GOBLET THRICE ON THE TABLE]
Silence! Silence!  For the king!

**King Richard III**
[STANDS, HUNCHED, SPEAKS AWKWARDLY]

Now is the summer of our sweet content,overcast winter by these Tudor clouds.

And I that am not shaped for black-fac'd war,...

**Lords**
Shame!

**King**
I that am rudely cast and want true majesty...

[MORE NOISES FROM THE PEOPLE; THEN HE FIXES HIS HUNCHED STANDING POSITION BY YANKING ON HIS CLOAK, WHICH HAD BEEN STUCK]

**Lords**
Never!

**King**
Am forced to fight,
To set sweet England free.

I pray to Heaven we fare well,
And all who fight us go to Hell.

**Lords**
Hurray! Hurray! Hurray!

[CHEERS FROM EVERYONE.  EDMUND, DUKE OF EDINBURGH, SITTING AT THE VERY END OF THE TABLE, STANDS UP, RAISING HIS GOBLET]

**Edmund**
Hurray, hurray, absolutely!  Hurray!
[NOTICES THAT HE'S THE ONLY ONE SPEAKING AND STANDING; SITS BACK DOWN, EMBARRASSED]

**King**
[TO RICHARD]
Who is that?

**Richard**
I know not, My Lord.  I'll ask my son.

 [HE CALLS TO HARRY, PRINCE OF WALES, WHO SITS ON THE OTHER SIDE OF THE KING FROM RICHARD]

 Harry, who is that?

**Harry**
It is your other son, My Lord.

**Richard**
[TO KING]
It is my other son, My Lord.

**King**
Fights he with us on the morrow?

**Richard**
[PAUSES, THEN TO HARRY AGAIN]
What's his name?

**Harry**
[WITH MOUTH FULL]
Edmund.

**Richard**
[TURNS AND YELLS ACROSS THE ROOM TO EDMUND]
Edna, fight you with us on the morrow?

**Edmund**
[STANDS AGAIN]

Er, oh goodness, no!  No, I thought I'd fight with the enemy!

[NO ONE LAUGHS; HE SITS DOWN EMBARRASSED]

**King**
[TO RICHARD]
You're, er, not putting him anywhere near me, are you?

**Richard**
No, no [?].  He'll be somewhere amongst the rabble.

**King**
Oh! Arrow fodder!

**Richard**
Precisely.

**King**
Yes... [CHUCKLES, WAVES TO EDMUND, GRINNING; MUTTERS BETWEEN HIS TEETH]
       What a little turd.

        [CUT TO EDMUND'S END OF THE TABLE]

**Edmund**
[to Percy, Duke of Northumberland, after giving a little wave back
         to King]
Ah, Percy, you see how the King picks me out for special
         greeting?

**Percy**
No, My Lord...

        [A SERVANT POKES HIS HEAD IN, REFILLING THEIR GOBLETS, AND SPEAKS]

**Baldrick**
I saw it, My Lord.

**Edmund**
Ah, and what is your name, little fellow?

**Baldrick**
My name is Baldrick, My Lord.

**Edmund**
Ah.  Then I shall call you...'Baldrick'!

**Baldrick**
...and I shall call you 'My Lord', My Lord.

**Edmund**
Mmm.  I like the cut of your jib, young fellow m'lad!
         How would you like to be my squire in the battle to-morrow?

        [BALDRICK KNEELS INSTANTLY]

**Percy**
[TRYING TO SHOW OFF IN FRONT OF BALDRICK, SPEAKS TO EDMUND]
        It will be a great day to-morrow for we nobles.

**Edmund**
Well, not if we lose, Percy.  If we lose, I'll be chopped to pieces.
         My arms will end up at Essex, my torso in Norfolk, and my genitalia
         stuck up a tree somewhere in Rutland.

**Baldrick**
With you at the helm, My Lord, we cannot lose.

**Percy**
[STILL TRYING TO SHOW OFF]
Well, we could if we wanted to!

**Edmund**
Ah, but we won't, Percy, and I shall prove to all that I am a man!

**Percy**
But you *are* a man, My Lord.

**Edmund**
But how shall it be proved, Percy...?

**Percy**
Well, they could look up that tree in Rutland.  [Edmund baps him on
        the forehead] [????], My Lord.

**Edmund**
It shall be proved by mine enemies rushing to the water closet in
         terror!

**Baldrick**
[RESTRAINED, OF COURSE -- THEY'RE IN A CROWDED ROOM]
Hurray!

**Percy**
Hurray!

**Edmund**
Come: a toast.  Let all those who go to don armour to-morrow
         remember to 'go' before they don armour to-morrow!  Hurray!
         [THEY CLINK GOBLETS]
Already I can hear the sound of battle
         ringing in my ears...

        [Cut to just before the battle, outside.  The following
         lines are spoken to the army]

**King**
Once more unto the breach, dear friends, once more!
       Consign their parts most private to a Rutland tree!

**Harry**
Let blood -- Blood -- BLOOD! -- be your motto! Slit their gizzards!

**Harry**
Now, I'm afraid there's going to have to be a certain amount of, well,
        violence.  But at least we know it's all in a good cause, don't we?

**King**
And gentlemen in London still in bed shall think themselves accursed
       they were not here, and hold their manhood cheap while others speak
       of those who fought with us on Ralph the Liar's Day! [he raises his
       sword high in the air.  Our view follows it into the sky.]

        [Our view comes down from the sky, to see the castle.  Inside Edmund's
         room, he sleeps in his bed, snoring, while Baldrick sleeps on the
         floor, using a dead dog as a pillow.  There is a knock on the door.]

**Mother**
Edmund?  [OPENING THE DOOR]
Edmund...

**Edmund**
Hmm?  Oh, Mother, what do you want?

**Mother**
Did you want to go to the battle this morning?

**Edmund**
[SITS UP WITH A START; REMOVES A COVER FROM A SUNDIAL, AND LOOKS AT IT]
Oh my god, it's eleven o'clock!

**Mother**
[SMILING UNCONCERNEDLY, AMUSEDLY SHAKES HER HEAD, CLOSES THE DOOR]

        [cut to long shot of a rise.  On it we see a silhouette of Edmund on
         horseback.  Following him at a distance is Baldrick on muleback.]

**Baldrick**
My Lord...

**Edmund**
What is it?

**Baldrick**
Where is this battle, then?

**Edmund**
Oh, somewhere called Bosworth Field...

        [they have ridden off to the right of the shot.  Suddenly, we see
         Baldrick going the other way, followed by Edmund.]

**Edmund**
Damn, damn, damn!  The first decent battle since I reached puberty...

        [Now we see them close up, riding together, up a rise leading to a
         valley.]

**Baldrick**
Here we are, My Lord...

**Edmund**
Onward, Baldrick!  To glory!

        [Over the top of the rise we now can see banners clashing together.
         Edmund stops his horse at the top.]

**Edmund**
Yes, erm, I'm not so sure we're needed, you know, Baldrick...
         I mean, everything seems to be going very well, doesn't it?
         Everyone's fighting -- clearly having the time of their lives.
         Wait a moment; some of them over there aren't fighting!  They're...
         they're just lying down!

**Baldrick**
They're dead, My Lord.

**Edmund**
Ah.  [HE WRIGGLES IN HIS SEAT]
Damn, I knew I'd forgotten something.
         Would you excuse me a moment, Baldrick?  [HE TURNS HIS HORSE AWAY]

        [JUST AWAY FROM THE BATTLE, KING ON FOOT MEETS RICHARD ON HORSEBACK]

**Harry**
Your Majesty, you've lost your steed!  Take mine!

**King**
No, no, no.  I've won the battle; I've saved the kingdom; I think I
       can find myself a horse!

**Harry**
How true, My Noble Lord.  I'll see you back at the castle!

**King**
So be it!

        [RICHARD RIDES OFF.  KING WALKS ALONG, CALLING...]

**King**
A horse!  [WHISTLES A CALL]
A horse!  My kingdom for a horse!
       [HE STOPS AS HE SEES A HORSE -- EDMUND'S -- TIED TO A TREE.]
       Ah, Horsie!  [He approaches the horse.  Edmund, doing business behind
       a nearby bush, sees.]

**Edmund**
[MUMBLING TO HIMSELF]
Who is this?

        [as King bends over to untie the horse from the tree, Edmund walks up
         behind...]

**Edmund**
[DRAWING HIS SWORD]
Wait! That's my horse!  [swings his sword;
         lops King's head clean off.  He's rather surprised at his strength
         but quickly gets a cocky feeling, and laughs a bit.]
There, that'll
         teach you!  [HE PICKS UP THE HELMETED HEAD]
You won't be doing
         *that* again, now will you?  [He lifts the helmet's face shield,
         then lowers the shield]

         Oh my god.  It's Uncle Richard.

        [Edmund screams.  Baldrick runs up, having just parked his mule by
         the tree.]

**Baldrick**
What's that, My Lord?

**Edmund**
Hmm?  [FRIGHTENEDLY TOSSES THE HEAD TO BALDRICK.]

**Baldrick**
[CATCHES THE HEAD WITH A CHUCKLE, THEN LIFTS THE FACE SHIELD]
Oh dear -- Richard III.  [HALF SHOUTS]
What are you going to do?

**Edmund**
Well, quick, quick... [he turns the body over, takes the head back
         and tries to replace it, asking Baldrick to hold it in steady.  He
         moves the corpse's arms about, and beats on its chest.  Baldrick
         for a moment puts his face down, trying to resuscitate the body
         through the face shield.]

**Baldrick**
[POINTS TO SOMETHING OFF-SHOT]
My Lord!  That hut there!

        [THEY EACH GRAB A LEG AND DRAG THE BODY AWAY.  THE HEAD STAYS BEHIND.]

        [They enter a small cottage.  Baldrick is solely dragging the body
         now.  Edmund enters afterward, carrying a gauntlet.]

**Edmund**
[STILL ENTERING]
Come on! Come on!  Will you wait! Will you wait!

        [BALDRICK COLLAPSES EXHAUSTED ON THE CORPSE.]

**Edmund**
[CLOSING THE DOOR]
Ah, well done...  [He sits on a barrel, then
         notices that something's missing.]
Where's the head?

**Baldrick**
I thought you had it.

**Edmund**
Baldrick, I can't be expected to carry everything!

        [They hear someone approaching.  Edmund cowers; Baldrick prepares to
         strike down the intruder with some sort of blunt object.  The door
         opens, and Percy enters.]

**Edmund**
Percy, you brainless son of a prostitute!  Where have you been?

**Percy**
I've just proved that I'm a man!  Look what I've found!  [He proffers
        the head.]

**Edmund**
Oh, thank God.  Quick, Percy, quick -- put it down and let's get out
         of here!

**Percy**
No no no no!  I found it. It's mine!

**Edmund**
What do you mean it's yours?  [HE TRIES TO TAKE IT FROM PERCY.]

**Percy**
[DEFENSIVELY]
I'm going to use it to prove that I killed a nobleman!

**Edmund**
[STOPS TRYING TO TAKE THE HEAD]
And which nobleman, pray...?

**Percy**
Er... [he looks under the face shield, laughs, then holds the head
        proudly]
Well, it's the King, actually!

**Edmund**
[STARES AT PERCY QUITE INTENTLY]

**Percy**
[FRIGHTENEDLY TOSSES THE HEAD TO EDMUND]

**Edmund**
[FRIGHTENEDLY TOSSES THE HEAD TO BALDRICK]

**Percy**
[FRIGHTENEDLY TOSSES THE HEAD IN THE BARREL]

        [a bloodied, armoured man approaches the cottage and staggers in just
         as our three were about to leave]

**Man**
Lost!  Lost!  All is lost!  [HE COLLAPSES TO THE FLOOR]

**Edmund**
What?

**Man**
Flee!  Flee!

**Edmund**
Oh my god!  Quick -- let's get out of here!

**Man**
Take me with you!  [HE GRABS ONE OF EDMUND LEGS]

**Edmund**
Get your hands off!  [PERCY FEEBLY HELPS IN THIS PROCESS]

**Man**
If you leave me alone here, I'll die.

**Edmund**
If you don't leave *me* alone, I'll kill you myself!  [Baldrick
         bops the man on the head with his blunt object.  The man falls to
         one side.]
Now; leave him here, come on!  [Edmund, Baldrick, and
         Percy make their way out.]

**Man**
I'll give you money!  Ten thousand sovereigns!

        [After a moment, the man collapses to the floor.  The door opens,
         and Percy's head pokes in...]

        [cut to Edmund and Baldrick entering the great hall in the castle.
         Baldrick keeps running, but Edmund stops as he meets his mother.]

**Edmund**
[FRANTIC]
Mother!

**Mother**
Edmund, dear.  How did it go?

**Edmund**
Within seconds, Henry Tudor will be here at our gates!

**Mother**
Oh, but, Edmund, I'm not ready -- I haven't had a bath or anything.

**Edmund**
Mother, Henry is our enemy.  When his men get here, they'll
         brutally ravish you and every woman in the castle!

**Mother**
Ah, well, I shan't bother to change, then.

        [BALDRICK RUNS INTO THE DOORWAY ACROSS THE HALL.]

**Baldrick**
My Lord!

**Edmund**
What do you want?

**Baldrick**
Listen!

        [AN ARMY'S DRUMS CAN BE HEARD FAINTLY IN THE DISTANCE.]

**Edmund**
Oh my god!  They're here already!  [He begins to run down the hall,
         shouting.]
Run for your lives!  Run for the hills!

**Baldrick**
Er, My Lord, they're coming from the hills.

**Edmund**
[STILL SHOUTING]
Oh, sorry.  Run *away* from the hills!  Run away
         from the hills!  If you see the hills, run the other way!

        [PERCY ARRIVES.]

**Percy**
No, My Lord, it's all right -- they're flying the banners of our
        King Richard.

**Edmund**
Well, that's impossible -- he's dead, isn't he!

**Mother**
[SHOCKED]
King Richard, dead?

**Edmund**
[SUDDENLY NOT SO FRANTIC]
Yes... Errr, God knows how...

**Mother**
Oh, dear.  That's really upset the tulip cart.

**Edmund**
[FRANTIC AGAIN]
Those flags, Percy, are obviously just a cunning
         trick to deceive us into staying!

**Baldrick**
No, My Lord, I don't think it is a cunning trick.

**Edmund**
Well, no, it's not a particularly cunning trick, because we've
         seen through it!  [HE LOCKS THE MAIN ENTRANCE TO THE GREAT HALL.]
         But obviously they thought it was cunning when they thought it out.

**Baldrick**
What I mean, My Lord, is that I don't think they did think it out.

**Edmund**
What, you think someone else thought it up, and they've borrowed it
         for the occasion?

**Baldrick**
No, My Lord.  I don't think it's a trick at all.

**Edmund**
You don't think that riding up to a castle under someone else's
         banner is a trick?  [SARCASTICALLY]
Well, no, I suppose it isn't!

        [There's a banging on the main door.  Edmund screams and goes through
        the inner door.  The main door has been broken down.]

**Percy**
[HE AND BALDRICK REMAIN IN THE GREAT HALL.]
It's only your father.

**Harry**
[ENTERING WITH HIS ENTOURAGE]
Who locked that bloody door?

**Mother**
Richard, it's you!

**Harry**
Well, who did you expect it to be, woman?

**Mother**
Why, I thought it would be Henry Tunip.

**Harry**
Henry Tunip?  Have you lost your conkers?

**Mother**
So you won?

**Harry**
Yes, of course!  *We* won!  We won!  Victory!

        [GENERAL CHEERS FROM HIS ENTOURAGE.]

**Mother**
So, I suppose now *you* want to ravish me...

**Harry**
[SHOCKED]
Yes, yes, in a moment...  [HE TURNS TO LORD CHISWICK, ONE OF HIS ENTOURAGE.]
The woman's insatiable!  [HE SHOUTS.]
Three cheers for good King Richard!  Hap hap! ["HUZZAH!"]
Hap hap! ["HUZZAH!"]
Hap hap! ["HUZZAH!"]

        [EDMUND APPEARS FROM THE OPPOSITE END HE LEFT, BEHIND THE GROUP.]

**Edmund**
[WEAKLY]
Huzzah...

**Harry**
All we need now is for King Richard to be here, and the day shall complete!

**Mother**
Yes, what a pity he's dead.

**Harry**
[SHOCKED WHISPER]
What?  Who told you that?

**Mother**
Well, Edmund.  [NODS TO HIS DIRECTION]

**Harry**
[HE AND THE GROUP TURN TO FACE EDMUND.]
Is this true?

**Edmund**
[QUITE INTIMIDATED, AS WELL AS FEARING FOR HIS LIFE]
Errr, well,
         I wouldn't know, really.  I was...nowhere near him at the time.
         I... I just...heard from someone that he'd, er... er... I mean,
         I don't even know where he was killed.  I was completely on the
         opposite side of the field.  I was nowhere near the cottage.

        [EVERYONE QUESTIONS THAT LAST STATEMENT, WITH STARES.]

**Edmund**
...not that it was a cottage -- it was a river.  But, then,
         I wouldn't know, of course, because I wasn't there.  But,
         apparently, some fool cut his head off...or at least killed him
         in some way...perhaps...took an ear off or something.  Yes, yes,
         in fact, I think he was only wounded! er, or was that somebody
         else?  Yes, I think it was.  Why, he wasn't even wounded!

        [Harry is staggering in behind Edmund, carrying the headless corpse,
         and the crown.]

**Edmund**
[NOT NOTICING HARRY]
Why, did someone say he was dead?

**Harry**
Yes!

**Harry**
What!

**Harry**
It's true, My Lord!  I stumbled on his body myself!  O, pardon me,
        thou bleeding piece of earth!  [He places the body on the floor,
        and lies on top of it.]

**Harry**
Er, yes...

**Harry**
Good night, sweet [KING? (IT'S NOT 'PRINCE')]...        \                                                        >
**Harry**
Yes, yes, that's enough of that, thank you, Harry...  /

**Harry**
...and flights of angels sing thee to thy [?]!              \                                                            >
**Harry**
Thank you, Harry... [SHOUTS, ANNOYED]
Thank you, Harry!  / [ANGERED WHISPER]
Yes! ...and we all know who did this dreadful deed -- [HE LOOKS AT EDMUND]
don't we?

        [Edmund slowly nods, as a sort of confession, and closes his eyes,
         preparing to have his head cut off.]

**Harry**
Henry Tudor!

        [EDMUND'S NOD INCREASES IN SPEED, HE OPENS HIS EYES AND GRINS.]

**Harry**
Yes! and he still roams free!  [HE SHOUTS QUITE LOUDLY.]
Harry, call for silence!

        [EVERYONE IS SILENT.]

**Harry**
[SHOUTS]
Silence!  [He slowly lowers the the crown onto his father's
        head.] ...for the King!  [Everyone, including Harry, kneels or bows
        before Richard.]

Everyone but **Harry**
Long live King Richard IV!

King (previously 'Richard'):  This day has been as 'twere                     A mighty stew                     In which the beef of victory                     Was mix'd                     With the vile turnip                     Of sweet Richard slain                     And the grisly dumpling                     Of his killer fled.                     But we must eat                     The yellow wobbly parts                     [?]
two [?]
serves.                     In life, each man gets                     What he deserves!

        [HIS SPEECH OVER, KING LOOKS AROUND AT THE KNEELED ASSEMBLY.]

**King**
[NONCHALANT]
Well, come on -- let's go and kill some more prisoners.

        [HIS ORIGINAL ENTOURAGE STANDS UP EXCITEDLY.]

**King**
Hap hap! ["HUZZAH!"]
Hap hap! ["HUZZAH!"]
Hap hap! ["HUZZAH!"]

        [Cut to Edmund's room.  He, Percy and Baldrick enter, dejectedly.
         Once they're in and the door closes, Edmund slowly turns and begins
         begins to grin.]

**Edmund**
Hurray!  [THE OTHERS ARE EXCITED NOW TOO.]
We're safe! and I am
         a prince of the realm!  Hap hap! [Baldrick says "Huzzah!" first,
         as Percy forgets the word.]
Can you imagine the power...

**Percy**
...and it's ours! all ours!

**Edmund**
What?

**Baldrick**
*Yours could* be 'Coverdale')]

**Edmund**
Lord Coverdale...

**Harry**
...who fought on *our* side, I believe.

**Edmund**
Er, yes... I think Lord Coverdale saw me slaying, erm...

        [Baldrick turns 90 degrees, turns his head and looks out the corner
         of his eye, then tilts his helmet over his eyes in an effeminate
         pose.]

**Edmund**
...Warwick.

**Harry**
Warwick the Wild of Leicester?

**Edmund**
Yes, that's him -- and pretty wild he was, too!  He took some
         finishing off, I can tell you!

**Harry**
Yes, indeed -- I killed him myself at one point.  Anyone else?

**Edmund**
Erm...erm...let me see... Just trying to put names to faces...

**Harry**
Yes, well, this is the list of the lords as yet unaccounted for:
        Roger de Runcie...

**Edmund**
Oh, de Runcie, yes, he was one of mine.

**Harry**
Lord Thomas of Devon...

**Edmund**
Ah, yes, backslash...

**Harry**
Lord Yeovil...

**Edmund**
Ah, yes, groin job...

**Harry**
Good lord!  This is remarkable, Edmund!  Remarkable!
        Oh, and the Bishop of Bath and Wells--

**Edmund**
Ah, yes, will never walk again!

**Harry**
...will conduct the thanksgiving service.

**Edmund**
Oh, Bath and *Wells*...

**Harry**
[TURNS TO PERCY.]
Ah, Lord Percy!  Edmund tells me that you managed
        to turn up late for the battle, [HE BEGINS WALKING OUT]
so there's
        not much point in asking you your score, is there?  [LEAVES]

        [Percy tries to speak, but can't think of anything.  He's upset.
         He turns to face Edmund.]

**Edmund**
Ha hah!!!

**Baldrick**
Ha hah!!!

**Percy**
[BITTERLY SARCASTIC]
Ha hah...

**Edmund**
At last I can relax!  [He opens the curtain to his bed, to find the
         dying man lying in it.  He turns back to Percy and Baldrick, and
         speaks quietly.]
Who the hell is this?

**Percy**
Ah, well, you remember that dying man we saw in the cottage?

**Edmund**
The one I specifically told you not to bring back to the castle
         under any circumstances?

**Percy**
Mm hmm, yes, that's the one, yes.

**Edmund**
So what is he doing in my bed?

**Percy**
Well, he claims to be a wealthy man.  I thought, if we nurse him
        back to health, he may reward us.

**Edmund**
No, wait -- I think I have an idea...  If he is a wealthy man, and
         we nurse him back to health, he may reward us!

**Baldrick**
Oh, brilliant, My Lord -- very quick thinking.

Edmund and **Percy**
Thank you, Baldrick.  [EDMUND EYES PERCY ANGRILY.]

**Edmund**
Well, what would you expect?  After all, who has the fastest
         brain in the land?

**Baldrick**
Prince Edmund, Duke of Edinburgh!

**Edmund**
Who is the boldest horseman in the land?  [LOOKING AT PERCY.]

**Baldrick**
Prince Edmund, Duke of Edinburgh!   \                                       >
**Percy**
[CATCHING ON] ...Duke of Edinburgh!   /

**Edmund**
Who is the bravest swordsman in the land?

**Percy**
Oh, don't tell me!  It's that [?]
from Norfolk...

**Edmund**
PRINCE...

Baldrick and **Percy**
Edmund, Duke of Edinburgh!

**Edmund**
Precisely.  [DRAMATICALLY]
Or, as I shall be known from now on:

         The Black...
         Vegetable!

**Baldrick**
My Lord, wouldn't something like 'The Black Adder' sound better?

**Edmund**
No, wait -- I think I have a better idea...  What about:

         The Black...
         Adder!

        [Cut to scene of him choosing a new outfit.  He points to a black
         suit with a coiled snake on it and a black cape; a pair of black
         shoes, more suited to a jester; a black bowl for haircut style.
         Cut to finishing of his haircut -- very short hair.  He looks in a
         mirror, and stands up.  Camera pans down to look at his entire
         outfit... large black rings, black tights and all.]

        [Cut to an inner hallway.  Edmund, Baldrick and Percy enter,
         laughing.]

**Baldrick**
Very witty, My Lord.

**Edmund**
Ah, thank you, Baldrick.

**Percy**
Very very very witty, My Lord.

**Edmund**
Ah, thank you, Percy.

**Baldrick**
You're certainly wittier than your father, My Lord.

**Percy**
...and head and shoulders over Richard III!

**Edmund**
[TURNS ON PERCY.]
IS THAT SUPPOSED TO BE WITTY?

**Percy**
Er, no, My Lord... No, no...that...that was...an example of the
        sort of thing that you yourself would not stoop to...

**Edmund**
GO AWAY!

Baldrick and **Percy**
Yes, My Lord.

        [Edmund enters his room, closing the door.  He hangs up his black
         hat, then goes to his bed, with the man in it.  The man is awake,
         having soup.]

**Edmund**
Ah, you're still here, are you?

**Man**
Er, yes.

        [Edmund looks closely at the man.  Viewers see a flashback to the
         opening of the show.  The man is Henry Tudor.]

**Edmund**
Wait a moment -- haven't I seen you somewhere before?

Henry (previously 'Man'):  I don't know.  I feel I've seen you before, also.

**Edmund**
Well, I am Prince Edmund, son of Richard IV!
         Why? Who are you?

Henry:  [SHOCKED TO DISCOVER WHERE HE IS]
Well, erm, I'm, er, not important.

**Edmund**
Not important?  You mean you're not rich?

Henry:  No.  [KNOWS THAT THAT WOULD MEAN DEATH.]
Yes! Yes, I'm incredibly
        rich!  I'm...I'm a very wealthy, errm, modest person, who wishes to
        remain nameless.

**Edmund**
Well, you'd better be rich.  Get your money together, get better,
         and get out of my bed, is that clear?  [HE SHUTS THE CURTAIN.]

        [Edmund looks around, uncovers a home-made crown, puts it on and
         looks at himself in the mirror.]

Ghost (of Richard III):  Oh yes, very fetching.

        [EDMUND TURNS, AND SCREAMS FOR ABOUT SIX SECONDS.]

Ghost:  ...and hello to *you*.

**Edmund**
Uh, uh, er, hello...hello...er...goodness me...I hadn't...
         expected...to see you...like this.

Ghost:  Sitting down, you mean?

**Edmund**
Er, yes, yes, that's right: sitting down.  Goodness, look!  Look!
         You're sitting down.

Ghost:  Yes.

**Edmund**
Why, I haven't seen you sitting down since, er...hoo...

Ghost:  Yesterday?

**Edmund**
Was it only yesterday?  Good lord!  Erm, errr...well...
         How was your battle?

Ghost:  Fine.  Somebody cut my head off at one point, but otherwise
        everything went swimmingly.  ...and how are you, Edna?

**Edmund**
Er, Edmund.

Ghost:  Your father told me 'Edna'.

**Edmund**
No...

Ghost:  So, Edna, you loathsome little fairy maggot, how are you?

**Edmund**
Er, how...how very very kind of you to ask, erm, Your Majesty...
         I'm very well, and, er, and it's very good to see you, because,
         frankly...

Ghost:  Yes?

**Edmund**
Well, well, well, frankly...er... Gosh, you look well.

Ghost:  Frankly what?  Spit it out, you horrid little scabby reptile!

**Edmund**
Er, well, frankly, everyone thought you were dead.

Ghost:  Well, frankly, [HIS HEAD RISES FROM HIS BODY TO BE LEVEL WITH EDMUND]
        I am.

**Edmund**
Eugh!

        [THERE'S A KNOCK AT THE DOOR.]

Ghost:  [TO THE DOOR]
Do come in.

**Edmund**
[RUSHING TO THE DOOR]
No!  Don't come in!

Queen (previously 'Mother'):  [FROM OUTSIDE THE DOOR]
Why not?                     Have you got someone in there with you?

**Edmund**
Erm, not as such...

Queen:  Is it a woman?

**Edmund**
No!

Queen:  Is it a man?

**Edmund**
Err, [HE WATCHES THE GHOST'S HEAD FLY ABOUT THE ROOM]
         err, yes, yes it is.

Queen:  You hesitated, Edmund -- it's not a sheep, is it?

**Edmund**
No, of course it isn't a sheep!

Queen:  Well then, let me in!

Ghost:  [BODY TOGETHER, STANDING]
So, farewell, Edna!  You'll be seeing
        me later.  [THE BODY WALKS OFF; THE HEAD REMAINS.]

**Edmund**
Erm, have, er, have you got...transport?  Erm, perhaps you'd like to
         borrow my horse again... [considers the possibility that the ghost
         doesn't know its slayer]
or at all! I mean, not that you've
         borrowed it before...

Ghost:  [THE BODY RETURNS, GESTURING FOR THE HEAD TO FOLLOW.]
        Coming!  [LEAVES]

        [EDMUND OPENS THE DOOR.  QUEEN ENTERS.]

Queen:  Are you all right, Edmund?  [Edmund quickly removes -- and hides --
        his crown.]
Why, you look as though you've just seen a ghost!

**Edmund**
Er, yes?

Queen:  Hurry up, anyway -- you're expected at the banquet!

        [HENRY IS LISTENING FROM THE BED.]

**Edmund**
Erm, look, er, Mother, er... You won't tell anyone about my
         oversleeping, er, this morning and... and what have you, now
         will you?

Queen:  Now, would I, Edmund...  Do I tell people that your brother Harry
        is scared of spoons? or that your father has very small private
        parts?  [SHE MOVES FROM THE CLOSET TO THE BED.]

**Edmund**
[TRYING TO STOP HER]
Oh! Mother!

Henry:  [LIKE A SHEEP]
Baaaa! Baaaa!

Queen:  Oh, Edmund!  It's the lying I find so hurtful...

**Edmund**
[WITH UNEASY GRIN]
Baaaa...

        [Cut to banquet.  Edmund enters, and prepares to sit between his
         father and his brother -- in Richard III's seat.]

**Edmund**
So sorry I'm late...

**King**
HOLD!  YOU DARE SIT THERE, BOY?  That was King Richard's seat!
       Would you insult his ghost?

**Edmund**
Eugh, erm, no, no -- sorry.

        [GHOST APPEARS IN THE CHAIR, BUT ONLY EDMUND CAN SEE OR HEAR IT.]

Ghost:  Yes, find your own chair, you smelly little dog's pizzle!

**Edmund**
Eugh!  [HE GOES BACK TO THE HIS NORMAL SPOT AND THE END OF TABLE]

**King**
[SPEAKING ACROSS WHERE GHOST IS, TO HARRY]
How many prisoners have
       you got, Harry?

Ghost:  I'm not Harry -- I'm... I'm Richard.  *He's* Harry.         \                                                            >
**Harry**
I've still got the [?]
of [?]
down in the dungeons, Father. /

**King**
Send the [?]
to my room, will you?

**Harry**
Very well.  Do you want them hung?  \                                    >
Ghost:  [WAVING]
Hello?                    /

**King**
No -- fresh ones; I want to practice my backhand.   \                                                   >
Ghost:  Hello?  Is anybody there?                          /

**Harry**
Oh, I don't think you need to, the way you slaughtered Lord Snedley!

Ghost:  Hello???

**King**
Oh, I wish Uncle Dicky was here.

Ghost:  Don't 'Dicky' me, Ducky...

**King**
[stands, bangs gold wine pitcher on table thrice, then holds up his
       goblet and speaks]
Tonight, honoured friends, we are gathered
       to celebrate a great victory, and to mourn a great loss.  [Raises his
       goblet]
A toast: to our triumph!  ["OUR TRIUMPH!"] [Ghost looks
       quite bored.] ...and I raise a royal curse upon the man who slew
       Richard, our noble king!

Ghost:  [STANDS, POINTS TO EDMUND]
It was him!

**Edmund**
Oh my god!

**King**
Quiet at the end there!  [SHOUTS AGAIN]
Whoever it was...

Ghost:  [SEATED AGAIN]
It was him -- Edna!

**King**
Wherever he be...

Ghost:  He's down there at the end!

**King**
He shall be struck down!

Ghost:  Well then get on with it, you stupid oaf -- he's there!

**Edmund**
It wasn't me!

**King**
Who said that?

Ghost:  The idiot who killed me this afternoon!

**Edmund**
I didn't!

**King**
Well then, who did?

**Harry**
It *was* actually Edmund who interrupted, Sire.

Ghost:  Hang the little slug!

        [EDMUND SCREAMS AND CRAWLS UNDER THE TABLE.]

**King**
I WILL HAVE SILENCE!  [bangs pitcher on table once more.  Raises
       goblet again]
Another toast: to dead King Richard.

Ghost:  [DISGUSTEDLY]
Oh my god...

**King**
Gentlemen...  ["KING RICHARD."]

Ghost:  [STILL DISGUSTED]
Well, thank you, [?].  Thank you.  Thank you
        very much for nothing.  Thank you so much.  That's the last you'll
        be seeing of me...not that you've seen much of me, in any case.
        [HE HAS FADED AWAY]

        [Edmund, still on his knees on the floor, but now out from under
         the table, wipes his brow and sighs.]

**King**
Now that we have silence, we shall continue with the ceremony of
       desecration.  Produce the portrait of the pretender, Henry Tudor!

        [A man carries the portait down the room.  People hiss and make
         general noises of unpleasantness.]

**Edmund**
[RECOGNISING THE FACE AS THE MAN IN HIS BED]
Oh my god!
         [HE CRAWLS OUT OF THE ROOM ON HIS HANDS AND KNEES]

        [Follow Edmund down inner hallway.  From inside his room, Ghost
         opens the door.]

Ghost:  Good evening.

**Edmund**
Where's Henry Tudor!  [HE RUSHES TO THE BED]

Ghost:  [SUDDENLY IN THE BED]
Baaaa!

**Edmund**
Oh no!  Where is he?  Where is he?  [HE CHECKS THE CLOSET]

Ghost:  [suddenly inside the closet, wiggles his fingers, making spooky
         'woo!' noise]

**Edmund**
[HE LOOKS OUT THE WINDOW TO SEE A HORSEMAN RIDING OUT OF THE CASTLE.  HE RUNS TO THE DOOR, AND IT IS OPENED BY GHOST.  HE BOWS TO GHOST AS HE EXITS, AND SPEAKS SCAREDLY RESPECTIVELY.]
Thank you...thank you so much.

        [Edmund chases Henry on horseback out of the castle and into a meadow
         outside.  We see Ghost snap his fingers, and the meadow suddenly is
         foggy.  Edmund rides out of the fog, at a clearing in the woods, to
         find three old witches bent over a cauldron.]

Witches tutti:  Oooh... Oooh... Oooh... Oooh...

**Edmund**
[HAVING DISMOUNTED, HE STANDS NEXT TO THEM, AND CLEARS HIS THROAT.]

Witches tutti:  [STARTLED]
Oooh!

Goncril:  Hail!

Cordelia:  Hail!

Regan:  Hail!

Goncril:  Ruler of men...

Cordelia:  Ravisher of women...

Regan:  Slayer of kings!

**Edmund**
Be gone, hideous crones!

Goncril:  Be not afraid...

Cordelia:  Be not overcome with fear...

Regan:  Be not paralysed with terror...

**Edmund**
[BORED]
Why have you lured me here, you loathsome drabs?

Regan:  We bear good news.

**Edmund**
What news could such repulsive harbingers convey?

Cordelia:  To-day has brought misfortune...

Goncril:  But one day...

Witches tutti:  O, glorious day!

Cordelia:  One day...

Witches tutti:  O, happy day!

        [PAUSE]

**Edmund**
Yes?

Witches tutti:  You shall be king!

**Edmund**
[EXCITED]
Really?

Witches tutti:  Yes!  Your Majesty!  [THEY BOW]

**Edmund**
Well, that *is* good news, isn't it?  [MOUNTING HIS HORSE]
         God be with you, you snaggletoothed vultures!
         History, here I come!
