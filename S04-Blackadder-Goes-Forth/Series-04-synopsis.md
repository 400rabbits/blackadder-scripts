# Blackadder Goes Forth

## Captain Cook

When General Haig unveils his new strategy to move his drinks cabinet closer to Berlin, Blackadder volunteers to be Official War Artist.

## Corporal Punishment

Orders for Operation Insanity arrive and Blackadder breaches regulations by eating the messenger. Can the Flanders Pigeon Murderer avoid the firing squad?

## Major Star

The Russian Revolution produces two more appalling results: an offensive by Germany and a really offensice Charlie Chaplin impersonation by Baldrick.

## Private Plane

German machine-guns in front, British firing-squads behinc — the only way out it a tiddly up-up.

## General Hospital

The secret of the Great Plan is out. Ordered to find a spy in the hospital, Blackadder spots a man with a strong German accent, a beautiful nurse and a chance for three weeks in bed.

## Goodgyeee

Millions have died but the troops have advanced no further than an asthmatic ant with some heavy shopping. Now, at last, the final big push looms…
