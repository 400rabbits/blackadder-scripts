Blackadder IV, Episode 3 - Major Star
Black Adder IV, Episode 3

Major Star

---------------------------------------------------------------------------

# Blackadder goes Forth: Episode 3: Major Star

## DRAMATIS PERSONAE

E: Captain Edmund Blackadder
B: Private S Baldrick
G: Lieutenant The Honourable George Colthurst St. Barleigh
D: Captain Kevin Darling
M: General Sir Anthony Cecil Hogmanay Melchett

## SCENE 1: THE DUG-OUT

> George is sitting on the table oiling a cricket bat and whistling irritatingly. Blackadder is lying on his bed getting irritated. Blackadder does a big sigh.

**George**
You a bit cheesed off, Sir?

**Blackadder**
George, the day this war began I was cheesed off. Within ten minutes of you turning up, I finished the cheese and moved on to the coffee and cigars. And at this late stage, I'm in a cab with two lady companions on my way to the Pink Pussycat in Lower Regency Street.

**George**
Oh well, because if you are cheesed off, you know what would cheer you up? A Charlie Chaplin film. Oh, I love old Chappers, don't you, Cap?

**Blackadder**
Unfortuately, no, I don't. I find his films about as funny as getting an arrow through the neck and then discovering there's a gas bill tied to it.

**George**
Oh, beg pardon, Sir, but come off! His films are ball-bouncingly funny.

**Blackadder**
Rubbish!

**George**
Well ,alright, why let's consult the men for a casting vote, shall we? Baldrick!

> Baldrick enters.

**Baldrick**
Sir?

**George**
Charlie Chaplin, Baldrick. What do you make of him?

**Baldrick**
Oh, Sir, he's as funny as a vegetable that's grown into a rude and amusing shape, Sir.

**Blackadder**
So you agree with me. Not at all funny.

**George**
Oh, come on, skipper, it ain't fair. I haven't asked for all of this. When he kicked that fellow in the backside, I thought I'd die!

**Blackadder**
Well, if that's your idea of comedy, we can provide our own without expending a ha'penny for the privilege.

> He gets up and knocks Baldrick's cap to the ground. As Baldrick bends to pick it up, Blackadder kicks Baldrick in the pants.

There, did you find that funny?

**George**
Well, no of course not, Sir, but you see, Chaplin is a genius.

**Blackadder**
He certainly is a genuis, George. He invented a way of getting a million dollars a year for wearing a pair og stupid trousers. Did you find that funny, Baldrick?

**Baldrick**
What funny, Sir?

> Blackadder chucks Baldrick's hat on the floor again, Baldrick bends down and Blackadder kicks him.

**Blackadder**
That funny.

**Baldrick**
No, Sir, you mustn't do that to me, Sir, because that is a bourgois   act of repression, Sir.

**Blackadder**
What?

**Baldrick**
I think I smelt it, Sir, there's something afoot in the wind. The   huddled masses yearning to be free.

**Blackadder**
Baldrick, have you been through the diesel oil again?

**Baldrick**
No, Sir, I've been sopping the milk of freedom. Already our Russian   comrades are poised on the brink of Revolution. And here too, Sir,   the huddled what's-names such as myself, Sir, are ready to throw   off the hated oppressors like you and the Lieutenant. Present com   pany accepted, Sir.

**Blackadder**
Go and clean out the latrines.

**Baldrick**
Yes, Sir, right away, Sir.

**George**
Now the reason why Chaplin is so funny, because he's part of a great British music hall tradition.

**Blackadder**
Oh yes, the Great British Music Hall Tradition. Two men, with incred- ibly unconvincing Cockney accents going, "what's up with you then? What's up with me then? Yeah, what's up with you then? (????????)" GET ON WITH IT!!!

**George**
Now, Sir, that was funny! You should have gotten a part yourself!

**Blackadder**
Thank you, George, but if you don't mind, I'd rather have my tongue beaten wafer-thin by a steak tenderiser and then stapled to the floor with a croquet hoop.

(loud voices are heard outside)

**Baldrick**
(rushing in) Sir, Sir, it's all over the trenches!

**Blackadder**
Well, mop it up then.

**Baldrick**
No, Sir, the news. The Russian Revolution has started. The masses   have risen up and shoveled their nobs!

**George**
Well, hurrah!

**Blackadder**
(reading a newspaper) Oh no, the Russians have pulled out of the war.

**George**
Well, we soon sawed them off, didn't we, Sir? Miserable slant-eye, sausage eating swine.

**Blackadder**
The Russians are on *our* side, George.

**George**
Oh really?

**Blackadder**
And they've abandoned the Eastern Front.

**Baldrick**
And they've overthrown Nicholas II who used to be bizzare.

**Blackadder**
Who used to be *the czar*, Baldrick. The point is, now that the Russians have made peace with the Kaiser. At this very moment, three quarters of a million Germans are leaving the Russian Front and coming over *here* with the express purpose of using my nipples for target practise. There's only one thing for it, I'll have to desert and I'm going to have to do it....right now.

(enter General Melchett)

**Melechett**
Are you leaving us, Blackadder?

**Blackadder**
No, Sir.

**Melechett**
Well I'm relieved to hear it. I need you to help me shoot more   deserters later on. There have been some subversive mutterings   amongst the men. You'll recall the French army last year at   Verdun where the top eschelons suffered from horrendeous uprisings   from the bottom.

**Blackadder**
Yes, Sir, but surely that was traced to a shipment of garlic eclairs.

**Melechett**
Nonsense Blackadder! It was bolshevist. Plain bolshevist! And now   that the Ruskys have followed suit, I'm damned if I can let the   same thing happen here.

**Blackadder**
Oh, and what are you going to do about it, Sir?

**Melechett**
I'm going to have a concert party to boost the men's morale.

**George**
A concert party, well, hurrah!

**Melechett**
You fancy an evening at a concert party, Blackadder?

**Blackadder**
Well frankly, Sir, I'd rather spend an evening on top of a stepladder in No Man's Land smoking cigarettes through an illuminous balacava (?).

**Melechett**
Well, I didn't think it would be your cup of tea, but I do need   someone to help me organise it, you know. Obviously not a tough   grizzled old soldier like yourself, but some kind of dandified   nancy-boy who will be prepared to spend the rest of the war in   the London Palladium.

**Blackadder**
The show's going to the London Palladium, Sir?

**Melechett**
Oh yes of course. No good crushing the Revolution over here only   to get back home to Blighty and find that everyone's wearing over-   alls and breaking wind in the palaces of the mighty.

**Blackadder**
Good point, Sir.

**Melechett**
So the thing is, Blackadder, finding a man to organise a concert   party is going t6o be damn difficult. So, I've come up with rather   a cunning set of questions with which to test the candidate's   suitability for the job.

**Blackadder**
And what sort of questions would these be, Sir?

**Melechett**
Well, the first question is, 'do you like Charlie Chaplin?'

**Blackadder**
(looks at George) Dismissed, Lieutenant. (George salutes and leaves) 'Do you like Charlie Chaplin?', yes that is a good question for a candidate, ah, to which my answer would of course be, 'yes, I love him, love him, Sir, particularly the amusing kicks.

**George**
That's what I said because I thought you said....

**Blackadder**
(abruptly) Goodbye George.

**Melechett**
And the second question is, 'do you like music hall?'

**Blackadder**
Ah, yes, another good question, Sir. Again, my answer would have to be 'yes, absolutely love it.' (mimiking) "Oops, Mr. Rothschild, (??)"

**Melechett**
Umm, yes. Well, it's in my view, Blackadder, that a person who   would answer 'yes' to both questions would be ideal for the jo-   (realises Edmund's early affirmative responses). Wait a minute.

**Blackadder**
What, Sir?

**Melechett**
(laughs) Why, without knowing it, Blackadder, you've inadvertently   shown me that you can do the job.

**Blackadder**
Have I, Sir?

**Melechett**
Yes, Sir! You have, Sir. And I want you to start work straight away.   A couple of shows over the weekend and if all goes well, we'll start   you off in London next Monday.

**Blackadder**
Oh...damn.

**Melechett**
If you need any help fixing and carrying and backstage and so on,   I'll lend you my driver if you like. (calls out) Bob?!

(a woman enters....the driver Bob)

Bob: (militaristically) Driver Parker reporting for duty, Sir!

**Melechett**
Alright, at ease, Bob, stand easy. Captain Blackadder, this is Bob.

**Blackadder**
Bob?

Bob: Good morning, Sir.

**Blackadder**
Unusual name for a girl?

**Melechett**
Oh yes, it would be an unusual name for a girl, but it's a perfectly   straightforward name for a young chap like you, eh Bob? Now Bob, I   want you to bunk up with Captain Blackadder for a couple of days, al-   right?

Bob: Yes, Sir.

**Melechett**
I think you'll find Bob just the man for this job, Blackadder. He has   a splendid sense of humour.

**Blackadder**
He, Sir? He? He?

**Melechett**
You see, you're laughing already! Well then, Bob, I'll leave you two   together, why don't you get to know each other, play a game of crim-   mage, have a smoke, something like that. They tell me that Captain   Blackadder has rather a good line in rough shag. Um, I'm sure he'd   be happy to fill your pipe. Carry on. (exits)

**Blackadder**
So you're a 'chap', are you Bob?

Bob: Oh yes, Sir. (laughs)

**Blackadder**
You wouldn't say you were a girl at all?

Bob: Oh, definitely not, Sir. I understand cricket, I fart in bed, everything.

**Blackadder**
Let me put it another way, Bob, you are a girl. And you're a girl with as much talent for disguise as a giraffe in dark glasses trying to get into a 'Polar Bears Only' golf club.

Bob: Oh, Sir, please don't give me away, Sir. I just wanted to be like my bro- thers and join up. I want to see how a real war is fought....so badly.

**Blackadder**
Well, you've come to the right place, Bob. A war hasn't been fought *this* badly since Olaf the Hairy, Chief of all the Vikings, accidently ordered 80,000 battle helmets with the horns on the *inside*.

Bob: I want to do my bit for the boys, Sir.

**Blackadder**
Oh really?

Bob: I'll do anything, Sir!

**Blackadder**
Yes, now keep that to yourself, if I was you.

(Edmund and Bob go over repetoire for concert hall show)

**Blackadder**
Alright Bob, the second half start with Corporal Smith and Johnson as the Three Silly Twerps.

Bob: Alright, Sir.

**Blackadder**
The big joke being that there's only two of them.

**Baldrick**
(laughing) I know that, it always cracks me up, Sir.

**Blackadder**
Followed by Baldrick's impersonation of Charlie Chaplin. Bob, take a telegram.

Bob: Yes, Sir.

**Blackadder**
Mr. C. Chaplin, Sennett Studios, Hollywood, California. (???) stop. Have discovered only person in the world less funny than you stop. Name Baldrick stop. yours, E. Blackadder stop.' Oh, and put a PS. 'Please please please stop.' Now after that, we have, ladies and gentlemen, the highlight of our show.

**Baldrick**
Ta-da...

(enter George in drag)

**George**
I feel fantastic!

**Blackadder**
Gorgeous Georgina, the traditional soldier's drag act.

**Baldrick**
You look absolutely lovely, Sir.

**Blackadder**
Well Baldrick, you are either, blind, or mad. The Lieutenant looks as all soldiers look on these occasions, about as feminine as W. G. Grace. What are you going to give them, George?

**George**
Well, I thought one or two cheeky gags, one followed by 'She was only the ironmonger's daughter but she knew a surprising amount about fish as well'.

**Blackadder**
(sarcatic) Inspired. Well, at least you made an effort with the dress, what is your costume, Baldrick?

**Baldrick**
I'm in it, Sir.

**Blackadder**
I see. So your Charlie Chaplin costume consists of only that hat.

**Baldrick**
Except that in this box, I've a dead slug as a brillaint false   moustache.

**Blackadder**
Yes, it's only quite brilliant, I fear. How, for instance, are you to attach it to your face?

**Baldrick**
Well, I was hoping to persuade the slug to cling on, Sir.

**Blackadder**
Baldrick, the slug is dead. If it failed to cling on to life, I see no reason that it should cling on to your upper lip.

**George**
Baldrick, Baldrick come on. Slugs are always a problem. What you do is screw your face up like this you see and you can clamp it between your top lip and your nose.

**Baldrick**
(leaning backward) What? Like this, Sir?

**George**
See, that's it, that's good. Sir, Sir, there's a visitor to see you.

**Blackadder**
(faking, but convincing) Good Lord, Mr. Chaplin! This is indeed an honour. Why, this calls for some sort of celebration. Baldrick, Bal- drick!

**George**
Sir, that is extraordinary, because, because this isn't Chaplin at all. This *is* Baldrick.

**Baldrick**
It is, it's *me*, Sir!

**Blackadder**
I know, I know. I was, in fact being sarcastic.

**George**
Oh, I see. Umm.

**Blackadder**
Everything goes above your head, doesn't it, George? You should go to Jamaica and become a limbo dancer.

(at the concert....backstage, George is seen giving encores)

Bob: They love him, Sir. We're a hit!

**Blackadder**
Yes, in one short evening, I've become the most successful impresario since the manager of the Roman Coliseum thought of putting the Christ- ians and the lions on the same bill.

**Baldrick**
Sir, some people seem to think I was best! Do you agree?

**Blackadder**
Baldrick, in the Amazonian rain forests, there are tribes of Indians yet untouched by civilisation who could develop more convincing Char- lie Chaplin impressionists.

**Baldrick**
Thank you very much, Sir.

Bob: (refering to George aka Georgina): He's coming out.

**George**
What do you think, Bob, one more? God, I love attention! (goes off stage to join Edmund and company) It's in my blood and soul. Bal- drick, put this in some water, will you?

(Baldrick dunks the flowers into the vase upside-down)

**George**
I need that applause in the same way that a osler needs his osle.

Bob: Well done, Sir!

**George**
(being modest) No, Sir, I really, I was hopeless. I mean, tell me honestly, Sir, I was, wasn't I?

**Blackadder**
Well...

**George**
No, no, no, come on, Sir. Out with it, cos I really need to know, I was hopeless.

**Blackadder**
No....

**George**
You're trying to be nice and that's very sweet of you, but, Sir, please, I can take it. I was hoepless.

**Blackadder**
George, you were bloody *awful*!

(George sobs.)

**Blackadder**
But you can't argue with the box office. Personally, I thought you were the least convincing female impressionist since Tarzan went through Jane's handbag and ate her lipstick. But I'm clearly in the minority. Look out London, here we come!

(at Melchett's headquarters, 'HQ'. Capt. Darling sits at his desk)

**Blackadder**
Ah, Captain Darling.

**Darling**
Ah, Captain Blackadder.

**Blackadder**
I must say, I had an absolutely splendid evening. Oh, glad you enjoyed the show.

**Darling**
The show? I couldn't go to the show. Important regimental business.

**Blackadder**
A lorry load of paper clips arrived?

**Darling**
Two lorry loads, actually.

**Melechett**
(enters) Ah, welcome to the great director, Miestrum.

**Blackadder**
You enjoyed it, Sir?

**Melechett**
Well, it was mostly awful, but I enjoyed the slug balance.

**Blackadder**
Private Baldrick, Sir.

**Melechett**
That's right, yes. The slug fell off a couple of times, but it   was....you can't have everything, can't you? I just suggest a   bit more practise and prehaps a sparkly costume for the slug.

**Blackadder**
I'll pass that on, Sir.

**Melechett**
But I do have certain others reasons for believing the show to be   nothing but a triumph. Captain Darling has your travel arrangements,   ticket to Dover, rooms at the Ritz and so forth.

**Blackadder**
Oh, thank you, Sir.

**Melechett**
However, there is one small thing you can do for me.

**Blackadder**
Yes?

**Melechett**
Captain Blackadder, I should esteem it a single honour if you would   allow me to escort your leading lady to the regimental ball this   evening.

**Blackadder**
My leading lady?

**Melechett**
The fair Georgina.

**Blackadder**
Ah, ha-ha, very amusing.

**Melechett**
You think she'll laugh in my face? I'm too old, too crusty?

**Blackadder**
Uh, no, no. It's just as her director, I'm afraid I could not allow it.

**Melechett**
I can always find another director who *would* allow it!

**Blackadder**
Quite. I'll see what I can do, but I must insist that she be home by midnight and that there'll be no hanky-panky, Sir, whatsoever.

**Melechett**
I shall, of course, respect your wishes, Blackadder. However I don't   think you need to be quite so protective. I'm sure she's a girl with   a great deal of spunk than most women you can find.

**Blackadder**
Oh, dear me.

(at the barracks)

**George**
Absolutely not, Sir. It's profoundly immoral, and utterly wrong. I will not do it.

**Blackadder**
We can always find another leading lady.

**George**
Well, the dress will need a clean.

**Blackadder**
Excellent. Now the important thing is, that Melchett should, under no circumstances, realise that you are a man.

**George**
Yes, yes, I understand that.

**Blackadder**
In order to insure this, there are three basic rules. One, you must never, I repeat, never remove your wig.

**George**
Right.

**Blackadder**
Second, never say anything. Tell him at the beginning of the evening that you're saving your voice for the opening night in London.

**George**
Excellent, Sir. And what's the third?

**Blackadder**
The third is most important, don't get drunk and let him shag you on the veranda.

(in Melchett's private quarters. The general puts on an impressive bemedaled
red jacket. Darling is with him.)

**Melechett**
(after a few sounds of self-satisfaction) How do I look, Darling?

**Darling**
Girl-bait, Sir. Pure bloody girl-bait.

**Melechett**
Moustache? Bushy enough?

**Darling**
Like a privet hedge, Sir.

**Melechett**
Good, because I want to catch a particularly beautiful creature in   this bush tonight.

**Darling**
You'll have her coming out of your moustache for a week, Sir.

**Melechett**
God, it's a spankingly beautiful world and tonight's my night. I   know what I'll say to her. 'Darling...'

**Darling**
(mistaken that the general's addressing him) Yes, Sir?

**Melechett**
What?

**Darling**
Um, I don't know, Sir.

**Melechett**
Well don't butt in! (exhales) 'I want to make you happy, darling'.

**Darling**
Well, that's very kind of you, Sir.

**Melechett**
Will you kindly stop interrupting? If you don't listen, how can you   tell me what you think? (continues) 'I want to make you happy, dar-   ling. I want to build a nest for your ten tiny toes. I want to cover   every inch of your gorgeous body in pepepper and sneeze all over you.'

**Darling**
I really think I must protest!

**Melechett**
What is the matter with you, Darling?

**Darling**
Well, it's all so sudden, I mean the nest bit's fine, but the pether  business is definitely out!

**Melechett**
How dare you tell me how I may or may not treat my beloved Georgina?

**Darling**
Georgina?

**Melechett**
Yes, I'm working on what to say to her this evening.

**Darling**
Oh yes. Of course. Thank God.

**Melechett**
Alright?

**Darling**
Yes, I'm listening, Sir.

**Melechett**
Honestly Darling, you really are the most graceless, dim-witted   pumpkin I ever met.

**Darling**
I don't think you should say that to her.

(Melchett groans)

(at the barracks)

**Blackadder**
Where's that George? It's three o'clock in the morning, he should be careful wandering the trench at night with nothing to protect his honour but a cricket box.

George (entering): Hello Captain.

**Blackadder**
About time, where the hell have you been?

**George**
Well I don't know, it's all been like a dream, my very first ball. The music, the dancing, the champagne, my mind is a mad world. Half whispered conversation with the promise of indisretion ever hanging in the air.

**Blackadder**
No, that old stoke Melchett tried for a snog behind the fruit cup.

**George**
Certainly not! The general behaved like a perfect gentleman. We tired the moon with our talking about everything and nothing. The war, mar- riage, proposed changes of the LBW rule.

**Blackadder**
Melchett isn't married, is he?

**George**
No, no, all his life, he's been waiting to meet the perfect woman. And tonight, he did.

**Blackadder**
Some poor unfortunate had Old Walrus-face dribbling in her ear all evening, did she?

**George**
Well yes. As a matter of fact, I did have to drape a napkin over my shoulder, yes.

**Blackadder**
George, are you trying to tell me that you're the General's perfect woman?

**George**
Well, yes, I rather think I am.

**Blackadder**
Well thank God the horny old blighthead didn't ask you to marry him.

(George stares out to Edmund, affirming this fact in silence)

**Blackadder**
He did?! Well how did you get out of that one?

**George**
Well, to be honest, Sir, I'm not absolutely certain that I did.

**Blackadder**
WHAT?!

**George**
You don't understand what it was like, Sir. You know, the candles, the music, the huge moustache, I can't remember it. (?)

**Blackadder**
You said 'yes'?

**George**
Oh, well he is a general, I didn't really feel I could refuse. He might have me court-martialed.

**Blackadder**
Whereas on the other hand, of course, he's going to give you the Victoria Cross when he lifts up your frock on the wedding night and finds himself looking at the blast turkey at the shop.

**George**
Yes, I, I, I know it's mess, ah but, you see, he got me scriffy and then when he looked into my eyes and said 'Chipmunk, I love you.'

**Blackadder**
CHIPMUNK???

**George**
It's a special name for me, you see, he says my nose looks just like a chipmunk's.

**Blackadder**
Oh God! We're in serious serious trouble here. If the General ever finds out that Gorgeous Georgina is, in fact, a strapping six footer from the rough end of the trench, which will precipitate the fastest execution since someone said, 'this Guy Fawlkes bloke, do we let him off, or wot?'

(phone rings, Edmund answers it)

**Blackadder**
Hello? Yes, Sir. Straight away, Sir. (hangs up) That was your finacee, 'Chipmunk'. He wants to see me. If I should die, think only this of me, 'I'll be back to get ya!'.

(at HQ again)

**Blackadder**
Sir, I can explain everything.

**Melechett**
Can you, Blackadder? Can you?

**Blackadder**
Well.....no, Sir, not really.

**Melechett**
I thought not, I thought not. Who can explain the mysteries of love?   I'm in love with Georgina, Blackadder. I'm going to marry her on Sa-   turday and I want you to be my best man.

**Blackadder**
I don't think that would be a very good idea, Sir.

**Melechett**
And why not?

**Blackadder**
Because there's something wrong with your finacee, Sir.

**Melechett**
Oh my God, she's not Welsh, is she?

**Blackadder**
No, Sir. Um, it's a terrible story, but true. Just a few minutes ago Georgina arrived unexpectedly in my trench. She was literally dancing with joy as if something wonderful had happened to her.

**Melechett**
Makes sense.

**Blackadder**
Unfortunately, she was in such a daze, danced straight throught the trench and out into No Man's Land. I tried to stop her, but before I could say, 'Don't tread on a mine', she trod on a mine.

(Melchett starts to sob)

**Blackadder**
When I say 'a mine', it was a cluster of mines, and she was blown to smitereens, rocketed up into the air, said something I couldn't quite catch, totally incomprehensible to me, something like, 'Tell him, his little chipmunk will love him forever'.

(Melchett howls in sadness)

**Darling**
It's heartnreaking, Sir.

**Blackadder**
I'm sorry, Sir.

**Melechett**
(recovering) Oh well, can't be helped, can't be helped.

**Darling**
Jolly bad luck, Sir. Of course, on top of everything else, without   your leading lady, you won't be able to put on your show. So no   show, no London Palladium.

**Blackadder**
On the contrary, I'm simply intending to rename it, the Georgina Melchett Memorial Show.

**Melechett**
Oh no, Georgina was the only thing that made the show come alive.   Apart from her, it was all awful!

**Darling**
Awful!

**Melechett**
You'll never find a girl like Georgina by tommorrow.

**Blackadder**
Well, it's funny you should say that, Sir, because I think I already have.

**Melechett**
Who is she?

**Darling**
Who is she?

(back at the barracks)

**George**
(as his 'normal' male self) So, come on, Sir, who is she?

**Blackadder**
Well, that's the problem. I haven't a bloody clue! The only exacting woman around here is carved out of stone called 'Venus' and is stan- ding in a fountain in the town square with water coming out of her armpits.

**George**
So we're a bit stuck.

Bob: (passing through) Morning chaps.

Edmund and **George**
Morning Bob.

**Blackadder**
You can say that again, George. We're in a stickier situation since Sticky the Stick Insect got stuck on a sticky bun. We are in trouble.

(enter Baldrick in drag)

**Baldrick**
No anymore, Sir. May I present my cunning plan.

**Blackadder**
Don't be ridiculous, Baldrick. Can you sing, can you dance? Or are you offering to be sawn in half?

**Baldrick**
I don't think those things are important in a modern marriage, Sir.   I offer simple home cooking.

**Blackadder**
Our plan is to find a new leading lady for our show. What is your plan?

**Baldrick**
My plan is that I will marry General Melchett. I am the other woman.

**George**
Well, congradulations Baldrick. I hope you will be very happy.

**Baldrick**
I will, Sir, cos when I get back from honeymoon, I will be a member   of the aristocracy and you will have to call me 'M'lady'.

**Blackadder**
What happened to your Revolutionary principles, Baldrick? I thought you hated the aristocracy.

**Baldrick**
I'm working to bring down the system from within, Sir. I'm a sort   of a Frozen Horse.

**Blackadder**
*Trojan House*, Baldrick.

**Baldrick**
Anmyway, I can't see what's so stupid about marry into wealth for   money and not having to sleep in a puddle.

**Blackadder**
Baldrick, NO! It's the worst plan since Abraham Lincoln said, 'Oh I'm sick of kicking around the house tonight. Let's take in a show.' And for a start, General Melchett is in mourning for the woman of his dreams. He's unlikely to be in the mood to marry a two legged badger wrapped in a curtain.. Anyway we are looking for a great entertainer and you're the worst entertainer since St. Paul the Evangelist toured Palestine with his trampoline act. Nah, we have to find somebody else.

**George**
What about Corporal Cartwright, Sir?

**Blackadder**
Corporal Cartwright looks like an orangatang. I've heard of the Bearded Lady, but the All Over Body Hair Lady simply just isn't on.

**George**
Willis?

**Blackadder**
Too short.

**George**
Petheridge?

**Blackadder**
Too old.

**George**
Taplowe?

**Blackadder**
Too dead. Ah, this is hopeless. There just isn't anyone!

(Bob is heard singing)

Bob: 'Goodbyeee, goodbyeee, wipe the tear, baby dear, from your eyeee'.

**Blackadder**
What am I doing? (calls out) Bob!

Bob: (naked but for a towel): Sir?

**George**
What a brilliant idea! Bob, can you think of anyone who can be our leading lady?

(at another concert performance)

**George**
What do you think, Bob, one more?

Bob: No George, always leave them hungry.

**Blackadder**
Congradulations, Bob. I must admit, I thought you were bloody mar- vellous.

Bob: Thank you, Sir. Permission to slip into something more uncomfortable, Sir.

**Blackadder**
Permission granted.

**Baldrick**
Oh, Sir, it's going to be wonderful. Not just for me, but for my little   partner, Graham. Doing our tour halfway 'round the world.

**Blackadder**
Yes, from Shaftsbury Avenue to the Co^te du Jour, they'll be saying, 'I like the little black one, but who's that berk he's sitting on?'

**Baldrick**
I'm not with you, Sir.

**Blackadder**
No, of course not. But don't worry, we'll have years in luxury hotels for me to explain. Now get packing, get packing. The boat-train leaves at six and we're going to be on it.

**Darling**
(entering) Blackadder.

**Blackadder**
Ah Darling, everything alright?

**Darling**
Oh yes.

**Blackadder**
Got the tickets?

**Darling**
Oh yes.

**Melechett**
[calling, enters] Blackadder!?

**Blackadder**
Oh hi, General. Enjoy the show?

**Melechett**
Don't be ridiculous, the worst evening I've ever spent in my life!   (paces forward toward Edmund)

**Blackadder**
(pacing backward) I'm sorry?

**Melechett**
(yells) Will you stand still when I'm talking to you! If by a man's   works shall ye know him, then you were a steaming pile of horse manure.

**Blackadder**
But surely, Sir, the show was a trimuph.

**Melechett**
(yells real loudly) TRIMUPH? The Three Twerps were one Twerp short,   again; the Slug Balancer seems now to be doing some feeble impres-   sion of Buster Keaton; and worst of all, the crowning turd in the   waterpipe, that revolting drag act in the end.

**Blackadder**
Drag?

**Melechett**
Yes, poor Bob Parker's been made to look a total arse! With that reedy   voice and that stupid effeminate dancing.

**Darling**
So the show's cancelled; permenantly. (rips up plane tickets)

**Blackadder**
But what about the men's morale, Sir, with the Russians out of the war and everything?

**Melechett**
Oh for goodness sake, Blackadder, have you been living in a cave?   The Amercians joined the war yesterday.

**Blackadder**
So how is that going to improve the men's morale, Sir?

**Melechett**
OOooooohhh, because you jibbering imbecile, they've brought with   them the largest collection of Charlie Chaplin films in existence.   I've lost patience with you. Fill him in, Darling. (exits)

**Darling**
We received a telegram this morning from Mr. Chaplin himself, at  Sennett Studios: [reads] 'Twice nightly screening of my films in  trenches, excellent idea stop. But must insist E. Blackadder be  projectionist. Oh PS, don't let him ever stop.'

**Blackadder**
Oh great.

**Darling**
No hard feelings, Blackadder.

**Blackadder**
Not at all Darling. Uh, care for a Liquorice Allsort?

**Darling**
[accepts it....which turns out to be Baldrick's dead slug] Well,  thank you. [eats it]

[CLOSING CREDITS]
                                ----------
                           Captain Edmund Blackadder
                                ROWAN ATKINSON

                             Private S. Baldrick
                                TONY ROBINSON

                           General Sir Anthony Cecil
                               Hogmanay Melchett
                                  STEPHEN FRY

                           Lieutenant the Honourable
                         George Colthurst St. Barleigh
                                  HUGH LAURIE

                             Captain Kevin Darling
                                 TIM McINNERNY

                                Title Music
                         Composed and Arranged by
                              HOWARD GOODALL

                                 Played by
                       The Band of the 3rd Battalion
                        The Royal Anglian Regiment
                             (The Pompadours)

                                Bandmaster
                             WOI TIM PARKINSON

                         P/BR.  647989  Libotte, J
                         Vis/E.  110143  Turner, R
                        Tech/Co. 364007  Massen, D
                          V/M  420372  Abbott, C
                        VTE.  614981  Wadsworth, C
                         Cm/S.  841842  Hoare, J
                         S/Svr.  733731  Deane, M
                         Dep/Svr.  713429  Way, N
                        L/Dr.  988212  Bristow, R
                        P/Mgr.  323476  Cooper, D
                       P/Att.  114209  Sharples, V
                         AFM  529614  Kennedy, J
                       C/Dgr.  368807  Hardinge, A
                        M/V Dgr.  82641  Noble, C
                          Dgr.  404371  Hull, C

                         Dir.  232418  Boden, R

                         Prod.  597602  Lloyd, J

                          (c) BBC TV MCMLXXXIX
