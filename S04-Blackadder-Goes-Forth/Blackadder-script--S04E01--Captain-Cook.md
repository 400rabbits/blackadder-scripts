Title:      Blackadder goes Forth: Episode 1: Captain Cook
Credit:     written by
Author:     Richard Curtis AND Ben Elton
Credit:     directed by
Director:   Richard Boden
Source:     NULL
Notes:      UNOFFICIAL TRANSCRIPTION FROM AIRED SHOW
Original air date: 28 september 1989
Copyright:   (c) 1989, British Broadcasting Corporation
Summary:   Captain Blackadder spots an opportunity to get away from the trenches when General Melchett announces a painting competition for the cover of "King and Country" magazine.

# Blackadder goes Forth: Episode 1: Captain Cook

## DRAMATIS PERSONAE

E: Captain Edmund Blackadder
B: Private S Baldrick
G: Lieutenant The Honourable George Colthurst St. Barleigh
D: Captain Kevin Darling
M: General Sir Anthony Cecil Hogmanay Melchett

## SCENE 1: THE DUG-OUT

> A dug-out in a trench in the middle of the Great War. There is a general atmosphere of mud. It contains table, chair, bed, Captain Blackadder and Private Baldrick. "Pomp and Circumstance" playe on the gramophone

Blackadder is reading, but there is a tiny annoying scratching sound. He shifts slightly, trying to ignore it but finally, can't... He lifts the needle on the gramophone, but the scratch continues...

**Blackadder**
Baldrick, what are you doing out there?

**Baldrick**
I'm carving something on this bullet, Sir.

**Blackadder**
What are you carving?

**Baldrick**
I'm carving "Baldrick", Sir.

**Blackadder**
Why?

**Baldrick**
It's a cunning plan, actually.

**Blackadder**
Of course it is.

**Baldrick**
You see, you know they say that somewhere there's a bullet with your name on it?

**Blackadder**
Yes?

**Baldrick**
Well, I thought if I owned the bullet with my name on it, I'd never get hit by it, 'cos I won't ever shoot myself.

**Blackadder**
Oh, shame.

**Baldrick**
And, the chances of there being two bullets with my name on them are very small indeed.

**Blackadder**
That's not the only thing around here that's "very small indeed". Your brain for example, is so minute, Baldrick, that if a hungry cannibal cracked your head open there wouldn't be enough inside to cover a small water-biscuit.

> George enters with a nice, new revolverand a magazine.. He isa very enthusiastic, bright-eyed and bubble-header young officer

**George**
Tally-ho, pip-pip and Bernard's your uncle.

**Blackadder**
In English we say, "Good Morning".

**George**
[EXCITED]
Look what I got for you, Sir.

**Blackadder**
What?

> Baldrick goes outside into the trench.

**George**
It's the latest issue of *King & Country*. Oh, damn inspiring stuff. "The magazine that tells the Tommies the truth about the war".

**Blackadder**
Or alternatively, the greatest work of fiction since vows of fidelity were included in the French marriage service.
[FLICKS THROUGH PAPER]

**George**
Come, come, Sir, now. You can't deny that this fine newspaper is good for the morale of the men.

**Blackadder**
Certainly not, I just think that more could be achieved by giving them some real toilet-paper.

[HANDS PAPER BACK TO GEORGE]

**George**
Not with you at all, Sir, what could any patriotic chap have against this magnificent mag?

**Blackadder**
Apart from his bottom?

**George**
Yes.

**Blackadder**
Well look at it.

[TAKES THE PAPER AGAIN]

I mean the stuff's about as convincing as Dr. Crippen's defence lawyer. The British Tommies are all portrayed as six foot six with biceps the size of Bournemouth.

**George**
Exactly. Thoroughly inspiring stuff. And look, Sir, this also arrived for you this morning.

> George holds out a gun wrapped in a brown paper bag. Blackadder unwraps it and handles it thoughtfully.

**Blackadder**
Hmm, do you know what this is, Lieutenant?

**George**
It's a good old service revolver.

**Blackadder**
Wrong. It's a brand new service revolver, which I've suspiciously been sent without asking for it. I smell something fishy, and I'm not talking about the contents of Baldrick's apple crumble.

**George**
That's funny, Sir, because we didn't order those new trench-climbing ladders either.

**Blackadder**
New ladders?

**George**
Yeah, came yesterday. I issued them to the men, and they were absolutely thrilled.

> He shouts out to the trench.

Isn't that right men?

> Baldrick appears at the entrance to the dug-out.

**Baldrick**
Yes, Sir, first solid fuel we've had since we burned the cat.

> Blackadder goes out to the trench, followed by George.

**Blackadder**
Something's going on, and I think I can make an educated guess what it is. Something which you, George, would find hard to do.

**George**
Ah, true, true. Where I was at school, education could go hang as long as a boy could hit a six, sing the school song very loud, and take a hot crumpet from behind without blubbing.

**Blackadder**
I, on the other hand, am a fully rounded human being with a degree from the university of life, a diploma from the school of hard knocks, and three gold stars from the kindergarten of getting the shit kicked out of me. My instincts lead me to deduce that we are at last about to go ... over the top.

[PEERS OVER THE TOP OF THE TRENCH WITH A PERISCOPE]

**George**
Great Scott, Sir, you mean, you mean the moment's finally arrived for us to give Harry Hun a darned good British style thrashing, six of the best, trousers down?

**Blackadder**
If you mean, "Are we all going to get killed?" Yes. Clearly, Field Marshal Haig is about to make yet another gargantuan effort to move his drinks cabinet six inches closer to Berlin.

**George**
Right! Bravo-issimo! Well let's make a start eh, up and over to glory, last one in Berlin's a rotten egg.

> He makes a move to charge up and over the trench walls.

**Blackadder**
Give me your helmet, lieutenant.

> Geroge gives Blackadder his hat. He throws it up in the air. Massive machine gun fire is heard. The hat descends totally perforated with bullet holes.

[GEORGE HANDS HIS HELMET TO BLACKADDER, WHO THROWS IT UP INTO THE SKY. IMMEDIATELY HEAVY MACHINE-GUN FIRE IS HEARD. HE CATCHES THE HELMET, WHICH NOW HAS OVER 20 HOLES IN IT, AND GIVES IT BACK TO GEORGE.]

**George**
Yes, some sort of clever hat-camouflage might be in order.

>He heads off sparkily.

**Baldrick**
Permission to speak, Sir.

**Blackadder**
Granted— with a due sense of exhaustion and dread.

**Baldrick**
I have a cunning plan to get us out of getting killed, Sir.

**Blackadder**
Ah yes, what is it?

**Baldrick**
Cooking.

**Blackadder**
[ENTERS THE DUGOUT AGAIN]
I see.

**Baldrick**
You know staff HQ is always on the lookout for good cooks? Well, we go over there, we cook 'em something, and get out of the trenches that way.

**Blackadder**
Baldrick, it's a brilliant plan.

**Baldrick**
Is it?

**Blackadder**
Yes, it's superb.

**Baldrick**
[DELIGHTED]
Permission to write home immediately, Sir, this is the first brilliant plan a Baldrick's ever had! For centuries we've tried, and they've always turned out to be total pig-swill. My mother will be as pleased as Punch.

**Blackadder**
Hm-hm, if only she were as good-looking as Punch, Baldrick. There is however one slight flaw in the plan.

**Baldrick**
Oh?

**Blackadder**
You're the worst cook in the entire world.

**Baldrick**
Oh yeah, that's right.

**Blackadder**
There are amoeba on Saturn who can boil a better egg than you. Your Filet Mignon in sauce béarnaise look like dog-turds in glue.

**Baldrick**
That's because they are.

**Blackadder**
Your plum-duff tastes like it's a molehill decorated with rabbit-droppings.

**Baldrick**
I thought you wouldn't notice.

**Blackadder**
Your cream custard has the texture of cat's vomit.

**Baldrick**
Again, it's.....

**Blackadder**
If you were to serve one of your meals in staff HQ you'd be arrested for the greatest mass poisoning since Lucretia Borgia invited 500 of her close friends around for a wine-and-anthrax party. No, we'll have to think of a better plan than that.

**Baldrick**
Right, how about a nice meal, while you chew it over?

**Blackadder**
[SUSPICIOUS]
What's on the menu?

**Baldrick**
Rat.

> Baldrick produces a rat as if a bottle of of fine wine.

Sautèed or fricassee?

**Blackadder**
[PEERS AT THE RAT]

Oh, the agony of choice. Sautée involves...?

**Baldrick**
Well, you take the freshly shaved rat, and you marinade it in a puddle for a while.

**Blackadder**
Hmm, for how long?

**Baldrick**
Until it's drowned. Then you stretch it out under a hot light bulb, then you get within dashing distance of the latrine, and then you scoff it right down.

**Blackadder**
So that's sautéeing, and fricasseeing?

**Baldrick**
Exactly the same, just a slightly bigger rat.

**Blackadder**
Well, call me Old Mr. Un-adventurous but I think I'll give it a miss this once.

[GEORGE ENTERS, WEARING A NEW HAT DECORATED WITH BARBED-WIRE.]

**Baldrick**
Fair enough, Sir, more for the rest of us.

[TO GEORGE]
Eh, Sir?

**George**
Absolutely, Private. Tally-ho BARF BARF.

> George has just popped back in.The hat on his head is camouflaged by a coil of barbed wire wrapped around it. The field telephone ring. Blackadder answers.

**Blackadder**
Hello, the Savoy Grill. Oh, it's you..... yes..... yes, I'll be over in 40 minutes.

> He puts the phone down.

**Baldrick**
Who was it then, Sir?

**Blackadder**
Strangely enough Baldrick, it was Pope Gregory IX, inviting me for drinks aboard his steam-yacht "The Saucy Sue", currently wintering in Montego Bay with the England Cricket team and the Balinese goddess of plenty.

**Baldrick**
Really?

**Blackadder**
No, not really. I'm ordered to HQ. No doubt that idiot General Melchett is about to offer me some attractive new opportunities to have my brains blown out for Britain.

> He puts on his greatcoat.

## SCENE 2: STAFF HQ

> It is a grand room, clearly purloined from a French mayor. There is a grand marble fireplace and a granddesk, at which Darling is sitting and writing. Maps cover the wall, filing cabinets and charts are off to one side. Blackadder enters.

**Blackadder**
What do you want, Darling?

**Darling**
It's Captain Darling to you. General Melchett wants to see you about a highly important secret mission.

**Melchett**
[ENTERS]
What's going on, Darling?

> Darling jumps up immediately from behind Melchett's desk

**Darling**
Captain Blackadder to see you, Sir.

**Melchett**
Ah, excellent. Just a short back and sides today I think, please.

**Darling**
Er, that's Corporal Black, Sir. Captain Blackadder is here about the other matter, Sir, the [LOWERS HIS VOICE] secret matter.

**Melchett**
Ah, yes, the special mission. At ease Blackadder. Now, what I'm about to tell you is absolutely tip-top-secret, is that clear?

**Blackadder**
It is, Sir.

**Melchett**
Now, I've compiled a list of those with security clearance, have you got it Darling?

**Darling**
Yes, Sir.

**Melchett**
Read it please.

**Darling**
It's top security, Sir, I think that's all the Captain needs to know.

**Melchett**
Nonsense! Let's hear the list in full!

**Darling**
Very well, Sir.

[READING]

"List of personnel cleared for Mission Gainsborough, as dictated by General C. H. **Melechett**
You and me, Darling, obviously. Field Marshal Haig, Field Marshal Haig's wife, all Field Marshal Haig's wife's friends, their families, their families' servants, their families' servants' tennis partners, and some chap I bumped into the mess the other day called Bernard."

**Melchett**
So, it's maximum security, is that clear?

**Blackadder**
Quite so, Sir, only myself and the rest of the English speaking world is to know.

**Melchett**
Good man!

> Melchett leads Blackadder across to the map table.

Now, Field Marshal Haig has formulated a brilliant new tactical plan to ensure final victory in the field.

[THEY GATHER AROUND A MODEL OF THE BATTLEFIELD]

**Blackadder**
Now, would this brilliant plan involve us climbing out of our trenches and walking slowly towards the enemy, Sir?

**Darling**
How can you possibly know that Blackadder? It's classified information.

**Blackadder**
It's the same plan that we used last time, and the seventeen times before that.

**Melchett**
E-E-Exactly! And that is what so brilliant about it! We will catch the watchful Hun totally off guard! Doing precisely what we have done eighteen times before is exactly the last thing they'll expect us to do this time! There is however one small problem.

**Blackadder**
That everyone always gets slaughtered in the first ten seconds.

**Melchett**
That's right! And Field Marshal Haig is worried that this may be depressing the men a tadge. So, he's looking to find a way to cheer them up.

**Blackadder**
Well, his resignation and suicide would seem the obvious solution.

**Melchett**
Interesting thought. Make a note of it, Darling!

**Darling**
[UNEASY]
Sir!

> But Melchett has another thought. He reaches intoa drawer.

**Melchett**
Take a look at this— I'm sure you know it.

* King & Country*.

**Blackadder**
Ah, yes, without question my favourite magazine; soft, strong and thoroughly absorbent.

**Melchett**
Top-hole Blackadder, I thought it would be right up your alley. Now, Field Marshal Haig's plan is this: to commission a man to do an especially stirring painting for the cover of the next issue, so as to really inspire the men for the final push. What I want you to do, Blackadder, is to labour night and day to find a first rate artist from amongst your men.

**Blackadder**
Impossible, Sir. I know from long experience that my men have all the artistic talent of a cluster of colourblind hedgehogs... in a bag.

**Melchett**
Hm, well that's a bit of a blow. We needed a man to leave the trenches immediately.

**Blackadder**
Leave the trenches?

**Melchett**
[AFFIRMATIVELY]
Hmm, mm.

**Blackadder**
Yes, I wonder if you've enjoyed, as I have, Sir, that marvellous painting in the National Portrait Gallery, "Bag Interior", by the Colour-Blind Hedgehog Workshop of Sienna.

**Darling**
I'm sorry, are you saying you *can* find this man?

**Blackadder**
I think I can. And might I suggest, Sir that having left the trenches, it might be a good idea to post our man to Paris

[POINTS NONCHALANTLY ON MELCHETT'S MAP],

in order to soak up a little of the artistic atmosphere. Perhaps even Tahiti [GESTICULATING AIRILY AT ANOTHER MAP], so as to produce a real masterpiece.

> Darling is alarmed.

**Melchett**
Yes, yes, but can you find the man?!

**Blackadder**
Now I know I can, Sir. Before you say "Sunflowers" I'll have Vincent van Gogh standing before you.

## SCENE 3: THE TRENCH

> The trench has been turned into an informal artists commune. Blackadder has an easel and Baldrick a drawing board. George is looking over Blackadder's shoulder, um-ing and ah-ing over the painting. Finally Blackadder stops, turns round and looks at him in annoyance.

**George**
No, don't stop, Sir. It's coming, it's definitely coming. I, hm, yeah, ah, er, hm. I just wonder if two socks and a hand-grenade is really the sort of thing that covers of *King and Country* are made of.

**Blackadder**
They will be when I painted them being shoved up the Kaiser's backside.
[GEORGE WALKS OVER TO BALDRICK.]

**George**
Ah, now, now this is interesting.

**Blackadder**
What is?

**George**
Well, Private Baldrick is obviously a bit of an impressionist.

**Blackadder**
The only impression he can do is of a man with no talent. What's it called, Baldrick? "The Vomiting Cavalier"?

**George**
That's not supposed to be vomit. It's dabs of light.

**Baldrick**
No, it's vomit.

**George**
Yes, now, err, why did you choose that?

**Baldrick**
You told me to, Sir.

**George**
Did I?

**Baldrick**
Yeah, you told me to paint whatever comes from within... so I did my breakfast. Look, there's a little tomato.

**Blackadder**
Hopeless. If only I'd paid attention in nursery art-class instead of spending my entire time manufacturing papier-mâché willies to frighten Sarah Wallis.

**George**
You know it's funny, but painting was the only thing I was ever any good at.

**Blackadder**
Well, it's a little pity you didn't keep it up.

> George goes into the dug-out, waffling aplogetically.

**George**
Well, as a matter of fact I did, actually. I mean...

[TAKES OUT PICTURES]

I mean normally I hadn't thought I would show them to anyone, because they're just embarrassing daubs really, but you know, ah, they give me pleasure. I'm embarrassed to show them to you now as it happens, but there you go, for what they're worth. To be honest, I should have my hands cut off, I mean...

**Blackadder**
George! These are brilliant! Why didn't you tell us about these before?

**George**
Well you know, one doesn't want to blow one's own trumpet.

**Blackadder**
You might at least have told us you had a trumpet. These paintings could spell my way out of the trenches.

**George**
Yours?

**Blackadder**
That's right, *ours*. All you have to do is paint something heroic to appeal to the simple-minded Tommy. Over to you, Baldrick.

**Baldrick**
How about a noble Tommy, standing with a look of horror and disgust over the body of a murdered nun, what's been brutally done over by a nasty old German.

**George**
Excellent. I, I can see it now— "The Nun and the Hun".

**Blackadder**
Brilliant! No time to lose. George, set up your easel. Baldrick and I will pose. This is going to be art's greatest moment since Mona Lisa sat down and told Leonardo da Vinci she was in a slightly odd mood. Baldrick, you lie down in the mud and be the nun.

**Baldrick**
I'm not lying down there, it's all wet.

**Blackadder**
Well, let's put it this way: either you lie down and get wet, or you're knocked down and get a broken nose.

**Baldrick**
Actually it's not that wet, is it?

**Blackadder**
No.

> He pushes Baldrick to the ground. There is a squelch of huge wetness. Baldrick looks up, his face covered in mud.

**Baldrick**
Who are you going to be then, Sir? The noble Tommy?

**Blackadder**
Precisely, standing over the body of the ravaged nun.

**Baldrick**
I want a wimple.

**Blackadder**
You should have gone before we started the picture.

**Baldrick**
You know, the funny thing is, my father was a nun.

**Blackadder**
[FIRMLY]
No he wasn't.

**Baldrick**
He was so, Sir. I know, 'cos whenever he was up in court, and the judge used to say "occupation?", he'd say "nun".

> George has been in the dug-out, changing. He returns in a big shirt and an berret, carrying a palette and easel. He is transformed as best he can into an artist.

**Blackadder**
Right. You're ready?

**George**
Just about, Sir, yes. Erm, if you just like to pop your clothes on the stool.

**Blackadder**
I'm sorry?

**George**
Just pop your clothes on the stool over there.

**Blackadder**
You mean, you want me... tackle out?

**George**
Well, I would prefer so, Sir, yes.

**Blackadder**
If I can remind you of the realities of battle George, one of the first things that everyone notices is that all the protagonists have got their clothes on. Neither we, nor the Hun, favour fighting our battles *au naturel*".

**George**
Sir, it's artistic licence. It's willing suspension of disbelief.

**Blackadder**
Well, I'm not having anyone staring in disbelief at *my willy suspension*. Now, get on and paint the bloody thing, sharpish!

> Blackadder strikes a pose for the picture.

## SCENE 4: THE DUG-OUT

> An hour later. Blackadder, George, and Baldrick are inspecing the painting.

**Blackadder**
Brilliant George— it's a masterpiece. The wimple suits you Baldrick.

**Baldrick**
But it completely covers my face.

**Blackadder**
Exactly. Now then, General Melchett will be here at any moment. When he arrives, leave the talking to me, all right? I like to keep an informal trench, as you know, but today you must only speak with my express permission, is that clear?

> They don't reply.

[SHARPLY]
Is that clear?

> Still no reply. Their eyes are bulging. Then at last Blackadder twigs.

[WITH A NOTE OF REGRET]
Permission to speak.

**George**
Yes, Sir. Absolutely.

**Baldrick**
Yes, Sir.

> From the trench a loud cry is heard:  "Ten Shun!"

**Darling**
[ENTERING]
Dugout— 'Ten-shun!!

[MELCHETT ENTERS.]

**Melchett**
Excellent, at ease. Now then, Blackadder, where would you like me to sit? I thought just a simple trim of the moustache today, nothing drastic.

**Darling**
We're here about the painting, Sir.

**Melchett**
Oh, yes, of course.

[SEEING GEORGE]
Good Lord! George! Hahahaaa, how are you my boy?

> George remains silent.

I said how are you?

> Blackadder not having been concentrating, suddenly realizes that George isn't speaking.

**Blackadder**
Permission to speak.

**George**
Absolutely top-hole, Sir, with a ying and a yang and a yippetty-doo.

**Melchett**
Splendid! And your uncle Bertie sends his regards. I told him you could have a week off in April; we don't want you missing the Boat Race, do we?

> George looks questioningly ay Blackadder— Blackadder suddenly clicks, and nods.

**Blackadder**
Permission to speak.

**George**
Certainly not. Permission to sing boisterously, Sir?

**Blackadder**
If you must.

**George**
Row, row, row your boat,

**Melchett**
[JOINS IN]
... Gently down the stream,
Belts off, trousers down!
Isn't life a scream.
Oi!

> All this is done with a little accompanying dance ending on a pelvic thrusting movement. Darling applauds enthusiastically and Blackadder claps drily.

**Blackadder**
Fabulous. University education— you can't beat it, can you?

**Melchett**
Bravo!

Now.

[MOVING ON TO BALDRICK]

What have we here? Name?

> Baldrick looks at Blackadder.

**Blackadder**
Permission to speak.

**Baldrick**
Baldrick, Sir.

**Melchett**
Ah, tally-ho, yippety-dip, and zing zang spillip. Looking forward to bullying off for the final chukka?

> Baldrick says nothing. Blackadder wishes he'd never started this.

**Blackadder**
Permission to speak.

> Baldrick still says nothing.

**Blackadder**
Answer the General, Baldrick.

**Baldrick**
I can't answer him, Sir, I don't know what he's talking about.

> Melechtt decides to speak to him like an intelligent foreigner.

**Melchett**
Aah. Are. You. Looking. Forward. To. The. Big. Push?

> He chucks his cheek indulgently.

**Baldrick**
> Answers the same way, and pinches the general's cheek in return.

No, Sir. I'm. Absolutely. Terrified.

**Melchett**
Mwaah! The healthy humour of the honest Tommy! Hahaaa, don't worry my boy, if you should falter, remember that Captain Darling and I are behind you.

**Blackadder**
About thirty-five miles behind you.

> Darling, rather annoyed by this twitches a little.

**Melchett**
Right, well stand by your beds. Let's have a look at this artist of yours, Blackadder. Next to me, Darling.

**Darling**
Thank you, Sir.
[SITS DOWN NEXT TO MELCHETT]

**Melchett**
So, ah, have you found someone?

**Blackadder**
Yes, Sir, I think I have. None other than young George here.

> George looks suitably modest.

**Melchett**
Oh, bravo. Well, let's have a shufti then.

> Blackadder takes out the picture he himself painted earlier.

**Blackadder**
This is called "War".

[SHOWS HIS OWN PAINTING]
> The painting is not good.

**Melchett**
Damn silly title George. Looks more like a couple of his socks and a stick of pineapple to me.

**George**
Ah, permission to speak, Sir?!

**Blackadder**
Er, I think not, actually.

**Melchett** Quite right, if what happens when you open your mouth is anything like what happens when you open your paintbox, we'd all be drenched in phlegm. Oh no, this isn't what we're looking for at all, is it Darling?

**Darling**
No, Sir.

**Melchett**
No, Sir!

**Blackadder**
There is this, Sir, it's Private Baldrick's

[SHOWS BALDRICK'S PAINTING]

He's called it My *Family and other Animals*.

**Melchett**
Oh, good Lord no.

**Blackadder**
Well, I'm afraid that's about it, Sir. Apart from... this little thing.

[SHOW GEORGE'S PAINTING]

**Melchett**
Ah, now, that's more like it!

**Darling**
Who painted this, Blackadder?

**Blackadder**
Well, actually, it was me.

**George**
Permission to speak, really quite urgently, Sir!

**Melchett**
Damn and blast your goggly eyes! Will you stop interrupting, George! Now, this is excellent!

[SHAKES BLACKADDER'S HAND]

Congratulations man! It's totally inspiring, makes you want to jump over the top and yell "Yah-boo sucks to you, Fritzy".

**Blackadder**
Thank you, Sir.

**Darling**
Are you sure you did this, Blackadder?

**Blackadder**
Of course I'm sure.

**Darling**
I'm afraid I don't believe you.

**Blackadder**
How dare you, Darling!?

[TO MELCHETT]

You know I can't let that slur pass, Sir... What possible low, suspicious, slanderous reasons could this "office-boy" have to think that I didn't paint the picture?

**Darling**
Well, three reasons as a matter of fact. Firstly: you're in it.

**Blackadder**
It's a self-portrait.

**Darling**
Secondly: you told us you couldn't paint.

**Blackadder**
Well, one doesn't want to blow one's own trumpet.

**George**
Permission...

**Blackadder**
Denied.

**Darling**
And thirdly: it's signed "George".

> Blackadder moves casually over to the picture and looks closely at it.

**Blackadder**
Well spotted. But not signed "George", dedicated "to George", King George. Gentlemen! The King!

**All**
[SNAPPING TO ATTENTION]

The King!

**Baldrick**
Where?

**Melchett**
Bravo Blackadder, I have absolutely no hesitation in appointing you our official regimental artist. You're a damn fine chap, not a pen-pushing, desk-sucking, blotter-jotter like Darling here, eh Darling?

**Darling**
No, Sir.

**Melchett**
No, Sir! Well, accompany us back to HQ immediately.

**Darling**
Attention!
 [MELCHETT AND DARLING EXIT.]

**George**
Permission to jolly well speak right now, Sir, otherwise I might just burst like a bally balloon.

**Blackadder**
Later George. Much later.

## SCENE 5: STAFF HQ

**Melchett**
Congratulations on your new appointment, Blackadder.

**Blackadder**
Thank you, Sir.

**Darling**
And may I say, Blackadder, I'm particularly pleased about it.

**Blackadder**
Are you.

**Darling**
[SMUGLY]

Oh yes.

**Melchett**
Now that you are our official war-artist, we can give you the full briefing. The fact is, Blackadder, that the *King and Country* cover story was just a... cover story. We want you, as our top painting bod, to leave the trenches...

**Blackadder**
Good.

**Melchett**
Tonight...

**Blackadder**
Suits me.

**Melchett**
And go out into No-Man's Land.

**Blackadder**
No-Man's Land?

**Melchett**
Yeeeeeees...

**Blackadder**
Not Paris?

**Melchett and Darling**
Noooooooo.

**Melchett**
We want you to come back with accurate drawings of the enemy positions.

**Blackadder**
You want me to sit in No-Man's Land, painting pictures of the Germans.

**Melchett**
Precisely! Good man!

**Blackadder**
Well, it's a very attractive proposition, gentlemen, but unfortunately not practical. You see, my medium is light. It'll be pitch dark; I won't be able to see a thing.

**Melchett**
Ah, hm. That is a point. I tell you what: we'll send up a couple of flares. You'll be lit up like a Christmas tree.

**Blackadder**
Oh, excellent, excellent. Glad I checked.
               ---------------

## SCENE 6: No-Man's Land

> Blackadder, George and Baldrick are crawling through very dangerous-looking territory.

**Blackadder**
All right, total and utter quiet, do you understand? So for instance if any of us crawl over any barbed wire they must onno account go—AAAAAAAAAAAHH!

**Baldrick**
Have you just crawled over some barbed wire, Sir?

**Blackadder**
No Baldrick, I just put my elbow in a blob of ice cream.

**Baldrick**
Oh, that's all right then.

**Blackadder**
Now, where the hell are we?

> George consults the map.

**George**
Well, it's difficult to say, we appear to have crawled into an area marked with mushrooms.

**Blackadder**
[PATIENTLY]
What do those symbols denote?

**George**
Pfff. That we're in a field of mushrooms?

**Blackadder**
Lieutenant, that is a military map, it is unlikely to list interesting flora and fungi. Look at the key and you'll discover that those mushrooms aren't for picking.

**George**
Good Lord, you're quite right, Sir— it says "mine". So, these mushrooms must belong to the man who made the map.

**Blackadder**
Either that, or we're in the middle of a mine *field*.

**Baldrick**
Oh dear.

**George**
So, he owns the field as well?
 [MACHINE-GUNS FIRE.]

> A star shell bursts and bathes them in light. Immediately the sound of machinegun fire is heard.

**George**
[YELLING]
THEY'RE FIRING SIR, THEY'RE FIRING.

[THE GUNS STOP.]

**Blackadder**
Ah yes, thank you, Lieutenant. If they hit me you'll be sure to point it out, won't you. Now come on, get on with your drawing and let's get out of here.

**George**
Well, surely we ought to wait for the flare, Sir? You see, my medium is light.

**Blackadder**
Just use your imagination for heavens sake.

> Blackadder has a sudden revelation.

Wait a minute, that's the answer--- I can't believe I've been so stupid

**Baldrick**
Yeah, that is unusual, 'cos usually I'm the stupid one.

**George**
Well, I'm not over-furnished in the brain department.

**Blackadder**
Well, on this occasion I've been stupidest of all.

**George**
Oh, now, Sir! I will not have that! Baldrick and I will always be more stupid than you. Isn't that right, Baldrick?

[STANDING UP]

Stupid, stupid, stupid.

**Baldrick**
Yeah, [STANDING UP ALSO] stupidy, stupidy, stupidy.

 [FLARES ARE FIRED, LIGHTING UP GEORGE AND BALDRICK. BLACKADDER COWERS ON THE GROUND.]

**George**
Stupidest stupids in the whole history of stupidityness.

 [MACHINE-GUN FIRE; BALDRICK AND GEORGE JUMP DOWN; THE GUNS STOP.]

**Blackadder**
Finished? I think the obvious point is this: we'll go straight out to the dugout and do the painting from there. You do the most imaginative, most exciting possible drawing of German defences from your imagination.

**George**
Oh, I see. Now that is a challenge.

**Blackadder**
Quite. Come on, let's get out of here.

**George**
Oh, Sir, just one thing. If we should happen to tread on a mine, what do we do?

**Blackadder**
Well, normal procedure, Lieutenant, is to jump 200 feet into the air and scatter yourself over a wide area.

## SCENE 7: STAFF HQ

> George and Blackadder stand at attention. Melchett and Darling are inspecting the map. Now the entire German army is massed opposite Blackadder's trench.

**Darling**
Are you sure this is what you saw, Blackadder?

**Blackadder**
Absolutely. I mean there may have been a few more armament factories, and

[LOOKS SIDEWAYS AT GEORGE]

not quite as many elephants, but...

**Melchett**
Well, you know what this means...

**Darling**
If it's true, Sir, we'll have to cancel the push.

**Melchett**
Exactly....

**George**
Damn!

**Blackadder**
What a nuisance...

**Melchett**
...Exactly what the enemy would expect us to do, and therefore exactly what we shan't do!

**Blackadder**
Ah.

**Melchett**
Now, if we attack where the line is strongest, then Fritz will think that our reconnaissance is a total shambles. This will lull him into a sense of false security, and then next week we can attack where the line is actually badly defended. And win the greatest victory since the Winchester flower-arranging team beat Harrow by twelve sore bottoms to one!

**Blackadder**
Tell me, have you ever visited the planet Earth, Sir?

**Melchett**
So, best fighting trousers on, Blackadder!

**George**
Permission to shout "Bravo" at an annoyingly loud volume, Sir?

**Melchett**
Permission granted.

**George**
[ANNOYINGLY LOUD VOLUME]
BRAVO!!!!!!!!!!

**Melchett**
That's the spirit. Just your kind of caper eheh, Blackadder?

**Blackadder**
Oh yes.

**Darling**
Good luck against those elephants...

[BLACKADDER AND GEORGE SALUTE AND LEAVE.]

## SCENE 8: THE DUG-OUT

> Blackadder and George enter. Baldrick has been cooking.

**Blackadder**
Get me a chisel and some marble,ssssssssssssssssssssssssssssssssd Baldrick.

**George**
Oh, you're taking up sculpture now, Sir?

**Blackadder**
No, I thought I'd get my headstone done.

**George**
What are you going to put on it?

**Blackadder**
"Here lies Edmund Blackadder, and he's bloody annoyed."

**Baldrick**
Are we goin' over, are we, Sir?

**Blackadder**
Yes, we are. Unless I can think of some brilliant plan.

**Baldrick**
Would you like some "rat-au-van" to help you think?

[SHOWS BLACKADDER A TIN PLATE WITH A VERY FLAT RAT ON IT]

**Blackadder**
"Rat-au-vin"?

**Baldrick**
Yeah, it's rat that's been...

**Blackadder**
...run over by a van. No thank you, Baldrick. Although it gives me an idea. Telephone please.

## SCENE 9: STAFF HQ

> A candle-lit table is set up with crystal goblets and salt and pepper, with china dishes and silver serving dishes.

> Darling and Melchett are finishing their main course and the pudding waits on a giant silver platter.

**Darling**
I suppose Blackadder and his boys will have gone over the top by now.

**Melchett**
Yes. God, I wish I were out there with them, dodging the bullets, instead of having to sit here drinking this Château Lafite, eating these filets mignon in sauce béarnaise.

**Darling**
My thoughts exactly, Sir. Damn this Château Lafite.

**Melchett**
He's a very brave man, Blackadder. And of course that Lieutenant of his, George. Cambridge man, you know. His uncle Bertie and I used to break wind for our college.

> He takes a bite.

Slightly unusual taste, this sauce béarnaise...

**Darling**
Yes, Sir, and to be quite frank, these mignon are a little... well...

**Melchett**
What?

**Darling**
Well, dungy...

**Melchett**
What on earth's wrong with our cook?

**Darling**
Well, it's a rather strange story, Sir.

**Melchett**
Oh? Tell, tell.

**Darling**
Well, Sir, I received a phonecall this afternoon from Pope Gregory IX, telling me that our cook had been selected for the England Cricket team and must set sail for the West Indies immediately.

**Melchett**
Really?

**Darling**
Then a moment later, the phone rang again. It was a trio of wandering Italian chefs, who happened to be in the area, offering their services. So I had the quartermaster take them on at once.

> Melchett seems satisfied at this, and takes a bite of his pudding.

**Melchett**
Ah, hm, Hm, HM , Ah, Oh, OH!! Jumping giblets! Are  you sure these are real raisins in this plum-duff?

**Darling**
Oh yes, I'm sure they are, Sir. Everything will be alright, once the cream custard arrives.

## SCENE 10: THE DUG-OUT

> Enter Blackadder, George and Baldrick with false moustaches and chefs' coats on after their night of catering at HQ. Baldrick is carrying a jug and a small kitten.

**George**
Well, that was all jolly good fun, Sir. But dash it all, we appear to have missed the big push.

**Blackadder**
Oh damn, so we have. One thing puzzles me, Baldrick; how did you manage to get so much custard out of such a small cat?

[CLOSING CREDITS]
                                ----------
                           Captain Edmund Blackadder
                                ROWAN ATKINSON

                             Private S. Baldrick
                                TONY ROBINSON

                           General Sir Anthony Cecil
                               Hogmanay Melchett
                                  STEPHEN FRY

                           Lieutenant the Honourable
                         George Colthurst St. Barleigh
                                  HUGH LAURIE

                             Captain Kevin Darling
                                 TIM McINNERNY

                                Title Music
                         Composed and Arranged by
                              HOWARD GOODALL

                                 Played by
                       The Band of the 3rd Battalion
                        The Royal Anglian Regiment
                             (The Pompadours)

                                Bandmaster
                             WOI TIM PARKINSON

                         P/BR.  647989  Libotte, J
                         Vis/E.  110143  Turner, R
                        Tech/Co. 364007  Massen, D
                          V/M  420372  Abbott, C
                        VTE.  614981  Wadsworth, C
                         Cm/S.  841842  Hoare, J
                         S/Svr.  733731  Deane, M
                         Dep/Svr.  713429  Way, N
                        L/Dr.  988212  Bristow, R
                        P/Mgr.  323476  Cooper, D
                       P/Att.  114209  Sharples, V
                         AFM  529614  Kennedy, J
                       C/Dgr.  368807  Hardinge, A
                        M/V Dgr.  82641  Noble, C
                          Dgr.  404371  Hull, C

                         Dir.  232418  Boden, R

                         Prod.  597602  Lloyd, J

                          (c) BBC TV MCMLXXXIX
