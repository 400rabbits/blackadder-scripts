Title:      Blackadder goes Forth: Episode 2: Corporal Punishment
Credit:     written by
Author:     Richard Curtis AND Ben Elton
Credit:     directed by
Director:   Richard Boden
Source:     NULL
Notes:      UNOFFICIAL TRANSCRIPTION FROM AIRED SHOW
Original air date: 05 october 1989
Copyright:  (c) 1989, British Broadcasting Corporation
Summary:    Blackadder hatches a cunning plan to avoid starvation and ignore his orders by eating a carrier pigeon.

# Blackadder goes Forth: Episode 2: Corporal Punishment

## SCENE 1: THE DUG-OUT

> It is morning. Blackadder is on the phone. George is listening keenly.

**Blackadder**
You'd like to book a table for three by the window for 9.30 PM, not too near the band, in the name of Oberleutnant von Genshler. Yes, yes, I think you might have the wrong number. Alright.

[HANGS UP; ENTER GEORGE]

**George**
Rather close line there, eh, Sir? That phone system is a shambles no wonder we haven't had any orders!

**Blackadder**
Oh, on the contrary, George, we've had plenty of orders. We have orders for six meters of Hungarian crushed velvet curtain material, four rock salmon and a ha'porh of chips and a cab for a Mr. Redgrave picking up from 14 Amos Grove, ring top bell.

**George**
Rather we don't want those sort of orders; we want orders to death or glory. When are we going to give Fritz a taste of our British spunk?

**Blackadder**
George, please. No one is more anxious to advance than I am, but until I get these communication problems sorted out, I'm afraid we're stuck.

> The phone rings again.

Captain Blackadder speaking..... No, I'm afraid the line's very cclllffffhhtttt!

> He makes a very strange, dysfunctional phone sound. Captain Darling is on the other end of the line in Melchett's HQ.

**Darling**
Hullo? Hullo, Captain Blackadder, hullo?

> Blackadder rustles a newspaper into the phone and bangs it against the receiver. Then fakes a German accent...

**Blackadder**
*Schenll! Achenll, Kartoffelkopf*! ...

**Darling**
I said, there's a terrible line at my end. You're to advance on the enemy at once!

> Now Blackadder starts blowing raspberries into the phone, and then begins to sing...

**Blackadder**

Pffft pppfffft ppp pffff... "A wandering minstrel, I..." beeelooop ... gale force eight imminent.

> And hangs up.

**George**
I say, come on, Sir, what's the message? I'm on tenterhooks. Do tell!

**Blackadder**
Well, as far as I can tell, the message was, "he's got a terrible lion up his end, so there's an advantage to an enema at once."

**George**
Damn!

> Baldrick enters.

**Baldrick** Message from HQ, Sir.

**George**
Ah, now, this should be it. A telegram ordering an advance!

**Blackadder**
Ummm yes, I'm afraid not, George, it is a telegram, it is ordering an advance, but it seems to be addressed to 'Catpain Blackudder'. Do you know a 'Catpain Blackudder', George?

**George**
Well, it rings a bell, but I..

**Blackadder**
Ouhh.....nope, me neither.

> He screws up the telegram and throws it away.

**George**
Oh well.

> Baldrick rushes in, shouting.

**Blackadder**
Go away George; I'm sure if they want to contact us, they'll find a way.

**Baldrick**
Pigeon, Sir! Pigeon. There's a pigeon in our trench!

> He exits again.

**George**
Ah, now, this'll be it!

> They leave the dug-out and see a large pigeon at the top of the trench.

Yes. It's one of the King's Carrier Pigeons.

**Baldrick**
No, it isn't. That pigeon couldn't carry the King! Hasn't got a tray or anything.

**Blackadder**
Lieutenant, revolver please.

> Blackadder takes George's gun.

**George**
Oh now, Sir, you really shouldn't do this you know!

**Blackadder**
Come on, George. With 50,000 men getting killed a week who's going to miss a pigeon?

> Blackadder shoots the pigeon. Feathers fly.

**George**
Well, not you, obviously, Sir.

**Blackadder**
In any case, it's scarcely a court-martial offence. Get plucking, Baldrick.

**Baldrick**
Alright, Sir. Look, it's got a little ring 'round it's leg, there's a novelty!

**George**
Oh, really? Is there a paper hat as well?

**Baldrick**
No, but there's a joke. Read it out, Sir.

**George**
It's a bit charred. "Something something at once... PS, due to communication crisis, the shooting of carrier pigeons is now a court-martial offence". I don't see what's so funny about that, Sir.

**Blackadder**
That's not funny, it's deadly serious. We're in trouble— so, I shall eat the evidence for lunch. And if anyone asks you any questions at all, we didn't receive any messages and we definitely did *not* shoot this delicious plump-breasted pigeon.

## SCENE 2: THE DUG-OUT

> Blackadder has just finished eating the pigeon.

**Blackadder**
Mmmm. Delicious.

> Suddenly from outside...

**Melechett**
Eahy, Blackadder!

**Darling**
'Ten Shun!

> Blackadder and Baldrick stand to attention. Melchett and Darling sweep in.

**Melechett**
And why, Captain, are you not advancing across No Man's Land?

**Blackadder**
Well, Sir, call me a bluff old traditionalist, but I was always taught to wait for the order to attack before attacking.

**Melechett**
Are you trying to tell me you haven't received any orders? What the hell are you playing at, Darling?

**Darling**
That's a blatant lie, Sir. I spoke to Blackadder less than an hour ago.

**Blackadder**
Yes, you did. To tell me some gobbledygook about having a lion up your bottom.

**Melechett**
Umm...I thought it's the old communications problem again. Stand easy!

Action on this is imperative, take that down, Darling.

> Darling unwillingly takes it down in his notebook.

**Darling**
Yes, Sir.

**Melchett**
And make a note of the word 'gobbledegook'— I like it— I want to use it more often in conversation.

**Darling**
I must say, Sir, I find this all very unlikely. Not only did I tele- phone Blackadder, but as you'll recall, we sent him a telegram and a carrier pigeon.

**Blackadder**
[IN MOCK ASTONISHMENT]

Did you?

**Darling**
Are you telling us you haven't had a pigeon, Blackadder?

> Blackadder eyes his plate nervously.

**Blackadder**
Err...

> He deftly pulls a napkin over the carcass.

**Melchett**
Come on, you must have done . I sent our top bird, Speckled Jim. My only true love, who's been with me since I was a nipper!

> He swiftly returns to business matters...

And to business. I'm giving you your order to advance now. Syncronize watches, gentlemen. Private, what is the time?

**Baldrick**
We didn't receive any messages, and Captain Blackadder definitely did not shoot this delicious plump-breasted pigeon, Sir.

> Melchett roars back at him.

**Melechett**
WHAT???

**Blackadder**
You want to be cremated, Baldrick, or buried at sea?

**Baldrick**
[Thinking it over]
Ummmmmm....

> At which moment George enters, humming.

**Darling**
Lieutenant?

**George**
Sir!

**Darling**
Do you mind answering a couple of questions?

**George**
Not at all, Sir. We didn't get any messages and Captain Blackadder definitely did not shoot this delicious plump breasted pigeon.

**Blackadder**
Thanks, George.

**Darling**
And look, Sir. Pigeon feathers! White feathers! Very apt, eh, Blackadder?

> Melchett takes the feathers Darling has found and examines them.

**Melechett**
White feathers?

**Baldrick**
Oh no, Sir, that's gobble a duke! They're not white, they're sort of speckly!

**Melechett**
[Shocked]
Speckly?! AAHHHHHHHH! YOU SHOT MY SPECKLED JIM???

**Darling**
You're for it now, Blackadder. Quite frankly, Sir, I've suspected this for some time. Quite clearly, Captain Blackadder has been disobeying orders with a breathtaking impertinence.

**Melechett**
I don't care if he's been rogering the Duke of York with a prize-winning leak! He shot my pigeon!

[Screams]

AAAHH! AHHHH! OOOHHHH!

> Darling holds back Melchett, who has gone mad and is trying to whip Captain Blackadder.

**Darling**
Easy, Sir. Easy. Take it easy. I think we should do this by the book, Sir.

**Melechett**
Yes, yes, you're right, of course. I'm sorry.

**Darling**
Dug-out, Ten Shun!

**Darling**
[DRUMS ARE HEARD IN THE BACKGROUND]

Captain Blackadder, as of this moment you may consider yourself under arrest. You know what the penalty is for disobeying orders, Blackadder?

**Blackadder**
Umm.. court-martial, followed by immediate cessation of chocolate rations?

**Darling**
No. Court-martial followed by immediate death by firing squad.

**Blackadder**
Oh, so I got it half right.

## SCENE 3: A CELL IN THE MILITARY PRISON

> A small cell, bars on the window. Blackadder sits reading. Perkins the jailor enters. He is a really friendly love.

**Perkins**
All settled in and happy then, are we, Sir? Written all our last goodbyes?

**Blackadder**
Oh, no need for that, Perkins, I'll just dash off a couple of notes, one asking for a sponge bag, and the other sending for my lawyer.

**Perkins**
Oh, your lawyer, yes, Sir. Now, don't you think that might be a bit of a waste of money, Sir.

**Blackadder**
Not when he's the finest mind in English legal history. Ever heard of Bob Massingbird?

**Perkins**
Oh, yes indeed, Sir! A most gifted gentleman!

**Blackadder**
Quite. I remember Massingbird's most famous case— the Case of the Bloody Knife. A man was found next to a murdured body. He had the knife in his hand, thirteen witnesses had seen him stab the victim, and when the police arrived he said, "I'm glad I killed the bastard." Massingbird not only got him off, but he got him knighted in the New Year's Honors list, and the relatives of the victim had to pay to have the blood washed out of his jacket.

**Perkins**
I hear he's a dab hand at the prosecution as well, Sir.

**Blackadder**
Yes, well, look at Oscar Wilde.

**Perkins**
Old "Butch" Oscar.

**Blackadder**
Exactly. Big, bearded, bonking, butch Oscar. The terror of the ladies. 114 illegitamate children, world heavyweight boxing champion, and author of the best-selling phamplet, "Why I Like to Do It With Girls". And Massingbird had him sent down for being a whoopsy.

> There is a knock at the door— Perkins goes to answer. Baldrick enters and Perkins leaves.

Ah, Baldrick. Anything from Massingbird yet?

**Baldrick**
Yes, Sir. It just arrived, Sir.

**Blackadder**
What is it?

**Baldrick**
A sponge bag, Sir.

**Blackadder**
A sponge bag.

**Blackadder**
Baldrick, I gave you two notes. You sent the note asking for a sponge bag to the finest mind in English legal history.

**Baldrick**
Certianly did, Sir!

**Blackadder**
And you sent the note requesting legal representation to...?

[Enter George]

**George**
Well, tally-ho, with a bing and a bong and a buzz-buzz-buzz! [THUMP!]

**Blackadder**
[Digustingly, as we've all heard before] Oh God!

> George enters in high spirits.

**George**
Well, tally-ho! With a bing and a bong and a buzz, buzz, buzz!

**Blackadder**
Oh, God!

**George**
May I say first of all, Sir, that I am deeply, deeply honored.

**Blackadder**
Baldrick, I'll deal you later. Am I to understand that you are going to represent me at the court-martial?

> Baldrick leaves before this important briefing.

**George**
Absolutly, Sir. Well, it's a sort of family tradition, really. My uncle's a lawyer, you know.

**Blackadder**
Your uncle's a lawyer... but you're not?

**George**
Oh, good Lord, no! I'm absolute duffer at this sort of thing. In school the debating society, I was voted the boy least likley to complete a coherent...umm...

**Blackadder**
Sentence?

**George**
That's it, yes! But anyway, my dear old friend, it's an honour to serve.

**Blackadder**
George, I'm in trouble here. I need to construct a case that's as watertight as a mermaid's brassiere. I'm not sure your particular brand of mindless optimism is going to contribute much to the proceedings.

**George**
Well, that's a shame, Sir, because I was planning on playing the mindless optimisim card really strongly during this trial.

**Blackadder**
I beg your pardon?

**George**
Yes, I've already planned my closing address based on that very thing.

> He takes out paper and grandly prepares to read.

"Oh, go on, let him off, your honour, please! It's a lovely day. Pretty clouds, trees, birds, etc. I rest my case."

**Blackadder**
So, Counsel, with that summing up in mind, what do you think my chances are?

**George**
Well, not all that good I'm afraid. As far as I can tell you're as guilty as a puppy sitting next to a pile of poo.

**Blackadder**
Charming.

### SCENE 4: MELCHETT's HQ

> Melchett's chamber has been transformed into a court of law. A clerk of the court is getting the judge's papers ready.
>
> Blackadder is standing between two guards. George charges in afer him, in a wig and gown. He rushes forward to speak.

**George**
Crikey! So sorry I'm late, m'lord. But anyway, let me open up my defence straight away, by saying that I've known this man for three years, he's an absolutely corking chap...

**Blackadder**
George?

**George**
Yes, Sir?

**Blackadder**
That's the clerk of the court.

**George**
Is it? Oh!

**Blackadder**
We haven't started yet.

**George**
Ah!

> Darling enters.

**Darling**
Good luck, Blackadder.

**Blackadder**
Well, thank you, Darling. And what's your big job here today? Straightening chairs?

**Darling**
No, in fact I'm appearing for the prosecution. I wouldn't raise your hopes too much, you're guilty as hell. You haven't got a chance.

**Blackadder**
Why thank you, Darling. And I hope your mother dies in a freak yachting accident.

**Darling**
Just doing my job, Blackadder. Obeying orders. And of course, having enormous fun into the bargain.

**Blackadder**
I wouldn't be too confident if I were you— any reasonably impartial judge is bound to let me off.

**Darling**
Well, absolutely.

**Blackadder**
Who is the judge, by the way?

> Melchett's familiar 'baarp' sounds from outside.

**Melechett**
BAARP!

**Blackadder**
I'm dead.

> And Melchett enters, judge and jury.

**Melechett**
Well, come on, then. Come on. Get this over in five minutes, and then we can have a spot of lunch.

Ooh, ah. The court is now in session, General Sir Anthony Cecil Hogmenay Melchett in the chair. The case before us is that of the Crown vs. Captain Edmund Blackadder, the Flanders Pigeon Murderer! Oh, uh, Clerk, hand me the black cap shall you,? I'll be needing that.

> The cap is handed to him

Thank you.

**Blackadder**
I love a fair trial.

**Melechett**
Anything to say before we kick off, Captain Darling?

**Darling**
May it please the court. As this is clearly an open and shut case, I beg leave to bring a private prosecution against the defence counsel for wasting the court's time.

**Melechett**
Granted. The defence counsel is fined fifty pounds for turning up.

**George**
[TO BLACKADDER]
This is fun! This is just like a real court!

**Melchett**
Alright! Let the trial begin! The charge before us is that the Flanders Pigeon Murderer did deliberately, callously, and with beastliness of forethought murder a lovely, innocent pigeon. And disobeyed some orders as well. Is this true?

> George springs to his feet.

**George**
Perfectly true, Sir. I was there.

**Blackadder**
Thanks, George.

> He suddenly realizes his mistake.

**George**
Oh, dammit.

**Melechett**
Right. Counsel for the defence, get on with it.

> George gets up hurridly.

**George**
Oh, right. Yes, yes, right. Um, yes. I'd like to call my first witness. Captain Darling.

**Melechett**
You wish to call the counsel for the prosecution as a defence witness?

**George**
That's right.

> He whispers to Blackadder

Don't worry, Sir, I've got it all under control.

> Blackadder is skeptical. Darling takes the stand.

You are Captain Darling of the General Staff?

**Darling**
I am.

> George makes a thumbs-up gesture to Blackadder— so far, so good!

**George**
Captain, leaving aside the incident in question, would you think of Captain Blackadder as the sort of man that would usually ignore orders?

**Darling**
Yes, I would.

**George**
 Ah! Errm. You sure? I... I was rather banking on you saying "no" there.

**Darling**
I'm sure. In fact, I have a list of other orders he's disobeyed, if it would be useful.

> Melchett nods vigorously in agreement.

**Melechett**
Hmmm. Hmmm.

> Darling pulls out a notebook.

May 16th, 9:15am, 10:23am, 10:24am,  11:17am...

**George**

> Reading the list with him.

You missed one out, there.

**Darling**
...10:30am, 11:46am...

**Blackadder**
George!

**George**
What?  Oh, oh ye-ye-right, yes. Thank you, Captain. No further questions.

> Darling leaves the stand.

**Blackadder**
Well done, George. You really had him on the ropes.

**George**
Don't worry, old man. I have a last, and I think you'll find decisive, witness. Call Private Baldrick.

**Guard**
Call Private Baldrick!

> Enter Baldrick. Blackadder is not hopeful and whispers to him as he passes.

**Blackadder**
Deny everything, Baldrick.

> Baldrick goes into the dock.

**George**
Are you Private Baldrick?

**Baldrick**
NO!

**George**
Um, but you are Captain Balckadder's batman?

**Baldrick**
NO!

> Blackadder is now in despair. He bangs his head on the desk in front.

**George**
Come on, Baldrick. Be a bit more helpful— it's me.

**Baldrick**
No, it isn't!

**Darling**
Sir, I must protest!

**Melechett**
Quite right! We don't need your kind here, Private. Get out. Now, George— sum up, please.

**George**
Oh, right, yes, uhhhh....

> Blackadder hands him a piece of paper and he begins to read it.

Oh...Uh... Gentlemen, you have heard all the evidence presented here today, but in the end it is up to the conscience of your hearts to decide, and I firmly belive, that like me, you will conclude that Captain Blackadder is in fact, totally and utterly, GUILTY...

> He sits down pleased with himself. Blackadder politely turns over the piece of paper to show George the other side. George stands up again.

...of nothing more than trying to do his duty under difficult circumstances.

**Melechett**
Nonsense! He's a hound and a rotter, and he's going to be shot!  However, before we proceed to the formality of sentancing the deceased — I mean, the defendant, (he laughs at this excellent joke) — I think we'd all rather enjoy the case for the prosecution. Captain Darling, if you please.

> Darling clears his throat.

**Darling**
Sir, my case is very simple. I call my first witness, General Sir Anthony Cecil Hogmaney Melchett.

**Melechett**
Ah..umm!

[Goes up to the stand]

**George**
Clever, clever!

**Darling**
General, did you own a lovely, plump, speckly pigeon called Speckled Jim, which you hand reared from a chick and which was your only childhood friend?

> Melchett, now in the witness stand, is very moved.

**Melechett**
[Hysterical] Yes! Yes, I did.

**Darling**
And did Captain Blackadder shoot the aforementioned pigeon?

> Melchett is very moved and very angry.

**Melechett**
Yes, he did!

**Darling**
[Shouts]
Can you see Captain Blackadder anywhere in this courtroom?

**Melechett**
YES, THAT'S HIM!! THAT'S HIM!! THAT'S THE MAN!! AAHHHHH!! AAAAHHHHHH!!!

> He screams and points at Blackadder.

**Darling**
No more questions, Sir.

> Melchett recovers instantly and goes back to his judicial seat.

**Melechett**
Splendid. Excellent. First class.

> He shuffles past the other judges

Out of the way, come on.

> He places the black cap on his head.

I therefore have absolutely no hesitation in announcing that the sentence of this court is that you, Captain Edmund Blackadder be taken from this place and suffer death by shooting tommorrow at dawn.

[BANGS GAVEL]

 Do you have anything to say?

**Blackadder**
Yes. Could I have an alarm call, please?

### SCENE 5: THE CELL

> Blackadder is lying on his bed. Perkins, the guard, enters.

**Perkins**
Chappy to see you, Captain.

**Blackadder**
What does he look like?

**Perkins**
Short, ugly...

**Blackadder**
Hullo, Baldrick.

> Baldrick enters just after Blackadder says his name. He is carrying a sack. He is also talking in a rather odd stacato manner.

**Baldrick**
I brought you some food, Sir: for your final breakfast tommorrow.

**Blackadder**
Ah, so you're not pinning much hope on a last minute reprieve then.

**Baldrick**
No, Sir, you are as dead as some doo-doos.

**Blackadder**
The expression, Baldrick, is 'as a dodo'. 'Dead as a dodo'.

**Perkins**
Well, I'll leave you to it then, shall I?

> Perkins leaves.

**Baldrick**
Do not despair, Sir. All my talk of food was jsut a dead herring. In fact, I have a cunning plan. This is not food, but an escape kit.

**Blackadder**
Good Lord! With a saw, a hammer, a chisel, a gun, a change of clothes, a Swiss passport, and a huge false moustache, I may just stand a chance.

> This throws Baldrick slightly.

**Baldrick**
Ah.

**Blackadder**
Let's see, what have we here?

> He reaches into the bag and takes out a little object.

A small painted wooden duck.

**Baldrick**
Yeah, I thought if you get caught near water, you can balance  it on the top of your head as a brillaint disguise.

**Blackadder**
Yeeeesss, I would, of course, have to escape first. Ah, but what's this­— unless I'm much mistaken, a hammer and a chisel?

**Baldrick**
You *are* much mistaken!

> Blackadder takes two more objects from the bag.

**Blackadder**
A pencil and a miniature trumpet.

**Baldrick**
Yeah, a pencil so you can drop me a postcard to tell me how the break out went, and a small, little, tiny, miniature trumpet in case during your escape, you have to win favour with a difficult child.

**Blackadder**
Baldrick, I don't want to spend my last precious hours rummaging through this feeble collection of stocking-fillers. Now, let me ask you some simple questions: is there a saw in this bag?

**Baldrick**
No.

**Blackadder**
A hammer?

**Baldrick**
No.

**Blackadder**
A chisel?

**Baldrick**
No.

**Blackadder**
A gun?

**Baldrick**
No.

**Blackadder**
A false passport?

**Baldrick**
> He thinks for a second.
No.

**Blackadder**
A change of clothes?

**Baldrick**
Yes, Sir, of course. I wouldn't forget a change of clothes.

**Blackadder**
Ah, now that's something, let's see...

> Blackadder takes out a little hat and a toy bow and arrow.

...a Robin Hood costume!

**Baldrick**
Yeah. I put in a French peasant's outfit first, but then I thought 'What if you arrive in a French peasant's village and they're in the middle of a fancy dress party?'

**Blackadder**
And what if I arrive in a French peasant village, dressed in a Robin Hood costume and there *isn't* a fancy dress party?

**Baldrick**
Well, to be quite frank, Sir, I didn't consider that eventuality, because if you did, you'd stick out like a.....

**Blackadder**
[Interrupting]
Like a man standing in a lake with a small painted wooden duck on his head?

**Baldrick**
Exactly!

> Perkins enters.

**Perkins**
Excuse me, Sir.

**Blackadder**
Alright. Aaahhmm, thank you, Baldrick, we'll finish this picnic later.

**Baldrick**
Yum, yum!

> He leaves.

**Perkins**
Do you mind if I disturb you for a moment, Sir?

**Blackadder**
No, no, not at all. My diary's pretty empty this week. Let's see, "Thursday morning— get shot". Yes, that's about it, actually.

**Perkins**
It's just there's a few chaps out here would like a bit of a chinwag.

**Blackadder**
Oh, lovely— always keen to meet new poeple.

> Perkins brings in a miscellaneous bunch of pleasant-looking Tommies.

**Perkins**
Corpral Jones and Privates Fraser, Robinson, and Tipperwick

> Blackadder hulloes them and they shake hands.

**Blackadder**
Oh, nice of you to drop by. And what do you do?

> There is an uneasy pause. The soldiers chuckle.

**Seargent Jones**
We're your firing squad, Sir.

**Blackadder**
Of course you are.

**Private Robinson**
Good sized chest.

> He is inspecting Blackadder professionally.

**Seargent Jones**
Shut up, lad.

**Private Robinson**
Sir!

**Seargent Jones**
You see, us firing squads are a bit like taxmen, Sir; everyone hates us, but we're just doin' our job, aint we, lads?

**Blackadder**
My heart bleeds for you.

**Private Robinson**
Well, Sir, we *aim* to please.

[THEY ALL LAUGH]

Just a little firing squad joke there, Sir!

**Seargent Jones**
You see, Sir, we take pride in the termanatory service we supply. So, is there any particular area you'd like us to go for, hmm? We can aim anywhere.

**Blackadder**
Well, in that case, just above my head might be a good spot.

**Seargent Jones**
You see,you see! a laugh and a smile, and all of a sudden the job doesn't seem quite so bad after all, does it, Sir?

**Private Tipplewick**
[POINTING]
A lovely roomy forehead...

**Private Robinson**
A good pulsing jugular there as well.

> They touch Blackadder, who swats them away.

**Blackadder**
Look, I'm sorry— I know you mean to be friendly, but I hope you won't take it amiss if I ask you to sod off and die.

**Squad**
Woooohh!

**Seargent Jones**
No, no, no, no, no, no, no, fair enough, 'course not, Sir. No one likes being shot first thing in the morning, do they? No, no, no. So, err, look foreward to seeing you tomorow, Sir. You'll have a blindfold on of course, but you'll recognize me. I'm the one that says, "Ready... Aim... Fire!"

**Blackadder**
Can I ask you to leave a pause between the word "aim" and the word "fire"? Thirty or forty years, perhaps?

> The lads fall about laughing again.

**Seargent Jones**
Ahh, wish I could pause, Sir. I really wish I could, but I can't, you see, cos I'm a gabbler me, you see. READYAIMFIE! No style. No finesse, but it gets the job done, dunit, lads, eh? That's it. Come onlads, let's go.

> On the way out they can be heard talking between themselves.

**Private Tipplewick**
Whoever gets closest to the mole gets to keep his gold teeth.

**Private Robinson**
Good night, Sir.

> Blackadder is left alone.

**Blackadder**
Perfect! I wonder if anything on earth could depress me more?

> Re-enter Baldrick.

**Baldrick**
Excuse me, Sir?

**Blackadder**
Of course it could.

**Baldrick**
I forgot to give you this letter from Lieutenant George, Sir.

**Blackadder**
[Sarcastically]
Ahh! Oh, joy! What wise words from the world's greatest defence counsel.

[Reads letter] "Dear Mother," ...Unusual start.

[Continues]

"Thanks for the case of Scotch." You've excelled yourself, Baldrick. You've brought the worng letter again!

**Baldrick**
Cor yeah, he did write two.

**Blackadder**
Yes, his mother's about to get a note, telling her he's sorry she's going to be shot in the morning, while *I* have to read this drivel.

[READS FURTHER]

"Hope Celia thrives in the Pony Club trials and that little Freddy scores a century for the first eleven."
[ASIDE]

You can't deny, it's a riveting read. Uhhh, "Send my love to Uncle Rupert— who'd have thought it? Mad Uncle Rupert, Minister of War, with power of life or death over every bally soldier in the army..."

Hang on a minute... This is it! All George has to do is send him a telegram and he'll get me off!

[IN A PLEASANT TONE]

Baldrick, I love you! I want to kiss your cherry lips and nibble your shell-like ears. I'm freeeee!

> Baldrick puckers up. He's game.

## SCENE 6: THE DUG-OUT

> George is in a very quiet mood.

**George**
Ooh,I'm useless. Useless!

> Baldrick enters.

**Baldrick**
Sir, Sir!

**George**
Ah, hullo Private, how's the Captain?

**Baldrick**
He's absolutely fine, Sir, but...

**George**
Uhh, you're just trying to cheer me up. I know the truth. He hates me because I completely arsed up his defence.

**Baldrick**
Yes, I know, Sir, but...

**George**
Because I'm thick, you see. I'm as thick as the big-print version of The Complete Works of Charles Dickens. If only I could've saved him. If only!

**Baldrick**
But you *can*, Sir.

**George**
What? How?

**Baldrick**
You send a telegram.

**George**
Of course! I send a telegram.

**Baldrick**
Yeah!

**George**
Who to?

**Baldrick**
To the person in the letter.

**George**
What letter?

**Baldrick**
To your mother.

**George**
I send a telegram to my mother!

**Baldrick**
No.

**George**
No!

**Baldrick**
You send a telegram to the person in the letter to your mother.

**George**
Who was in the letter to my mother?

**Baldrick**
I can't remember!

**George**
Well, think, think.

**Baldrick**
No, you think think!

**George**
Well, uhh... Celia! Of course, the Pony Club Trials. Yes! Celia can leap over the walls of the prison and save him.

**Baldrick**
No, no, no!

> George begins to swing a cricket bat.
>
**George**
No, no. Uhm... Yes, cricket. Yes, I've got Cousin Freddie, of course! He can knock out the firing squad with his cricket bat.

**Baldrick**
No, there's someone else!

**George**
Oh well, who!?

**Baldrick**
I don't know.

**George**
Well, neither do I!

**Baldrick**
Well, think!

**George**
You think!

**Baldrick**
You think then!

**George**
I'm stuck, I'm stuck!

> Baldrick hits George on the head with the cricket bat.

**George**
No, it hasn't helped.

> Baldrick has a sudden revelation.
>
**Baldrick**
Yes it has, Sir. Your Uncle Rupert who's just been made Minister of War.

**George**
Of course. Uncle Rupert has just been made Minister of War. Baldrick, I'll, I'll send him a telegram and he'll, he'll pull strings and scratch backs and fiddle with nobs, and... he'll get the captain off.

**Baldrick**
HURRAY!

**George**
Aah, pfft. Well, I got there in the end, eh Baldrick?

**Baldrick**
Ooh, just about, Sir.

**George**
I think this calls for a celebration, don't you? What about a tot of old Morehen's Shredded Sporun, which Mumsie has just sent over?

> He gets a whisky bottle out and pours them both a drink.

I think a toast, don't you? To Captain Blackadder and freedom!

**Baldrick**
Captain Blackadder and Freedom, Sir.

## SCENE 7: THE COURTYARD

> the firing squad is loading up. Blackadder walks out onto the top step. He is very chipper.

**Blackadder**
'Morning!

**Squad**
'Morning, Sir.

**Perkins**
I must say, Captain, I've got to admire your balls.

**Blackadder**
Prehaps later.

> He moves down the firing squad.

So, boys, how're you doing?

> They each greet the captain.

**Private Fraser**
Very well, thank you, Captain.

**Blackadder**
Robinson, good to see ya!

> He does a little mime of pointing a gun at him— all very jovial.

**Robinson**
Good to see you, too, Sir.

**Blackadder**
Ahh, Corporal. How's the voice?

**Seargent Jones**
Excellent, Sir. READYAIMFIRE!

> The squad snatch up their rifles at this command.

Wait for it. Wait for it!

**Blackadder**
So, the phone's *on* the hook, is it Perkins?

**Perkins**
Oh yes, Sir.

**Blackadder**
So, where do you want me?

**Seargent Jones**
Well, up against the wall is traditional, Sir.

**Blackadder**
Course it is. Ah. This side or the other side?

> They all laugh— What a jolly day!

No messengers waiting, Perkins?

[SEARGENT JONES APPLIES THE BLINDFOLD]

**Perkins**
Oh, I'm afraid not, Sir.

**Blackadder**
Fair enough. Fair enough.

> Blackadder is starting to worry.

**Seargent Jones**
Alright, lads, line up.

**Blackadder**
Yes. Ahh... now look, I think there might have been a bit of a misunderstanding here. You see, I was expecting a telegram.

**Seargent Jones**
Ten shun!

**Blackadder**
Quite an imporant one, actually.

**Seargent Jones**
TAKE AIM!

> They point their rifles at him.

**Perkins**
Stop!

**Blackadder**
I think that's what they call 'the nick of time'.

**Perkins**
Message for you, Captain.

**Blackadder**
Of course it is. Read it please.

**Perkins**
Eh, "Here's looking at you— love from all the boys in the firing squad."

**Seargent Jones**
You soft bastards, you!

**Private Robinson**
I saw the card­ — I couldn't resist it.

> They are all giggling — very pleased with themselves.

**Blackadder**
[Sarcastically]
How thoughtful!

**Seargent Jones**
Ten shun!

**Blackadder**
Now look, ah, something has gone spectacularly badly wrong.

**Seargent Jones**
TAKE AIM!

**Blackadder**
Baldrick, you're mincemeat!

**Seargent Jones**
F...

## SCENE 8: THE DUG-OUT

> Baldrick and George are waking up where they passed out. They were clearly deeply, deeply drunk.

**George**
[GROANING]
Oh, my head! Ah, my head! Feels like the time I was initiated into the Silly Buggers society at Cambridge. I misheard the rules and push a whole aubergine into my earhole.

**Baldrick**
Permission to die, Sir.

**George**
Oh! wh-wh-wha-what started us drinking? Oh, yes, well— we were celebrating getting Captain Blackadder off scot...

[REALISES IT'S TOO LATE]

free. Oh my sainted trousers, we forgot!

> He jumps up.

**Baldrick**
Oh, whoops.

**George**
Oh no. He's dead, you see. He's dead, dead, dead because we're a, we're a pair of selfish so-and-sos...Oh God, if I have a rope, I'd put it around my neck and bally well hang myself until it really hurt.

> Blackadder enters, bright, breezy and casual, looking over the room.

**Blackadder**
'Morning George, 'Morning, Baldrick...

> They gape, open-mouthed in surprise, not quite believing that Blackadder is still alive.

Still the striking resemblence to guppy fish at feeding time. Yep, it arrived in the nick of time.

> He waves the telegram

**George**
[UNCOMPREHENDING]
Oh, excellent!

**Blackadder**
Ah, see you've got the Scotch out, anyway?

**George**
Oh, well, well, of course, Sir, yes. We wanted to lay on a bit of a bash for your safe return. Ah..here you go.

[GEORGE HANDS BLACKADDER SOME SCOTCH IN A TIN MUG. HE LAUGHS NERVOUSLY]

**Blackadder**
There was a second telegram that arrived actually, George, addressed to you personally from your uncle.

**George**
Oh, thank you, I....

? George reaches for it, but Blackadder casually opens it and reads...

**Blackadder**
"George, my boy... Outraged to read in dispatches how that ass Melchett made such a pig's ear out of your chum Blackadder's court-martial. Have reversed the decision forthwith. Surprised you didn't ask me to do it yourself, actually." Now this *is* interesting, isn't it?

> He looks up at George and Baldrick. They know they are in trouble.

**George**
Uh, uhh, yes, well, I, you see, Sir. Uh... the thing is...

**Blackadder**
You two got whammed last night, didn't you?

**George**
We— well, well, no, uh, no. Not whammed, exactly. A little tiddly, perhaps.

**Blackadder**
And you forgot the telegram to your uncle!

**George**
Well, n--n--n-no. Not, not, not completely. Partially, umm.... Well yes, yes. Entirely. Yes.

**Baldrick**
I think I can explain, Sir.

**Blackadder**
Can you, Baldrick?

**Baldrick**
No.

**Blackadder**
As I suspected. Now, I'm not a religious man, as you know, but henceforth, I shall nightly pray to the God who killed Cain and squashed Sampson, that He comes out of retirement and gets back into practice on the pair of you!

> The field phone rings. Blackadder snatches it up.

**Blackadder**
Captain Blackadder! Ah, Captain Darling. Well, you know, some of us just have friends in high places, I suppose. Yes, I can hear you perfectly. You want what? You want two volunteers for a mission into No Man's Land, Codename—  Operation Certain Death. Yes, yes I think I have *just* the fellows.

> He hangs up and looks at them.

God is *very* quick these days.

[CLOSING CREDITS]
                                      ----------

                           Captain Kevin Darling
                               TIM McINNERY

                                Title Music
                         Composed and Arranged by
                              HOWARD GOODALL

                                 Played by
                       The Band of the 3rd Battalion
                        The Royal Anglian Regiment
                             (The Pompadours)

                                Bandmaster
                             WOI TIM PARKINSON

                         P/BR.  647989  Libotte, J
                         Vis/E.  110143  Turner, R
                        Tech/Co. 364007  Massen, D
                          V/M  420372  Abbott, C
                        VTE.  614981  Wadsworth, C
                         Cm/S.  841842  Hoare, J
                         S/Svr.  733731  Deane, M
                         Dep/Svr.  713429  Way, N
                        L/Dr.  988212  Bristow, R
                        P/Mgr.  323476  Cooper, D
                       P/Att.  114209  Sharples, V
                         AFM  529614  Kennedy, J
                       C/Dgr.  368807  Hardinge, A
                        M/V Dgr.  82641  Noble, C
                          Dgr.  404371  Hull, C

                         Dir.  232418  Boden, R

                         Prod.  597602  Lloyd, J

                          (c) BBC TV MCMLXXXIX
